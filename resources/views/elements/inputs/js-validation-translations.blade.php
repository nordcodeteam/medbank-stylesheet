<script type="text/javascript">
    window.validationTranslations = {
        required: '{{ trans('validation.required') }}',
        email: '{{ trans('validation.email') }}',
        url: '{{ trans('validation.url') }}',
        tel: '{{ trans('validation.tel') }}',
        maxLength: '{{ trans('validation.max_length') }}',
        minLength: '{{ trans('validation.min_length') }}',
        maxFileSize: '{{ trans('validation.max_file_size') }}',
        image: '{{ trans('validation.image') }}',
        minImageDimensions: '{{ trans('validation.min_image_dimensions') }}',
        maxImageDimensions: '{{ trans('validation.max_image_dimensions') }}',
        requiredFromList: '{{ trans('validation.required_from_list') }}',
        confirmation: '{{ trans('validation.confirmation') }}',
        minOptions: '{{ trans('validation.min_options') }}',
        minValue: '{{ trans('validation.min_value') }}',
        maxValue: '{{ trans('validation.max_value') }}',
        companyCode: '{{ trans('validation.company_code') }}',
        personsName: '{{ trans('validation.persons_name') }}',
        documentNumber: '{{ trans('validation.document_number') }}',
        documentDates: '{{ trans('validation.document_dates') }}',
        identificationCode: '{{ trans('validation.identification_code') }}',
        vatCode: '{{ trans('validation.vat_code') }}',
        password: '{{ trans('validation.bad_passowrd') }}',
        uniqueNumberNotUnique: '{{ trans('validation.unique_number_not_unique') }}',
        iban: '{{ trans('validation.bad_iban') }}'
    };

    window.isingTranslations = {
        success: '{{ trans('ising.success_will_be_redirected') }}',
        error: '{{ trans('ising.something_went_wrong')  }}'
    };

    window.flashTranslations = {
        infoOfferNotEnough: '{{ trans('flash_messages.step1_offer_not_below_minimum_info') }}',
        errorOfferNotEnough: '{{ trans('flash_messages.step1_offer_not_below_minimum_error') }}',
        onDocumentsLoad: '{{ trans('flash_messages.step5_on_documents_load_info') }}',
    };
</script>