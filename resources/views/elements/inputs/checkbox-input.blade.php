<div data-input-module="checkbox-input" class="form-group checkbox-input {{ ! empty($error) ? 'checkbox-input---with-error' : '' }}">
    <input class="checkbox-input--field" type="checkbox" id="{{ $id }}" value="{{ $value }}" name="{{ $id }}" {{! empty($rules) ? $rules : ''}} {{ !empty($checked) ? 'checked' : '' }} />
    <label class="checkbox-input--label" for="{{ $id }}">
        <span class="checkbox-input--checkbox">{!! renderSvg($iconsPath . 'tick.svg') !!}</span>
        {!! ! empty($label) ? $label : '' !!}
    </label>

    @if(! empty($error))
        <div class="checkbox-input--error">
            <div id="{{ $id }}-validation" class="checkbox-input--error-text">
                {{ is_array($error) ? implode(', ', $error) : $error }}
            </div>
            <div class="checkbox-input--error-icon"></div>
        </div>
    @endif
</div>