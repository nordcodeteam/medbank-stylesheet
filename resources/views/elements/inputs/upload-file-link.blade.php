<div class="form-group upload-file-link {{ ! empty($error) ? 'primary-input---with-error' : '' }} {{ ! empty($disabled) ? 'primary-input---disabled' : '' }} {{ ! empty($iconPath) ? 'primary-input---with-icon' : '' }} {!! ! empty($modifiers) ? $modifiers : '' !!}">

    {{-- Label --}}
    <label for="{{ $id }}" class="upload-file-link--label">{{ $value }}</label>

    {{-- input itself--}}
    <input type="file" name="{{ !empty($name) ? $name : $id }}" id="{{ $id }}" class="upload-file-link--field" {!! !empty($attributes) ? $attributes : '' !!} {!! !empty($rules) ? $rules : '' !!}>
</div>