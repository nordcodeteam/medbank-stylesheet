@inject('svg', 'App\Injections\SvgService')

<div data-input-module="primary-input" class="form-group primary-input {{ ! empty($error) ? 'primary-input---with-error' : '' }} {{ ! empty($disabled) ? 'primary-input---disabled' : '' }} {{ ! empty($iconPath) ? 'primary-input---with-icon' : '' }} {!! ! empty($modifiers) ? $modifiers : '' !!}">

    {{-- input itself--}}
    <input class="primary-input--field" id="{{ $id }}" name="{{ !empty($name)? $name : $id }}" type="{{ !empty($type) ? $type : 'text' }}" {{ isset($rules) ? $rules : '' }} value="{{ !empty($value) ? $value : '' }}" onkeyup="this.setAttribute('value', this.value);" onchange="this.setAttribute('value', this.value);" {{ ! empty($disabled) ? 'disabled' : '' }}/>

    {{-- Label --}}
    <label class="primary-input--label" for="{{ $id }}">{{ ! empty($label) ?  $label : '' }}</label>
    <div class="primary-input--label-line-break"></div>

    {{-- Decoration underline--}}
    <div class="primary-input--decoration"></div>

    {{-- Input's prefx and suffix--}}
    @if(!empty($prefix))
        <div class="primary-input--prefix">{{ $prefix }}</div>
        <div class="primary-input--fake-label-prefix">{{ $prefix }}</div>
    @endif

    @if(!empty($suffix) || !empty($passwordEye))
        <div class="primary-input--suffix">
            @if(empty($passwordEye))
                {!! !empty($suffix) ? $suffix : '' !!}
            @else
                <div class="primary-input--password-eye" onclick="window.showPassword(this)">
                    {!! renderSvg($iconsPath . 'eye.svg') !!}
                </div>
            @endif
        </div>
    @endif

    {{-- Prefixed icon --}}
    @if(! empty($iconPath))
        <div class="primary-input--icon">
            {!! $svg->render($iconPath) !!}
        </div>
    @endif

    {{-- after decoration--}}
    @if(! empty($error))
        <div class="primary-input--error">
            <div class="primary-input--error-text">
                {{ is_array($error) ? implode(', ', $error) : $error }}
            </div>
            <div class="primary-input--error-icon"></div>
        </div>
    @endif

    @if(isset($hint))
        <div class="primary-input--hint">{!! $hint !!}</div>
    @endif
</div>


<script type="text/javascript">
    if(typeof window.showPassword === 'undefined') {
        window.showPassword = function(eye) {
            var field = eye.parentNode.parentNode.querySelector('input');

            if(field.getAttribute('type') === 'password') {
                eye.classList.add('primary-input--password-eye---active');
                field.setAttribute('type', 'text');
            } else {
                eye.classList.remove('primary-input--password-eye---active');
                field.setAttribute('type', 'password');
            }
        }
    }
</script>