@inject('svg', 'App\Injections\SvgService')

<div data-input-module="alternative-input" class="alternative-input {{ ! empty($error) ? 'alternative-input---with-error' : '' }} {{ ! empty($disabled) ? 'alternative-input---disabled' : '' }} {{ ! empty($iconPath) ? 'alternative-input---with-icon' : '' }}">

    <div class="alternative-input--box">
        {{-- input itself--}}
        <{{(empty($type) || $type !== 'number') ? 'textarea' : 'input' }} class="alternative-input--field" id="{{ $id }}" name="{{ (! empty($name)) ? $name : $id }}" type="{{ !empty($type) ? $type : 'text' }}" {{ isset($rules) ? $rules : '' }} value="{{ !empty($value) ? $value : '' }}" onkeyup="this.setAttribute('value', this.value); auto_grow(this)" {{ ! empty($disabled) ? 'disabled' : '' }}>{{ !empty($value) && (empty($type) || $type !== 'number') ? $value : '' }} {!! (empty($type) || $type !== 'number') ? '</textarea>' : '' !!}

        {{-- Label --}}
        <label class="alternative-input--label" for="{{ $id }}">{{ $label }}</label>
        <div class="alternative-input--label-line-break"></div>

        {{-- Decoration underline--}}
        <div class="alternative-input--decoration"></div>

        {{-- Input's prefx and suffix--}}
        <div class="alternative-input--prefix {{empty($prefix) ? 'alternative-input--prefix---fake' : ''}}">{{ !empty($prefix) ? $prefix : '' }}</div>

        @if(!empty($suffix))
            <div class="alternative-input--suffix">{{ $suffix }}</div>
        @endif

        {{-- Prefixed icon --}}
        @if(! empty($iconPath))
            <div class="alternative-input--icon">
                {!! $svg->render($iconPath) !!}
            </div>
        @endif
    </div>


    {{-- after decoration--}}
    @if(isset($hint))
        <div class="alternative-input--hint">{{ $hint }}</div>
    @endif

    @if(empty($hint) && (!empty($hintLeft) || !empty($hintRight)))
        <div class="alternative-input--hint-group">
            <div class="alternative-input--hint-left">{{ $hintLeft }}</div>
            <div class="alternative-input--hint-right">{{ $hintRight }}</div>
        </div>
    @endif

    @if(! empty($error))
        <div class="alternative-input--error">
            <div id="{{ $id }}-validation" class="alternative-input--error-text">
                {{ is_array($error) ? implode(', ', $error) : $error }}
            </div>
            <div class="alternative-input--error-icon"></div>
        </div>
    @endif


</div>

<script type="text/javascript">
    function auto_grow(element) {

        element.style.height = "42px";
        element.style.height = (element.scrollHeight)+"px";
    }
</script>