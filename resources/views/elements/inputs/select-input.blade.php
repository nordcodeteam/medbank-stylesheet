@inject('svg', 'App\Injections\SvgService')

<div data-input-module="primary-input" class="form-group primary-input primary-input---select {{ ! empty($error) ? 'primary-input---with-error' : '' }} {{ ! empty($disabled) ? 'primary-input---disabled' : '' }} {{ ! empty($iconPath) ? 'primary-input---with-icon' : '' }}">

    {{-- input itself--}}
    <select data-toggle="blur" class="primary-input--field" {!! ! empty($attributes) ? $attributes : '' !!} id="{{ $id }}" name="{{ $id }}" {{ isset($rules) ? $rules : '' }} value="{{ !empty($selected) ? $selected : '' }}" onchange="this.setAttribute('value', this.options[this.selectedIndex].value);" {{ ! empty($disabled) ? 'disabled' : '' }}>


        @if(! empty($options))
            @foreach($options as $key => $value)

                @if(is_array($value))
                    <optgroup label="{{ $key }}">
                        @foreach($value as $_key => $_value)
                            <option value="{{ $_key }}" {{!empty($selected) && $selected == $_key ? 'selected' : ''}}>{{ $_value }}</option>
                        @endforeach
                    </optgroup>
                @else

                <option {{ $loop->first ? 'disabled' : '' }} value="{{ $key }}" {{(!empty($selected) && $selected == $key) || (empty($selected) && $loop->first) ? 'selected' : ''}}>{{ $value }}</option>
                @endif
            @endforeach
        @endif
    </select>

    {{-- Label --}}
    <label class="primary-input--label" for="{{ $id }}">{{ $label }}</label>
    <div class="primary-input--label-line-break"></div>

    {{-- Decoration underline--}}
    <div class="primary-input--decoration"></div>

    {{-- Prefixed icon --}}
    @if(! empty($iconPath))
        <div class="primary-input--icon">
            {!! $svg->render($iconPath) !!}
        </div>
    @endif

    {{-- Caret underline--}}
    <div class="primary-input--caret">
        {!! $svg->render((!empty($iconsPath) ? $iconsPath : config('frontend.iconsPath')) . 'caret-down.svg') !!}
    </div>

    {{-- after decoration--}}
    @if(! empty($error))
        <div class="primary-input--error">
            <div id="{{ $id }}-validation" class="primary-input--error-text">
                {{ is_array($error) ? implode(', ', $error) : $error }}
            </div>
            <div class="primary-input--error-icon"></div>
        </div>
    @endif

    @if(isset($hint))
        <div class="primary-input--hint">{{ $hint }}</div>
    @endif
</div>