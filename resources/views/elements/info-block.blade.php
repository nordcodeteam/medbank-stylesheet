<div class="info-block {!! ! empty($modifiers) ? $modifiers : '' !!}">
    <div class="info-block--label">{!! $label !!}</div>
    <div class="info-block--value" {!! ! empty($valueAttributes) ? $valueAttributes : '' !!}>{!! $value !!}</div>
    <div class="info-block--icon">{!! $icon !!}</div>
</div>



