<div class="file-upload-control--file">
    <div class="file-upload-control--file-icon">
        {!! renderSvg($iconsPath . 'document-in-circle.svg') !!}
    </div>
    <div class="file-upload-control--file-name"></div>
    <div class="file-upload-control--file-remove">
        <div class="cross"></div>
    </div>
    <div class="file-upload-control--file-preloader">
        {!! renderSvg($iconsPath . 'preloader.svg') !!}
    </div>
</div>