<{{ !empty($href) ? 'a' : 'button' }}
    {{ !empty($type) ? 'type=' . $type : '' }}
    {!! !empty($href) ? 'href="' . $href . '"' : '' !!}
    {{ ! empty($name) ? 'name=' . $name : '' }}
    {{ ! empty($id) ? 'id=' . $id : '' }}
    class="primary-button {{ ! empty($modifiers) ? $modifiers : '' }}"
    {!! ! empty($attributes) ? $attributes : '' !!}
>
    {{-- Three elements static button --}}
    @if(! empty($prefix))
        <span class="primary-button--prefix">{!! $prefix !!}</span>
    @endif
    @if(! empty($value))
        <span class="primary-button--value {{ ! empty($valueModifiers) ? $valueModifiers : '' }}">{!! $value !!}</span>
    @endif
    @if(! empty($suffix))
        <span class="primary-button--suffix">{!! $suffix !!}</span>
    @endif

    {{-- Two elements animation --}}
    @if(! empty($primaryValue))
        <span class="primary-button--primary-value">{!! $primaryValue !!}</span>
    @endif
    @if(! empty($secondaryValue))
        <span class="primary-button--secondary-value">{!! $secondaryValue !!}</span>
    @endif
</{{ ! empty($href) ? 'a' : 'button' }}>