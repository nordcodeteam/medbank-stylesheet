<button {{ !empty($dataAttributes) ? $dataAttributes : '' }} {{! empty($name) ? 'name=' . $name : ''}} {{isset($value) ? 'value=' . $value : ''}} class="animated-button  {{ !empty($modifiers) ? $modifiers : '' }}">
    <div class="animated-button--secondary-value">
        {!! $secondaryValue !!}
    </div>
    <div class="animated-button--primary-value">
        {!! $primaryValue !!}
    </div>
</button>