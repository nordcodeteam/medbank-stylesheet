<div class="wait">
    @if(isset($preloader))
        <div class="wait--preloader">
            {!! renderSvg($iconsPath . 'preloader.svg') !!}
        </div>
    @endif

    @if(!empty($mainMessage))
        <div class="wait--main-message">{{ $mainMessage }}</div>
    @endif
</div>

@if(!empty($url) && !empty($callAgain))
    <script type="text/javascript">
        window.wait = {
            url: '{{ $url }}',
            callAgain: '{{ $callAgain }}',
            _token: '{{ csrf_token() }}'
        }
    </script>
@endif