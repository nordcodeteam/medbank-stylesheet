<div class="primary-panel">
    @if(! empty($panelHeading))
        <div class="primary-panel--heading">
            <h3 class="h3">{{ $panelHeading }}</h3>
        </div>
    @endif

    <div class="primary-panel--content">
        <div class="primary-panel--buttons">
            @foreach($files as $file)
            <div class="primary-panel--button">
                    @include('elements.buttons.primary-button', [
                        'value' => $file['value'],
                        'href' => $file['href'],
                        'prefix' => renderSvg($iconsPath . 'download.svg'),
                        'modifiers' => 'primary-button---download-button',
                        'attributes' => 'target="_blank"'
                    ])
                </div>
            @endforeach
        </div>
    </div>
</div>