<div id="flash-info">
    {!! renderSvg($iconsPath . 'flash-info.svg') !!}
</div>
<div id="flash-document">
    {!! renderSvg($iconsPath . 'flash-document.svg') !!}
</div>