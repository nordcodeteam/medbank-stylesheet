<div class="alternative-panel {!! ! empty($modifiers) ? $modifiers : ''!!}">
    @if(! empty($heading))
        <div class="alternative-panel--heading">
            <h3 class="h3 h3---invert">{!! $heading !!}</h3>
        </div>
    @endif
    @if(! empty($subheading))
        <div class="alternative-panel--subheading">
            {!! $subheading !!}
        </div>
    @endif
    <div class="alternative-panel--amount">
        <div class="alternative-panel--amount-value" {!! ! empty($amountValueAttributes) ? $amountValueAttributes : '' !!}>{!! ! empty($amountValue) ? $amountValue : '' !!}</div>

        <div class="alternative-panel--amount-suffix">
            {!! ! empty($amountSuffix) ? $amountSuffix : '' !!}
        </div>
    </div>
</div>