<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/loan-for-RE.css') }}">
</head>
<body>

<header>
    @include('loan-for-the-real-estate::application.elements.manager-header-brand-row')
    @include('loan-for-the-real-estate::application.elements.manager-header-info-row', [
        'pageTitle'  => 'Title',
        'www'        => true
     ])

    @include('loan-for-the-real-estate::application.elements.header-brand-row')
    @include('loan-for-the-real-estate::application.elements.header-info-row', [
        'step' => 1
    ])

    <div class="header-primary-data-info-row">
        <div class="header-primary-data-info-row--content">
            <div class="header-primary-data-info-row--item">
                @include('elements.info-block', [
                    'label' => 'Pavadinimas',
                    'value' => '500 ' . $currency,
                    'valueAttributes' => 'data-format="currency"',
                    'icon' => renderSvg($iconsPath . $currency . '-in-circle.svg'),
                ])
            </div>
            <div class="header-primary-data-info-row--item">
                @include('elements.info-block', [
                    'label' => '<span>' . trans('loan_for_the_real_estate_page.loan_period') . '</span><span>' . trans('loan_for_the_real_estate_page.loan_period_short') . '</span>',
                    'value' => '12 ' . trans('loan_for_the_real_estate_page.short_month'),
                    'icon' => renderSvg($iconsPath . 'clock-in-circle.svg'),
                ])
            </div>

            <div class="header-primary-data-info-row--item">
                @include('elements.buttons.primary-button', [
                        'value' => renderSvg($iconsPath . 'pencil.svg'),
                        'modifiers' => 'primary-button---roundish',
                        'attributes' => 'data-print=".policy-popup--content" data-dropdown="edit-links"'
                    ])
                <div data-dropdown-id="edit-links" class="dropdown">
                    <a href="#" class="dropdown--item">Link</a>
                    <a href="#" class="dropdown--item">Link</a>
                    <a href="#" class="dropdown--item">Link</a>
                    <a href="#" class="dropdown--item">Link</a>
                </div>
            </div>

        </div>
    </div>
</header>


@include('elements.wait', [
    'url' => route('open'),
    'callAgain' => 2000,
    'mainMessage' => 'optional main message',
    'preloader' => true
])


<main>
    @yield('content')
</main>

<div class="footer">
    <div class="footer--content">
        <div class="footer--copyright">
            {{ trans('loan_for_the_real_estate_page.copyright') }}
        </div>
        <div class="footer--navigation">
            <div class="footer--navigation-item">
                <button class="primary-link" data-popup-trigger="privacyPolicy">{{ trans('loan_for_the_real_estate_actions.privacy_policy') }}</button>
            </div>
            <div class="footer--navigation-item">
                <button class="primary-link" data-popup-trigger="privacyPolicy">{{ trans('loan_for_the_real_estate_actions.privacy_policy') }}</button>
            </div>
            <div class="footer--navigation-item">
                <button class="primary-link" data-popup-trigger="privacyPolicy">{{ trans('loan_for_the_real_estate_actions.privacy_policy') }}</button>
            </div>
        </div>
        <div class="footer--help">
            <div class="footer--help-label">
                {{ trans('loan_for_the_real_estate_page.call_center_number_label') }}
            </div>
            <div class="footer--help-button">
                @include('elements.buttons.primary-button', [
                    'prefix' => renderSvg($iconsPath . 'phone-in-circle.svg'),
                    'value' => '<a href="tel:' . trans('loan_for_the_real_estate_actions.call_center_number') . '">' .trans('loan_for_the_real_estate_actions.call_center_number') . '</a>',
                    'modifiers' => 'primary-button---mobile-only-prefix',
                    'valueModifiers' => 'primary-button--value---mobile-number'
                ])
            </div>
        </div>
    </div>
</div>

{{-- prefabs --}}
<div class="prefab">
    @include('elements.file-upload-control')
    @include('elements.flash-icons')
    @include('popups.policy-popup')
    @include('elements.prefab-preloader')
</div>


{{-- varibles --}}
@include('elements.inputs.js-validation-translations')

<script type="text/javascript">
    window.flashMessages = [
        {
            type: 'info',
            icon: 'flash-document',
            message: 'test info message content'
        },
        {
            type: 'warning',
            icon: 'flash-info',
            message: 'test info message content'
        },
        {
            type: 'success',
            icon: 'flash-info',
            message: 'test info message content'
        }
    ];

    window.currency = '€';
    window.currencyLetters = 'Eur';

    window.popupData = {
        privacyPolicy : {module: 'policy-popup'}
    };
</script>

{{-- Scripts --}}
@yield('scripts')
<script type="text/javascript">
    window['products.loan.upload_limits.client_report'] = '{{ config('products.loan.upload_limits.client_report') }}';
</script>
<script type="text/javascript" src="{{ asset('js/form.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/public.bundle.js') }}"></script>

</body>
</html>