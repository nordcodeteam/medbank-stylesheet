@extends($defaultViewPrefix . 'stylesheet')

@section('content')
    <div class="content-wrapper">
        <div class="page-title">
            <h1 class="h1">Heading 1</h1>
        </div>
        <div class="page-title">
            <h2 class="h3">Heading 3</h2>
        </div>
        <div class="page-title">
            <h3 class="h4">Heading 4</h3>
        </div>

        <div class="page-title">
            <p class="paragraph">
                Paaragraph
            </p>
        </div>

        <div class="notice notice---standalone">
            <div class="notice--icon">
                {!! renderSvg($iconsPath . 'exclamation-mark.svg') !!}
            </div>
            <div class="notice--content">
                Notice
            </div>
        </div>

        <div class="notice notice---standalone">
            <div class="notice--icon notice--icon---alternative">
                {!! renderSvg($iconsPath . 'exclamation-mark.svg') !!}
            </div>
            <div class="notice--content">
                Notice icon alternative
            </div>
        </div>



        <div class="grid">
            <div class="grid--wrapper">
                <div class="grid--two-thirds">
                    <div class="primary-panel">
                        <div class="primary-panel--heading">
                            <h3 class="h3">Heading</h3>
                        </div>
                        <div class="primary-panel--content">
                            <div class="inputs-grid">
                                <div class="inputs-grid--full">
                                    @include('elements.inputs.primary-input', [
                                        'id' => 'id1',
                                        'label' => 'label',
                                        'iconPath' => $iconsPath . 'document-in-circle.svg'
                                    ])
                                </div>
                                <div class="inputs-grid--half">
                                    @include('elements.inputs.primary-input', [
                                        'id' => 'id2',
                                        'label' => 'label',
                                        'prefix' => 'prefix'
                                    ])
                                </div>
                                <div class="inputs-grid--half">
                                    @include('elements.inputs.primary-input', [
                                        'id' => 'id3',
                                        'label' => 'label',
                                        'prefix' => 'prefix',
                                        'suffix' => 'suffix',
                                        'disabled' => true
                                    ])
                                </div>
                                <div class="inputs-grid--half">
                                    @include('elements.inputs.primary-input', [
                                        'id' => 'id4',
                                        'label' => 'label',
                                        'type' => 'password',
                                        'passwordEye' => true
                                    ])
                                </div>
                                <div class="inputs-grid--half">
                                    @include('elements.inputs.primary-input', [
                                        'id' => 'id5',
                                        'label' => 'label',
                                        'prefix' => 'prefix',
                                        'type' => 'password',
                                        'passwordEye' => true
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid--one-third">
                    <div class="primary-panel">
                        <div class="primary-panel--heading primary-panel--heading---alternative">
                            <h3 class="h3 h3---invert">Heading</h3>
                        </div>
                        <div class="primary-panel--content">
                            <p class="paragraph">Paragraph</p>
                        </div>
                    </div>
                </div>
                <div class="grid--half">
                    <div class="primary-panel">
                        <div class="primary-panel--heading">
                            <h3 class="h3">Heading</h3>
                        </div>
                        <div class="primary-panel--content">
                            <div class="inputs-grid">
                                <div class="inputs-grid--half">
                                    <div class="confirm-data">
                                        <div class="confirm-data--label">
                                            Label
                                        </div>
                                        <div class="confirm-data--value">
                                            Value
                                        </div>
                                    </div>
                                </div>
                                <div class="inputs-grid--half">
                                    <div class="confirm-data">
                                        <div class="confirm-data--label">
                                            Label
                                        </div>
                                        <div class="confirm-data--value">
                                            Value
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid--half">
                    <div class="primary-panel">
                        <div class="primary-panel--heading primary-panel--heading---alternative">
                            <h4 class="h4 h4---invert">Heading</h4>
                        </div>
                        <div class="primary-panel--content">
                            <h4 class="h4">
                                <div class="h4--icon">{!! renderSvg($iconsPath . 'document-in-circle.svg') !!}</div>
                                <div class="h4--text">Heading</div>
                            </h4>

                        </div>
                    </div>
                </div>
                <div class="grid--full">
                    <div class="primary-panel primary-panel---with-sticking-elements">
                        <div class="primary-panel--heading">
                            <h4 class="h4">Centered inputs grid</h4>
                        </div>
                        <div class="primary-panel--content">
                            <div class="inputs-grid inputs-grid---centered">
                                <div class="inputs-grid--third">
                                    @include('elements.inputs.select-input', [
                                        'id' => 'id6',
                                        'label' => 'label',
                                        'options' => ['--', 'a', 'b'],
                                        'disabled' => true,
                                        'iconPath' => $iconsPath . 'document-in-circle.svg'
                                    ])
                                </div>
                                <div class="inputs-grid--third">
                                    @include('elements.inputs.select-input', [
                                        'id' => 'id7',
                                        'label' => 'label',
                                        'options' => ['--', 'a', 'b'],
                                        'selected' => 1,
                                        'iconPath' => $iconsPath . 'document-in-circle.svg'
                                    ])
                                </div>
                                <div class="inputs-grid--third">
                                    @include('elements.inputs.checkbox-input', [
                                        'id' => 'id8',
                                        'label' => 'label',
                                        'value' => '1'
                                    ])
                                </div>
                                <div class="inputs-grid--fourth">
                                    @include('elements.inputs.select-input', [
                                        'id' => 'id9',
                                        'label' => 'label',
                                        'options' => ['--', 'a', 'b'],
                                        'disabled' => true
                                    ])
                                </div>
                                <div class="inputs-grid--fourth">
                                    @include('elements.inputs.select-input', [
                                        'id' => 'id10',
                                        'label' => 'label',
                                        'options' => ['--', 'a', 'b'],
                                        'selected' => 1
                                    ])
                                </div>
                                <div class="inputs-grid--fourth">
                                    @include('elements.inputs.select-input', [
                                        'id' => 'id11',
                                        'label' => 'label',
                                        'options' => ['--', 'a', 'b'],
                                    ])
                                </div>
                                <div class="inputs-grid--fourth">
                                    @include('elements.inputs.checkbox-input', [
                                        'id' => 'id12',
                                        'label' => 'label',
                                        'value' => '1'
                                    ])
                                </div>
                            </div>

                            <div class="primary-panel--separator"></div>

                            <div class="property-content--add-button">
                                @include('elements.buttons.primary-button', [
                                     'primaryValue' => renderSvg($iconsPath . 'plus.svg'),
                                     'secondaryValue' => 'Pridėti',
                                     'type' => 'button'
                                 ])
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid--one-third">
                    @include('elements.alternative-panel', [
                        'heading' => 'Heading',
                        'subheading' => 'subheading',
                        'amountValue' => 100,
                        'amountValueAttributes' => 'data-format="currency"',
                        'amountSuffix' => renderSvg($iconsPath . $currency . '-in-circle.svg')
                    ])
                </div>

                <div class="grid--one-third">
                    @include('elements.alternative-panel', [
                        'heading' => 'Heading',
                        'subheading' => 'subheading',
                        'amountValue' => 100,
                        'amountValueAttributes' => 'data-format="currency"',
                        'amountSuffix' => renderSvg($iconsPath . $currency . '-in-circle.svg'),
                        'modifiers' => 'alternative-panel---secondary'
                    ])
                </div>

                <div class="grid--one-third">
                    @include('elements.alternative-panel', [
                        'heading' => 'Heading',
                        'subheading' => 'subheading',
                        'amountValue' => 100,
                        'amountValueAttributes' => 'data-format="currency"',
                        'amountSuffix' => renderSvg($iconsPath . $currency . '-in-circle.svg'),
                        'modifiers' => 'alternative-panel---secondary alternative-panel---small'
                    ])
                </div>
            </div>
        </div>

        <div class="form-buttons">
            <div class="form-buttons--wrapper">
                <div class="form-buttons--button">
                    @include('elements.buttons.primary-button', [
                        'value' => 'button'
                    ])
                </div>
                <div class="form-buttons--button">
                    @include('elements.buttons.primary-button', [
                        'value' => 'button',
                        'prefix' => renderSvg($iconsPath . 'document-in-circle.svg')
                    ])
                </div>
                <div class="form-buttons--button">
                    @include('elements.buttons.primary-button', [
                        'value' => 'button',
                        'suffix' => renderSvg($iconsPath . 'arrow-right.svg')
                    ])
                </div>
                <div class="form-buttons--button">
                    @include('elements.buttons.animated-button', [
                        'value' => 'button',
                        'primaryValue' => '1st',
                        'secondaryValue' => '2nd'
                    ])
                </div>
                <div class="form-buttons--button">
                @include('elements.buttons.animated-button', [
                    'value' => 'button',
                    'primaryValue' => '1st',
                    'secondaryValue' => '2nd',
                    'modifiers' => 'animated-button---clicked'
                ])
            </div>
            </div>
        </div>

        <div class="page-content">
            @include('elements.file-box', [
            'panelHeading' => 'Heading',
            'files' => [
                [
                    'value' => 'Value',
                    'href' => '#'
                ],
                [
                    'value' => 'Value',
                    'href' => '#'
                ],
            ]
        ])

            <div class="application-processing">
                <div class="application-processing--status application-processing--status---completed">
                    <div class="application-processing--completed">
                        {!! renderSvg($iconsPath . 'tick.svg') !!}
                    </div>
                    <div class="application-processing--processing">
                        {!! renderSvg($iconsPath . 'preloader.svg') !!}
                        {!! renderSvg($iconsPath . 'preloader_oval.svg') !!}
                    </div>
                    <div class="application-processing--step-number">
                        1
                    </div>
                </div>

                <div class="application-processing--label">
                    Label
                </div>
            </div>

            <div class="application-processing">
                <div class="application-processing--status application-processing--status---processing">
                    <div class="application-processing--completed">
                        {!! renderSvg($iconsPath . 'tick.svg') !!}
                    </div>
                    <div class="application-processing--processing">
                        {!! renderSvg($iconsPath . 'preloader.svg') !!}
                        {!! renderSvg($iconsPath . 'preloader_oval.svg') !!}
                    </div>
                    <div class="application-processing--step-number">
                        1
                    </div>
                </div>

                <div class="application-processing--label">
                    Label
                </div>
            </div>

            <div class="application-processing">
                <div class="application-processing--status application-processing--status---idle">
                    <div class="application-processing--completed">
                        {!! renderSvg($iconsPath . 'tick.svg') !!}
                    </div>
                    <div class="application-processing--processing">
                        {!! renderSvg($iconsPath . 'preloader.svg') !!}
                        {!! renderSvg($iconsPath . 'preloader_oval.svg') !!}
                    </div>
                    <div class="application-processing--step-number">
                        1
                    </div>
                </div>

                <div class="application-processing--label">
                    Label
                </div>
            </div>
        </div>

    </div>
@stop