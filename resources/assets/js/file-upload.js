import {JS} from "./functions";

export const validationConfig = {
    allowedTypes: [
        'application/pdf',
        'image/png'
    ]
};

export const Validators = {
    sizelimit(file, input) {
        if(input.hasAttribute('sizelimit')) {
            if(file.size > input.getAttribute('sizelimit') * 1024) {
                return {type: 'sizelimit'};
            }
        }

        return false;
    },

    alowedtypes(file) {
        let hasErrors = true;

        for(let i = 0; i < validationConfig.allowedTypes.length; i++) {
            if(file.type === validationConfig.allowedTypes[i]) {
                hasErrors = false;
            }
        }

        return hasErrors;
    },

    uploadlimit() {
        let fileEl = JS.selectAll('.file-upload-control--file');
        let count = 0;

        for(let i = 0; i < fileEl.length; i++) {
            if(fileEl[i].parentNode.getAttribute('class') !== 'prefab') {
                count++;
            }
        }

        return count > (window['products.loan.upload_limits.client_report'] || 10);
    }
};

export const FileControls = {
    getHtml(file) {
        let fileEl = JS.selectFirst('.prefab .file-upload-control--file').cloneNode(true);

        let name = JS.selectFirst('.file-upload-control--file-name', fileEl);
        name.innerHTML = file.name;

        return fileEl;
    },

    createFileElement(file, module) {
        let fileEl = this.getHtml(file);

        module.appendChild(fileEl);

        return fileEl;
    },

    removePreloader(node) {
        let preloader = JS.selectFirst('.file-upload-control--file-preloader', node);
        JS.addClass(preloader, 'file-upload-control--file-preloader---loaded');
    },

    initRemoveButton(node, id) {
        let removeEl = JS.selectFirst('.file-upload-control--file-remove', node);
        JS.addClass(removeEl, 'file-upload-control--file-remove---inited');

        let cross = JS.selectFirst('.cross', removeEl);
        cross.setAttribute('id', id);

        JS.addEvent(cross, 'click', () => this.handleRemoveButtonClick(cross));
    },

    initRemoveButtonInDOM() {
        let crosses = JS.selectAll('.file-upload-control--file-remove .cross[id]');

        for(let i = 0; i < crosses.length; i++) {
            JS.addEvent(crosses[i], 'click', () => this.handleRemoveButtonClick(crosses[i]));
        }
    },

    handleRemoveButtonClick(cross) {
        let fileEl = cross.closest('.file-upload-control--file');
        let form = cross.closest('form');

        let id = cross.getAttribute('id');
        let processor = new FileProcessor(cross.getAttribute('id'), form);

        console.log(form);

        let preloader = JS.selectFirst('.file-upload-control--file-preloader', fileEl);
        if(preloader) {
            preloader.setAttribute('class', 'file-upload-control--file-preloader');
            cross.parentNode.setAttribute('class', 'file-upload-control--file-remove');
        }

        processor.removeFile(id).then(() => {
            FileControls.removeFileElement(fileEl);
        }).catch((reason) => {
            /* remove preloader and re-init remove cross */
            if(preloader) {
                cross.parentNode.setAttribute('class', 'file-upload-control--file-remove file-upload-control--file-remove---inited');
                preloader.setAttribute('class', 'file-upload-control--file-preloader file-upload-control--file-preloader---loaded');
            }
            console.log('error deleting file');
        });
    },

    removeFileElement(node) {
        node.parentNode.removeChild(node);
    }

};

export class FileProcessor {
    constructor(fileListiner, form) {
        this.form = form;
        this.controlModule = JS.selectFirst('[data-file-listener="' + fileListiner + '"]');
    }

    hasError(file, input) {
        let errors = [];

        const validators = Object.keys(Validators);
        for(let i = 0; i < validators.length; i++) {
            let response;
            if(response = Validators[validators[i]](file, input)) {
                errors.push(response);
            }
        }

        if(errors.length > 0) {
            return errors;
        }

        return false;
    }

    displayError(error) {
        console.log('display error: ', error);
    }

    getToken() {
        const token = JS.selectFirst('[name="_token"]', this.form);

        return token.value;
    }

    uploadFile(file) {
        let fileEl = FileControls.createFileElement(file, this.controlModule);
        let form_data = new FormData();
        form_data.append("file", file);

        const url = this.form.getAttribute('action');
        const _token = this.getToken();

        let xhr = JS.getXhr('POST', url);

        xhr.setRequestHeader('X-CSRF-TOKEN', _token);
        xhr.onreadystatechange = () => this.handleReadyStateChange(xhr, fileEl);
        xhr.send(form_data);
    }

    removeFile(id, form) {
        console.log(this.form, form);
        return new Promise((resolve, reject) => {
            const url = this.form.getAttribute('action');
            const _token = this.getToken();
            let xhr = JS.getXhr('DELETE', url + '?id=' + id);

            xhr.setRequestHeader('X-CSRF-TOKEN', _token);
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        const response = JSON.parse(xhr.response);

                        if(response.status === 'ok') {
                            resolve();
                        } else {
                            reject();
                        }
                    } else {
                        reject();
                    }
                }
            };
            xhr.send(null);
        });
    }

    handleReadyStateChange(xhr, fileEl) {
        if (xhr.readyState === 4){
            if (xhr.status === 200) {
                this.handleResponse(JSON.parse(xhr.response), fileEl);
            } else {
                /* todo: add flash message */
                console.log('error uploading file');

                FileControls.removeFileElement(fileEl);
            }
        }
    }

    handleResponse(response, fileEl) {
        if(response['status'] === 'ok') {
            FileControls.removePreloader(fileEl);
            FileControls.initRemoveButton(fileEl, response['id']);
        } else if(response['status'] === 'error') {
            /* todo: add flash message */
            console.log('error uploading file');
            FileControls.removeFileElement(fileEl);
        }
    }

    processFile(file, input) {
        let error;

        if(error = this.hasError(file, input)) {
            this.displayError(error);
        } else {
            this.uploadFile(file);
        }
    }
}

export class FileDragAndDrop {
    constructor(input, multiInput) {
        this.multiInput = multiInput;
        this.input = input;
        this.processor = new FileProcessor(input.getAttribute('id'), input.closest('form'));

        this.init();
    }

    init() {
        let fileDragZone;
        if(this.multiInput) {
            fileDragZone = this.input.closest('[data-file-drag-box]')
        } else {
            fileDragZone = JS.selectFirst('html');
        }

        //File has been selected and dropped to form, handle it
        JS.addEvent(this.input, 'change', (e) => this.FileSelectHandler(e));

        // is XHR2 available?
        let xhr = new XMLHttpRequest();
        if (xhr.upload) {
            // file drop
            JS.addEvent(window, 'dragover', (e) => this.highlightDropBoxes(e));
            JS.addEvent(window, 'dragleave', (e) => this.removeHighlightsFromBoxes(e));
            JS.addEvent(fileDragZone, 'drop', (e) => this.FileSelectHandler(e));
            JS.addEvent(window, 'drop', (e) => this.removeHighlightsFromBoxes(e));
        }
    }

    FileSelectHandler(e) {
        e.stopPropagation();
        e.preventDefault();

        let input;

        this.removeHighlightsFromBoxes(e, true);

        // fetch FileList object
        const files = e.target.files || e.dataTransfer.files;

        // process all File objects
        for (let i = 0, f; f = files[i]; i++) {
            input = this.getInput(e.target);
            this.processor.processFile(f, input);
        }

        this.recreateFileUpload(input);
    }

    highlightDropBoxes(e) {
        const self = this;
        e.stopPropagation();
        e.preventDefault();

        let boxes = JS.selectAll('[data-file-drag-box]');
        for(let i = 0; i < boxes.length; i++) {
            boxes[i].setAttribute('data-file-drag-box', 1);
        }
    }

    removeHighlightsFromBoxes(e, force = false) {
        e.stopPropagation();
        e.preventDefault();


        if(e.target.tagName === 'MAIN' || force) {
            let boxes = JS.selectAll('[data-file-drag-box]');
            for (let i = 0; i < boxes.length; i++) {
                boxes[i].setAttribute('data-file-drag-box', 0);
            }
        }
    }

    getInput(section) {
        if(section.getAttribute('type') === 'file') {
            return section;
        }

        let el;
        if(el = JS.selectFirst('[type="file"]', section)) {
            return el;
        }

        if(el = section.closest('[type="file"]')) {
            return el
        }

        if(el = JS.selectFirst('[type="file"]', section.closest('form'))) {
            return el;
        }

        return false;
    }

    recreateFileUpload(input) {
        const populateAttributes = function(orgEl, newEl, attribute) {
            newEl.setAttribute(attribute, orgEl.getAttribute(attribute));
        };

        let newInput = document.createElement('input');

        populateAttributes(input, newInput, 'type');
        populateAttributes(input, newInput, 'name');
        populateAttributes(input, newInput, 'id');
        populateAttributes(input, newInput, 'class');
        populateAttributes(input, newInput, 'data-file-upload');
        populateAttributes(input, newInput, 'multiple');

        JS.addEvent(newInput, 'change', (e) => this.FileSelectHandler(e));

        input.parentNode.appendChild(newInput);
        input.parentNode.removeChild(JS.selectFirst('input', input.parentNode));
    }
}

JS.addEvent(window, 'load', function() {
    let inputGroups = JS.selectAll('[data-file-upload]');
    for(let i = 0; i < inputGroups.length; i++) {
        new FileDragAndDrop(inputGroups[i], (inputGroups.length > 1));
    }
    FileControls.initRemoveButtonInDOM();
});