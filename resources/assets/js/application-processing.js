import {JS} from "./functions";

export const config = {
    isStepsPage: window.applicationProcessingStepsPage,
    ajaxUrl: window.applicationProcessingDataUrl,
};

export class DataFetch {
    constructor() {
        this.url = config.ajaxUrl;
        this.isStepPage = config.isStepsPage;

        if(this.url) {
            this.sendRequest();
        }
    }

    sendRequest() {
        let xhr = new XMLHttpRequest();

        xhr.open('GET', this.url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');;
        xhr.send(null);

        xhr.onreadystatechange = () => this.handleReadyStateChange(xhr);
    }

    handleReadyStateChange(xhr) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let response = JSON.parse(xhr.response);
                this.handleResponse(response);
            } else if(xhr.status === 500) {
                setTimeout(() => this.sendRequest(), 3000);
            }
        }
    }

    handleResponse(response) {
        if(typeof response['callAgain'] !== 'undefined' && response['callAgain'] > 0) {
            setTimeout(() => this.sendRequest(), response['callAgain'])
        }

        if(typeof response['steps'] !== 'undefined' && this.isStepPage) {
            window.applicationProcessingControl.setStepStatuses(response['steps']);
        }else if(typeof response['refresh'] !== 'undefined' && response['refresh'] === true) {
            window.location.reload();
        }
    }
}

/*
* Usage examples:
*   Change status:
*     window.applicationProcessingControl.setStepStatus(4, 'processing');
*     window.applicationProcessingControl.setStepStatus(3, 'completed');
*     window.applicationProcessingControl.setStepStatus(5, 'idle');
* */
export class applicationProcessingControl {
    constructor() {
        this.steps = [];

        let steps = JS.selectAll('[data-process-step]');

        if(steps) {
            for(let i = 0; i < steps.length; i++) {
                this.appendNewStep(steps[i]);
            }
        }
    }

    /*
    * Appends step to structure
    * */
    appendNewStep(step) {
        let key = step.getAttribute('data-process-step');
        let status = applicationProcessingControl.getStepStatus(step);

        if(!key) {
            console.log('Missing data-process-step attribute in:', step);
            return -1;
        }

        if(!status) {
            console.log('Status not parsed from:', step);
            return -1;
        }

        this.steps.push({
            key: key,
            status: status
        });
    }

    static getStepStatus(step) {
        let modifierHolderEl = JS.selectFirst('.application-processing--status' ,step);
        let className = modifierHolderEl.getAttribute('class');

        if(className.indexOf('---completed') >= 0) {
            return 'completed';
        } else if(className.indexOf('---processing') >= 0) {
             return 'processing';
        } else if(className.indexOf('---idle') >= 0) {
            return 'idle';
        }

        return false;
    }

    static getStepElement(stepKey) {
        return JS.selectFirst('[data-process-step="' + stepKey + '"]');
    }

    disableCurrentProcessing() {
        for(let i = 0; i < this.steps.length; i++) {
            if(this.steps[i].status === 'processing') {
                this.setStepStatus(this.steps[i].key, 'completed');
            }
        }
    }

    /*
    * Fetches data from structure
    * */
    getStepData(stepKey) {
        for(let i = 0; i < this.steps.length; i++) {
            if(+this.steps[i].key === +stepKey) {
                return {index: i, data: this.steps[i]};
            }
        }

        return false;
    }

    setStepStatus(stepKey, status) {
        const step = applicationProcessingControl.getStepElement(stepKey);
        let modifierHolderEl = JS.selectFirst('.application-processing--status', step);
        let className = modifierHolderEl.getAttribute('class');
        const currentStepData = this.getStepData(stepKey);

        if(currentStepData) {
            if (status === 'processing') {
                this.disableCurrentProcessing();
            }

            //Generates new classname
            className = className.replace('---' + currentStepData.data.status, '---' + status);
            //Changes class name
            modifierHolderEl.setAttribute('class', className);
            //Updates structure's status attribute
            this.steps[currentStepData.index].status = status;
        } else {
            //console.log('Step not found in applicationProcessingControl.steps structure', 'stepKey:' + stepKey, this.steps);
        }
    }

    setStepStatuses(steps) {
        for(let i = 0; i < steps.length; i++) {
            this.setStepStatus(steps[i].key, steps[i].status);
        }
    }
}



JS.addEvent(window, 'load', function() {
    window.applicationProcessingControl = new applicationProcessingControl();

    new DataFetch();
});