import {JS} from "./functions";

export let TabbedHeader = {
    init(wrapper = null) {
        let self = this;

        let init = function(wrapper) {
            let buttons = JS.selectAll('[data-tabbed-header-button]', wrapper);
            for(let i = 0; i < buttons.length; i++) {
                self.addEvents(buttons[i]);
            }
        };

        if(wrapper === null) {
            let wrappers = JS.selectAll('[data-tabbed-header]');
            for(let i = 0; i < wrappers.length; i++) {
                init(wrappers[i]);
            }
        } else {

        }

        JS.addEvent(window, 'resize', () => this.handleResize());
    },

    addEvents(button) {
        JS.addEvent(button, 'click', () => this.handleSelectionClick(button));
        JS.addEvent(button, 'touch', () => this.handleSelectionClick(button));
    },

    handleResize() {
        if(JS.windows().width < 768) { //mobile

        }  else { // tablet && desktop
            let buttons = JS.selectAll('[data-tabbed-header-button]');
            for (let i = 0; i < buttons.length; i++) {
                JS.removeClass(buttons[i], 'hide');

                if(buttons[i].getAttribute('data-tabbed-header-button') === 'clone') {
                    buttons[i].parentNode.removeChild(buttons[i]);
                }
            }
        }
    },

    handleSelectionClick(clickedButton) {
        let wrapper = clickedButton.parentNode;
        let deleteFirstChild = function() {
            let firstChild = wrapper.firstElementChild;
            if(firstChild.getAttribute('data-tabbed-header-button') === 'clone') {
                wrapper.removeChild(wrapper.firstChild);
            }
        };
        let showOthers = function() {
            if(clickedButton.getAttribute('data-tabbed-header-button') !== 'clone') {
                let buttons = JS.selectAll('[data-tabbed-header-button]', wrapper);
                for (let i = 0; i < buttons.length; i++) {
                    JS.removeClass(buttons[i], 'hide');
                }
            }
        };
        let hideThisButton = function () {
            let attributeValue = clickedButton.getAttribute('data-tabbed-header-button');
            if(!isNaN(attributeValue)) {
                JS.addClass(clickedButton, 'hide');
            }
        };
        let getClone = function() {
            let clone = clickedButton.cloneNode(true);
            clone.setAttribute('data-tabbed-header-button', 'clone');

            return clone;
        };

        if(JS.windows().width < 768) {
            let clone = getClone();
            deleteFirstChild();
            showOthers();
            hideThisButton();

            this.addEvents(clone);

            wrapper.insertBefore(clone, wrapper.firstChild);

            this.handleTriggerClick(wrapper);
        }
    },

    openDropdown(wrapper) {
        wrapper.setAttribute('data-tabbed-header', 'opened');
    },

    closeDropdown(wrapper) {
        wrapper.setAttribute('data-tabbed-header', 'closed');
    },

    handleTriggerClick(wrapper) {
        let attributeValue = wrapper.getAttribute('data-tabbed-header');

        if(attributeValue === 'opened') {
            this.closeDropdown(wrapper);
        } else if(attributeValue === 'closed') {
            this.openDropdown(wrapper);
        }
    }
};

JS.addEvent(window, 'load', function() {
    TabbedHeader.init();
});

