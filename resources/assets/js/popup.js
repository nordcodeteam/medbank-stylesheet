import {JS} from "./functions";
let jsPDF = require('./jspdf.debug.js');
let print = require('./print.min');

export let Popup = {
    init(trigger = null) {
        let triggers = JS.selectAll('[data-popup-trigger]');

        if(trigger) {
            this.addEvents(trigger);
        } else {
            for(let i = 0; i < triggers.length; i++) {
                this.addEvents(triggers[i]);
            }
        }

        this.addEvents();
    },

    addEvents(el = null) {
        if(el) {
            JS.addEvent(el, 'click', () => this.openPopup(el));
            JS.addEvent(el, 'touch', () => this.openPopup(el));
        } else {
            JS.addEvent(window, 'resize', () => this.setAlignment(el));
        }
    },

    openPopup(el) {
        let popupName = el.getAttribute('data-popup-trigger');
        if(popupName) {
            let data = window.popupData[popupName];
            if(data) {
                let html = this.generateHtml(data);
                if(html) {
                    JS.selectFirst('body').appendChild(html);

                    this.setAlignment();
                } else {
                    console.log('no HTML generated');
                }
            } else {
                console.log('no data found');
            }
        }else {
            console.log('popup name must be set');
        }
    },

    generateHtml(data) {
        let popupModule = document.createElement('div');
        let popupItem   = document.createElement('div');

        popupModule.classList.add('popup');
        popupItem.classList.add('popup--item');

        popupModule.appendChild(popupItem);

        popupItem.appendChild(JS.selectFirst('.prefab .' + data.module).cloneNode(true));

        /* Required close button init */
        JS.addEvent(JS.selectFirst('[data-popup-close]', popupItem), 'click', () => this.destroyPopup(popupModule));
        JS.addEvent(JS.selectFirst('[data-popup-close]', popupItem), 'touch', () => this.destroyPopup(popupModule));

        /* Optional download button init */
        let downloadButton = JS.selectFirst('[data-download]', popupItem);
        if(downloadButton) {
            let selector = downloadButton.getAttribute('data-download');
            let fileName = downloadButton.getAttribute('data-file-name');
            JS.addEvent(downloadButton, 'click', () => this.downloadInnerHtml(popupItem, selector, fileName));
            JS.addEvent(downloadButton, 'touch', () => this.downloadInnerHtml(popupItem, selector, fileName));
        }

        /* Optional print button init */
        let printButton = JS.selectFirst('[data-print]', popupItem);
        if(printButton) {
            let selector = printButton.getAttribute('data-print');
            JS.addEvent(printButton, 'click', () => this.printInnerHtml(popupItem, selector));
            JS.addEvent(printButton, 'touch', () => this.printInnerHtml(popupItem, selector));
        }

        return popupModule;
    },

    destroyPopup(el) {
        el.parentNode.removeChild(el);
    },

    setAlignment() {
        let popup = JS.selectFirst('.popup--item');

        if(popup) {
            let headingHeight = JS.selectFirst('.policy-popup--heading', popup).offsetHeight;
            let popupHeight = popup.firstChild.offsetHeight;
            let contentEl = JS.selectFirst('.policy-popup--content', popup);

            contentEl.style.height = popupHeight - headingHeight + 'px';

            let h = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

            if(popup.offsetHeight > h) {
                JS.addClass(popup.parentNode, 'popup---fix-overflow');
            } else {
                JS.removeClass(popup.parentNode, 'popup---fix-overflow');
            }
        }
    },

    downloadInnerHtml(context, selector, filename) {
        let content = JS.selectFirst(selector, context).innerHTML;

        let doc = new jsPDF('p', 'pt', 'letter');
        doc.fromHTML(content, 60, 40, {'width': 515 });
        doc.save(filename + '.pdf');
    },

    printInnerHtml(context, selector) {
        let content = JS.selectFirst(selector, context).innerHTML;

        let printEl = document.createElement('div');
        printEl.innerHTML = content;
        JS.addClass(printEl, 'display-only-for-print');

        let html = JS.selectFirst('html');
        html.appendChild(printEl);

        window.print();

        printEl.parentNode.removeChild(printEl);
    }
};

JS.addEvent(window, 'load', function() {
    Popup.init();
});