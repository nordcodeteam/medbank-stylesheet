import {JS} from "./functions";

export let Helpers = {
   /* events: {
        openstart  : new Event('openstart'),
        closestart : new Event('closestart'),
    },*/

    getTargets(el, expand) {
        let targetSelector = expand ? el.getAttribute('data-expand-trigger') : el.getAttribute('data-collapse-trigger');

        console.log(el, targetSelector, expand);

        return JS.selectAll('[data-expand-target="' + targetSelector + '"]');
    },

    open(el) {
        let openButton = JS.selectFirst('[data-expand-trigger="change-company"]');
        let closeButton = JS.selectFirst('[data-collapse-trigger="change-company"]');

        /* handle open */
        if(openButton) {
            JS.addClass(openButton.parentNode, 'page-title--button---hidden');
        }
        if(closeButton) {
            JS.removeClass(closeButton.parentNode, 'page-title--button---hidden');
        }

        JS.removeClass(el, 'expandable---collapsed');
        JS.addClass(el, 'expandable---expanded');
    },

    close(el) {
        let openButton = JS.selectFirst('[data-expand-trigger="change-company"]');
        let closeButton = JS.selectFirst('[data-collapse-trigger="change-company"]');

        /* handle close */
        if(closeButton) {
            JS.addClass(closeButton.parentNode, 'page-title--button---hidden');
        }
        if(openButton) {
            JS.removeClass(openButton.parentNode, 'page-title--button---hidden');
        }

        JS.addClass(el, 'expandable---collapsed');
        JS.removeClass(el, 'expandable---expanded');
    },

    toggle(el) {
        let expanded = el.getAttribute('data-expanded');

        if(expanded === '1') {
            this.close(el);
            el.setAttribute('data-expanded', '0');
        } else {
            this.open(el);
            el.setAttribute('data-expanded', '1');
        }
    }
};

export let Expand = {
    init(selector, expand = true) {
        let triggers = JS.selectAll(selector);

        for(let i = 0; i < triggers.length; i++) {
            JS.addEvent(triggers[i], 'click', () => this.handleClick(triggers[i], expand));
            JS.addEvent(triggers[i], 'touch', () => this.handleClick(triggers[i], expand));
        }
    },

    handleClick(el, expand) {
        let targets = Helpers.getTargets(el, expand);

        for(let i = 0; i < targets.length; i++) {
            if(expand) {
                if(expand === 'toggle') {
                    Helpers.toggle(targets[i]);
                }else {
                    Helpers.open(targets[i]);
                }
            } else {
                Helpers.close(targets[i]);
            }
        }
    },

    setElementsHeight() {
        let targets = JS.selectAll('[data-expand-target]');
        let previouseHeight = [];

        for(let i = 0; i < targets.length; i++) {
            targets[i].style.maxHeight = targets[i].scrollHeight + 'px';

            previouseHeight[i] = targets[i].scrollHeight;
        }

        if(JS.selectFirst('.document-item')) {

            setInterval(function () {
                for (let i = 0; i < targets.length; i++) {
                    if (previouseHeight[i] && targets[i].scrollHeight > previouseHeight[i]) {
                        targets[i].style.maxHeight = targets[i].scrollHeight + 'px';
                        previouseHeight[i] = targets[i].scrollHeight;
                        if (JS.hasClass(targets[i], 'expandable---expanded')) {
                            JS.removeClass(targets[i], 'expandable---expanded');
                            setTimeout(function () {
                                JS.addClass(targets[i], 'expandable---expanded');
                            }, 250);
                        }

                        return false;
                    }
                }
            }, 500);

        }
    }
};


JS.addEvent(window, 'load', function() {
    Expand.init('[data-expand-trigger]', 'toggle');
    Expand.init('[data-collapse-trigger]', false);
    Expand.setElementsHeight();

    window.Expand = Expand;
});