import {JS} from "./functions";

export let Dropdown = {
    init() {
        JS.addEvent(document, 'click', (e) => this.handleClick(e));
        JS.addEvent(document, 'touch', (e) => this.handleClick(e));
    },

    toggleDropdown(id) {
        let els = JS.selectAll('[data-dropdown-id="' + id + '"]');

        for (let i = 0; i < els.length; i++) {
            let el = els[i];

            if(JS.hasClass(el, 'dropdown---active')) {
                JS.removeClass(el, 'dropdown---active');
            } else {
                JS.addClass(el, 'dropdown---active');
            }
        }
    },

    closeDropdown(el, forceClose = false) {
        if(!el.hasAttribute('data-special-close') || forceClose) {
            JS.removeClass(el, 'dropdown---active');
        }
    },

    handleClick(e) {
        let self = this;

        let checkIfButton = function(target) {
            let el = target;
            let id;

            for(let j = 0; j < 4; j++) {
                if (id = el.getAttribute('data-dropdown')) {
                    self.toggleDropdown(id);

                    return true;
                } else {
                    el = el.parentNode;

                    if(el === document || el === null)
                        return false;
                }
            }

            return false;
        };

        if(!checkIfButton(e.target))  {
            let els = JS.selectAll('[data-dropdown-id]');

            for(let i = 0; i < els.length; i++) {
                this.closeDropdown(els[i]);
            }
        }
    }
};

JS.addEvent(window, 'load', function () {
    Dropdown.init();
});