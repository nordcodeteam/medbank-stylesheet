import {JS} from "./functions";

export const stylesConfig = {
    markerIconUrl: window.markerIconUrl,
    mapStyles: [
        {
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#fcfcfc"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#fcfcfc"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#dddddd"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#dddddd"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#dddddd"
                }
            ]
        }
    ]
};

export const GoogleApis = {
    sendRequest(url, callback) {
        let xhr = new XMLHttpRequest();

        xhr.open('GET', url);
        xhr.send(null);

        xhr.onreadystatechange = () => callback(xhr);
    },

    getCoordsFromAdress(address) {
        return new Promise((resolve, reject) => {
            const output = 'json';
            const params = encodeURIComponent(address);
            const url = 'https://maps.googleapis.com/maps/api/geocode/' + output + '?address=' + params;

            this.sendRequest(url, function (xhr) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        let response = JSON.parse(xhr.response);
                        if(response.status === 'OK') {
                            resolve({
                                position: response.results[0].geometry.location,
                                title: response.results[0].formatted_address
                            });
                        } else {
                            reject();
                        }
                    }
                }
            });

            setTimeout(() => reject(), 3000);
        });
    }
};

export const GoogleMap = {
    init(selector, address) {
        this.markers = [];

        let self = this;
        const containerElement = JS.selectFirst(selector);

        if(containerElement) {
            this.getMarkersData(address).then((response) => {
                if(response.length > 0) {
                    self.makeMakeInstance(containerElement, response[0].position);
                    for (let i = 0; i < response.length; i++) {
                        self.addMarkers(response);
                    }
                }
            }).catch((reason) => {
                console.log(reason);
            });
        }
    },

    getMarkersData(address) {
        return new Promise((resolve, reject) => {
            let markers = [];

             switch (typeof address) {
                case 'object':
                    for (let i = 0; i < address.length; i++) {
                        GoogleApis.getCoordsFromAdress(address[i]).then((response) => {
                            markers.push(response);

                            if(i === address.length - 1) {
                                resolve(markers);
                            }
                        }).catch(() => {
                            reject('Something went wrong');
                        });
                    }
                    break;
                case 'string':
                    GoogleApis.getCoordsFromAdress(address).then((response) => {
                        markers.push(response);
                        resolve(markers);
                    }).catch(() => {
                        reject('Something went wrong');
                    });
                    break;
            }
        });
    },

    makeMakeInstance(node, center) {
        this.map = new google.maps.Map(node, {
            zoom: 16,
            center: center,
            styles: stylesConfig.mapStyles,
            bounds_changed: function() {
                if (this.getZoom() > 15 && this.initialZoom == true) {
                    // Change max/min zoom here
                    this.setZoom(15);
                    this.initialZoom = false;
                }
            }
        });

        this.map.initialZoom = true;
    },

    addMarkers(markers) {
        let bounds  = new google.maps.LatLngBounds();

        for(let i = 0; i < markers.length; i++) {
            if(markers[i].position && markers[i].title) {
                let marker = new google.maps.Marker({
                    position: markers[i].position,
                    map: this.map,
                    title: markers[i].title,
                    icon: stylesConfig.markerIconUrl
                });

                const loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                bounds.extend(loc);

                this.markers.push({
                    title: markers[i].title,
                    instance: marker
                });
            }
        }

        this.map.fitBounds(bounds);
        this.map.panToBounds(bounds);
    }
};

JS.addEvent(window, 'load', function() {
    window.googleMap = (selector, config) => GoogleMap.init(selector, address);
});