import {JS} from "./functions";

export let formatCurrency = {
    init(el = null) {
        el = el || JS.selectAll('[data-format="currency"]');

        if(typeof el === 'object') {
            for(let i = 0; i < el.length; i++) {
                this.format(el[i]);
            }
        } else {
            this.format(el);
        }
    },

    formatMoney(n, c, d, t) {
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t;

        let s = n < 0 ? "-" : "";
        let i = n < 0 ? "-" : "";
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
        let j = i.length;
            j = j > 3 ? j % 3 : 0;

        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },

    format(el) {
        let value = el.innerHTML;
        if(!isNaN(value)) {
            el.innerHTML = this.formatMoney(value, 0, '', ' ') + ' ' + window.currency;
        }
    }
};

JS.addEvent(window, 'load', function() {
    formatCurrency.init();
});

window.showPassword = function(eye) {
    const input = JS.selectFirst('input', eye.parentNode.parentNode);
    const type = input.getAttribute('type');

    if(type === 'password') {
        input.setAttribute('type', 'text');
    } else if(type === 'text') {
        input.setAttribute('type', 'password');
    }
};