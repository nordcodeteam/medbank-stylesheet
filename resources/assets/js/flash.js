import {JS} from "./functions";

export const config = {
    containerId: 'flash-container',

    flashModule: 'flash',
    flashItem: 'item',
    close: 'close',
    cross: 'cross',
    icon: 'icon',
    message: 'message',
    collapse: 'collapsing',
};

export const flashControl = {
    flashList: [],

    init() {
        const self = this;

        if(window.flashMessages && window.flashMessages.length > 0) {
            /* creates new flash message item*/
            this.new(window.flashMessages[0].type, window.flashMessages[0].icon, window.flashMessages[0].message);

            /* Removes it from not inited flash messages variable*/
            window.flashMessages.splice(0, 1);

            /* creates next flash messages after short delay*/
            if(window.flashMessages.length > 0) {
                setTimeout(function () {
                    self.init();
                }, 100);
            }
        }
    },

    new(type, icon, message) {
        const key = this.flashList.length;
        this.flashList.push(new Flash(type, icon, message, key));

        return key;
    },

    removeFromList(key) {
        this.flashList.splice(key, 1);
    }
};

export class Flash {
    constructor(type, icon, message, key) {
        this.type = type;
        this.icon = icon;
        this.message = message;
        this.key = key;

        this.init();
    }

    init() {
        this.getContainer();
        this.createFlashEl();
        this.appendToContainer();

        JS.addEvent(this.closeButton, 'click', () => this.destroy());
    }

    createContainer() {
        this.container = document.createElement('div');
        this.container.classList.add(config.flashModule);
        this.container.setAttribute('id', config.containerId);

        const body = JS.selectFirst('body');
        body.appendChild(this.container);
    }

    createFlashEl() {
        this.flashElement = document.createElement('div');
        this.flashElement.classList.add(config.flashModule + '--' + config.flashItem);
        this.flashElement.classList.add(config.flashModule + '--' + config.flashItem + '---' + this.type);

        this.closeButton = document.createElement('div');
        this.closeButton.classList.add(config.flashModule + '--' + config.close);

        const cross = document.createElement('div');
        cross.classList.add(config.flashModule + '--' + config.cross);

        const icon = document.createElement('div');
        icon.classList.add(config.flashModule + '--' + config.icon);

        const svg = this.getIcon();

        this.messageEl = document.createElement('div');
        this.messageEl.classList.add(config.flashModule + '--' + config.message);
        this.messageEl.innerHTML = this.message;


        /* Combine separate elements */
        this.closeButton.appendChild(cross);
        this.flashElement.appendChild(this.closeButton);

        if(svg) {
            icon.appendChild(svg);
        }
        this.flashElement.appendChild(icon);

        this.flashElement.appendChild(this.messageEl);
    }

    getIcon() {
        return JS.selectFirst('.prefab #' + this.icon).firstElementChild.cloneNode(true);
    }

    appendToContainer() {
        this.container.appendChild(this.flashElement);
    }

    getContainer() {
        if(!this.container) {
            if(!(this.container = JS.selectFirst('#' + config.containerId))) {
                this.createContainer();
            }
        }
    }

    destroy() {
        const self = this;
        const height = this.flashElement.innerHeight|| this.flashElement.clientHeight;
        this.flashElement.style.height = height + 'px';

        JS.addEvent(this.flashElement, 'animationend', function(){
            self.flashElement.parentNode.removeChild(self.flashElement);
            flashControl.removeFromList(this.key);
        });
        this.flashElement.classList.add(config.flashModule + '--' + config.flashItem + '---' + config.collapse);
    }
}

window.flash = flashControl;

JS.addEvent(window, 'load', function () {
    window.flash.init();
});