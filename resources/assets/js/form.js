import {JS} from "./functions";
import {Validation} from "./validation";

export class FormValidation {
    autoInit() {
        let formsToValidate = JS.selectAll('[js-validate]');

        for(let i = 0; i < formsToValidate.length; i++) {
            Validation.init(formsToValidate[i], true);
        }
    }
}

export let selectFocus = {
    init(inputs = null) {
        this.inputs = inputs || JS.selectAll('.primary-input--field[data-toggle]');

        if(typeof this.inputs === 'object') {
            for(let i = 0; i < this.inputs.length; i++) {
                this.addEvents(this.inputs[i]);
            }
        } else {
            this.addEvents(inputs);
        }

        JS.addEvent(document, 'click', (e) => this.handleClick(e));
    },

    addEvents(input) {
        // JS.addEvent(input, 'focus', () => this.handleFocus(input));
        // JS.addEvent(input, 'blur', () => this.handleBlur(input));
    },

    closeAllSelects() {
        let inputs = JS.selectAll('[data-toggle]');

        for(let i = 0; i < inputs.length; i++) {
            inputs[i].setAttribute('data-toggle', 'blur');
        }
    },

    handleClick(e) {
        let toggleState = e.target.getAttribute('data-toggle');

        this.closeAllSelects();

        if(JS.hasClass(e.target, 'primary-input--field')) {
            if(toggleState === 'blur') {
                e.target.focus();
                e.target.setAttribute('data-toggle', 'focus');
            } else if (toggleState === 'focus') {
                e.target.blur();
                e.target.setAttribute('data-toggle', 'blur');
            }
        } else {
            this.blurInput();
        }
    },

    blurInput(input = null) {
        let handleIt = function(input) {
            input.blur();
            input.setAttribute('data-toggle', 'blur');
        };

        if(input === null) {
            for(let i = 0; i < this.inputs.length; i++) {
                handleIt(this.inputs[i]);
            }
        } else {
            handleIt(input);
        }
    },

    focusInput(input = null) {
        let handleIt = function(input) {
            input.focus();
            input.setAttribute('data-toggle', 'focus');
        };

        if(input === null) {
            for(let i = 0; i < this.inputs.length; i++) {
                handleIt(this.inputs[i]);
            }
        } else {
            handleIt(input);
        }
    }
};

let formValidation;
JS.addEvent(window, 'load', function() {
    formValidation =  new FormValidation().autoInit();
    selectFocus.init();
});