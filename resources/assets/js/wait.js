import {JS} from "./functions";

export class Wait {
    constructor() {
        this.error = 0;

        if(window.wait && window.wait.url && window.wait['_token']) {
            this.url = window.wait.url;
            this.callAgain = window.wait.callAgain || 1000;
            this.token = window.wait['_token'];
            this.init();
        }
    }

    init() {
        this.sendRequest();

    }

    sendRequest() {
        const self = this;

        JS.postAjax(this.url, {_token: this.token}, (response) => {
            try {
                response = JSON.parse(response);
                self.handleResponse(response);
            } catch(error) {
                console.log(error);
            }
        });
    }

    handleResponse(response) {
        const self = this;
        let timeout = 1000;

        if(response.status === 'ok') {
            this.error = 0;
            if(response.redirect === 'self') {
                location.reload();
            } else if(response.redirect){
                window.location = response.redirect;
            } else {
                if(response.callAgain) {
                    timeout = response.callAgain;
                }

                setTimeout(function() {
                    self.sendRequest();
                }, timeout);
            }
        } else {
            this.error++;

            if(this.error < 3) {
                setTimeout(function() {
                    self.sendRequest();
                }, 4000 * this.error);
            }
        }
    }
}

JS.addEvent(window, 'load', () => {
    new Wait();
});