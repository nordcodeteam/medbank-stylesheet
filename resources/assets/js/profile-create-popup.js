import {JS} from "./functions";
import {Validation, ValidationUI} from "./validation";

export const helpers = {
    getXhr(method, url) {
        let xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');

        xhr.open(method, url, true);
        xhr.withCredentials = true;
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');

        return xhr;
    },
};

export const createPopup = {
    create(html) {
        this.popupItem.innerHTML = html;

        this.initPopup();
    },

    displayOverlay() {
        this.popupModule = document.createElement('div');
        this.popupModule.classList.add('popup');
        this.popupItem   = document.createElement('div');
        this.popupItem.classList.add('popup--item');
        this.preloader = JS.getPreloader();
        this.popupItem.appendChild(this.preloader);

        this.popupModule.appendChild(this.popupItem);

        JS.selectFirst('body').appendChild(this.popupModule);
    },

    initPopup(popupModule = null, popupItem = null) {
        popupModule = popupModule || this.popupModule;
        popupItem = popupItem || this.popupItem;

        const smsCodeInputs = JS.selectAll('[name="sms_code[]"]');
        autoFocus.init(smsCodeInputs);

        /*init close button*/
        JS.addEvent(JS.selectFirst('[data-popup-close]', popupItem), 'click', () => this.destroyPopup(popupModule));
        JS.addEvent(JS.selectFirst('[data-popup-close]', popupItem), 'touch', () => this.destroyPopup(popupModule));
    },

    destroyPopup(el) {
        el.parentNode.removeChild(el);
    },
};

export const autoFocus = {
    init(inputs) {
        if(inputs) {
            for(let i = 0; i < inputs.length; i++) {
                JS.addEvent(inputs[i], 'keypress', (e) => this.clearInput(e, inputs[i]));
                JS.addEvent(inputs[i], 'keydown', (e) => this.handleChange(e, inputs[i]));
            }
        }
    },

    getNextInput(input) {
        const id = input.getAttribute('id');
        const currentInputIndex = id.substring(id.length - 1);
        const prefix = id.substring(0, id.length - 1);

        return JS.selectFirst('#' + prefix + (+currentInputIndex + 1));
    },

    clearInput(e, input) {
        if(e) {
            e.preventDefault();
            if(/^[0-9]+$/g.test(e.key)) {
                input.value = '';
            }
        } else {
            input.value = '';
        }
    },

    handleChange(e, input) {
        e.preventDefault();

        if(/^[0-9]+$/.test(e.key)) {
            let form = JS.selectFirst('#confirm-registration');
            let nextInput = this.getNextInput(input);
            input.value = e.key;

            if(nextInput) {
                nextInput.focus();
            } else {
                if(this.valid()) {
                    const data = new FormData(form);
                    profileNavigation.sendRequest(form.getAttribute('action'), data);
                }
            }
        } else if(e.key === 'Backspace') {
            this.clearInput(null, input);
        }
    },

    valid() {
        const form = JS.selectFirst('#confirm-registration');
        const inputs = JS.selectAll('input', form);

        let areValidInputs = true;

        for(let i = 0; i < inputs.length; i++) {
            if(inputs[i].value === '') {
                areValidInputs = false;
            } else if(inputs[i].length > 1) {
                areValidInputs = false;
            }
        }

        return areValidInputs;
    }
};

export const profileCreation = {
    init(wrapper) {
        let button = JS.selectFirst('a[href]', wrapper);
        JS.addEvent(button, 'click', (e) => this.handleClick(e, button));
    },

    sendRequest(url) {
        let xhr = helpers.getXhr('GET', url);

        xhr.onreadystatechange = () => this.handleReadyStateChange(xhr);

        xhr.send();
    },

    handleReadyStateChange(xhr) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                let response = xhr.response;
                this.handleResponse(response);
            }

            window.fetchingProfileCreationPopupHtml = false;
        }
    },

    handleResponse(response) {
        createPopup.create(response);
    },

    handleClick(button) {
        const url = button.getAttribute('href');

        if(url) {
            if(!window.fetchingProfileCreationPopupHtml) {
                window.fetchingProfileCreationPopupHtml = true;
                createPopup.displayOverlay();
                this.sendRequest(url);
            }
        }
    }
};

export const profileNavigation = {
    submit(form) {
        this.popupModule = form.closest('.popup');
        this.popupItem = form.closest('.popup--item');

        const self = this;
        const url = form.getAttribute('action');
        const formData = new FormData(form);

        ValidationUI.removeErrorNodesFromSection(form);
        Validation.validateSection(form).then((data) => {
            if(data === true) {
                self.sendRequest(url, formData);
            }
        });
    },

    redirect(link) {
        this.popupModule = link.closest('.popup');
        this.popupItem = link.closest('.popup--item');
        const url = link.getAttribute('href');

        this.sendRequest(url);
    },

    displayPreloader() {
        this.preloader = document.createElement('div');
        this.preloader.classList.add('panel-preloader');
        this.preloader.appendChild(JS.getPreloader());

        this.popupItem.appendChild(this.preloader);
    },

    removePreloader() {
        this.preloader.parentNode.removeChild(this.preloader);
        this.preloader = null;
    },

    sendRequest(url, data = null) {
        let xhr = helpers.getXhr(data ? 'POST' : 'GET', url);

        if(!this.preloader) {
            this.displayPreloader();
        }

        xhr.onreadystatechange = () => this.handleReadyStateChange(xhr);

        xhr.send(data);
    },

    handleReadyStateChange(xhr) {
        if (xhr.readyState === 4){
            if (xhr.status === 200) {
                this.handleResponse(xhr.response);
            } else if(xhr.status === 422) {
                this.handleError(xhr.response);
            }

            this.removePreloader(this.preloader);
        }
    },

    handleError(response) {
        const getInput = function(errorPair) {
            return JS.selectFirst('[name="' + Object.keys(errorPair)[0] + '"]');
        };
        const getMessage = function(errorPair) {
            return errorPair[Object.keys(errorPair)[0]];
        };

        try {
            response = JSON.parse(response);

            switch(response.constructor) {
                case {}.constructor:
                    ValidationUI.setErrorMessage(ValidationUI.getInputGroup(getInput(response)), getMessage(response));
                    break;
                case [].constructor :

                    break;
            }

            for(let i = 0; i < response.length; i++) {
                //const input = JS.select()
                console.log(response[i]);
            }
            //
        } catch(error) {
            console.log(error);
        }

    },

    handleResponse(response) {
        try {
            response = JSON.parse(response);

            if(typeof response['redirect'] === 'string') {
                window.location = response['redirect'];
            } else if(typeof response['request'] === 'string') {
                this.sendRequest(response['request']);
            }
        } catch(error) {
            this.replacePopupHtml(response);
        }
    },

    replacePopupHtml(html) {
        this.popupItem.innerHTML = html;
        createPopup.initPopup(this.popupModule, this.popupItem);

        const smsCodeInputs = JS.selectAll('[name="sms_code[]"]');
        autoFocus.init(smsCodeInputs);
    }
};

JS.addEvent(window, 'load', function () {
    window.profileNavigation = profileNavigation;
    window.profileCreation = profileCreation;
});