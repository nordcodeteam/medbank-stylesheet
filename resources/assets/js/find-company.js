import {JS} from "./functions";

export let findCompany = {
    init() {
        this.xhr = [];
        this.form = JS.selectFirst('#company-search-form');
        this.searchInput = JS.selectFirst('#company_query_string', this.form);
        this.dropdown = JS.selectFirst('[data-dropdown-id="suggestions"]', this.form);

        if (this.form && this.searchInput) {
            this.addEvents();
        }
    },

    addEvents() {
        JS.addEvent(this.searchInput, 'keyup', (e) => this.handleKeyup(e));
        JS.addEvent(this.searchInput, 'keydown', (e) => this.handleKeydown(e));
    },

    clearDropdown() {
        while(this.dropdown.hasChildNodes()) {
            this.dropdown.removeChild(this.dropdown.firstChild);
        }
    },

    getPreloader() {
        let prefab = JS.selectFirst('[data-prefab="preloader"] > *');
        let clone = prefab.cloneNode(true);

        let dropdownPreloader = document.createElement('div');
        JS.addClass(dropdownPreloader, 'dropdown--preloader');
        dropdownPreloader.appendChild(clone);

        return dropdownPreloader;
    },

    clearXhrs() {
        for(let key in this.xhr) {
            let value = this.xhr[key];
            if(typeof value === 'object') {
                this.xhr[key].abort();
            }
        }

        this.xhr = [];
    },

    search() {
        this.preloader = this.preloader || this.getPreloader();

        let self     = this;
        let url      = this.form.getAttribute('action');

        let data = new FormData(this.form);
        let input = JS.selectFirst('#company_query_string', this.form).value;

        if(input.length >= 3 && isNaN(input)) {
            this.clearXhrs();

            let date = Date.now();
            this.xhr[date] = new XMLHttpRequest();
            let xhr = this.xhr[date];

            this.clearDropdown();
            this.showDropdown();

            xhr.open('POST', url);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(data);

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        let response = JSON.parse(xhr.response);
                        if (response.success === 'ok') {
                            self.useRetrievedData(response);
                        } else if (response.success === 'error') {
                            self.clearDropdown();
                            self.displayMessage(response.message);
                        } else {
                            self.hideDropdown();
                        }
                    }
                }
            }
        }
    },

    showDropdown() {
        JS.addClass(this.dropdown, 'dropdown---active');
        this.dropdown.appendChild(this.preloader);
    },

    hideDropdown() {
        JS.removeClass(this.dropdown, 'dropdown---active');
    },

    populateDropdown(suggestions) {
        this.clearDropdown();

        for(let i = 0; i < suggestions.length; i++) {
            let suggestion = suggestions[i];

            let el = document.createElement('div');
            JS.addClass(el, 'dropdown--item');
            el.innerHTML = suggestion.title;
            el.setAttribute('data-query-string', suggestion.title);

            JS.addEvent(el, 'click', () => this.handleSelect(el));
            JS.addEvent(el, 'touch', () => this.handleSelect(el));

            this.dropdown.appendChild(el);
        }
    },

    displayMessage(message) {
        let messageEl = document.createElement('div');
        JS.addClass(messageEl, 'dropdown--message');
        messageEl.innerHTML = message;
        this.dropdown.appendChild(messageEl);
    },

    useRetrievedData(response) {
        if(response.suggestions) {
            this.populateDropdown(response.suggestions);
        } else {
            this.hideDropdown();
        }
    },

    handleKeyup(e) {
        let self = this;

        if(this.timeout) {
            clearTimeout(this.timeout);
        }

        this.timeout = setTimeout(function() {
            if(/^([A-z])$/.test(e.key) ||
                /^([0-9])$/.test(e.key)) {
                self.search();
            }
        }, 200);
    },

    handleSelect(el) {
        let queryString = el.getAttribute('data-query-string');

        this.hideDropdown();

        this.searchInput.value = queryString;
        this.searchInput.blur();
        this.form.submit();
    },

    handleKeydown(e) {
         if(e.keyCode === 13 && isNaN(JS.selectFirst('[name="company_query_string"]').value)){
             e.preventDefault();

            if(this.dropdown.hasChildNodes()) {
                this.handleSelect(this.dropdown.firstChild);
                this.hideDropdown();
            }
        }
    }
};

export let changeCompanyButton = {
    openButton: JS.selectFirst('[data-expand-trigger="change-company"]'),
    closeButton: JS.selectFirst('[data-collapse-trigger="change-company"]'),

    init() {
        let triggers = JS.selectAll('[data-expand-target="change-company"]');


        for(let i = 0; i < triggers.length; i++) {
            JS.addEvent(triggers[i], 'openstart', () => this.handleOpen(triggers[i]));
            JS.addEvent(triggers[i], 'closestart', () => this.handleClose(triggers[i]));
        }
    },

    handleOpen() {
        JS.addClass(this.openButton.parentNode, 'page-title--button---hidden');
        JS.removeClass(this.closeButton.parentNode, 'page-title--button---hidden');
    },

    handleClose() {
        JS.addClass(this.closeButton.parentNode, 'page-title--button---hidden');
        JS.removeClass(this.openButton.parentNode, 'page-title--button---hidden');
    }
};

JS.addEvent(window, 'load', function() {
    findCompany.init();
    changeCompanyButton.init();
});