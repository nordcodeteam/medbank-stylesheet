import {JS} from "./functions";

JS.addEvent(window, 'load', function() {
    let textareas = JS.selectAll('textarea');

   for(let i = 0; i < textareas.length; i++) {
       let element = textareas[i];

       element.style.height = "5px";
       element.style.height = (element.scrollHeight)+"px";
   }
});