import {JS} from "./functions";
import {Validation, ValidationUI} from "./validation";
import {formatCurrency} from "./helpers";
import 'core-js/fn/string/includes';

if(typeof window.propertyLocationsTitles === 'undefined') {
    window.propertyLocationsTitles = {
        city : 'Didmiesčiai',
        district : 'Rajonai',
        district_center : 'Rajono centrai',
    };
}

if(typeof window.propertyLocations === 'undefined') {
    //use dummy locations JSON
    window.propertyLocations = {
        city : [{value : 1, title : 'Vilnius'}, {value : 2, title : 'Klaipėda'}],
        district : [{value : 1, title : 'Šilalės r. sav.'}, {value : 2, title : 'Šiaulių r sav.'}],
        district_center : [{value : 1, title : 'Šilalė'}, {value : 2, title : 'Biržai'}],
    };
}

if(typeof window.propertyTypeLocationsMap === 'undefined') {
    window.propertyTypeLocationsMap = {
        apartment: ['city', 'district_center'],
        house: ['city', 'district_center', 'district'],
        commercial_real_estate: ['city', 'district_center', 'district'],
        land_of_land_use: ['district'],
        land_for_home: ['city'],
        land_for_commercial_use: ['city']
    };
}

export const propertyHelpers = {
    replaceFirstArrayIndex(id, newIndex) {
        let start = id.indexOf('[');
        let end = id.indexOf(']');

        return id.substring(0, start + 1) + newIndex + id.substring(end);
    },

    updateInputsAttributes(input, index) {
        let id = input.getAttribute('id');
        if(id) {
            let newId = this.replaceFirstArrayIndex(id, index);

            /* updates id's index */
            input.setAttribute('value', input.value);

            /* updates id's index */
            input.setAttribute('id', newId);

            /* updates name's index */
            input.setAttribute('name', newId);

            /* updates label's for index */
            let label = JS.selectFirst('label', input.parentNode);
            if(label) {
                label.setAttribute('for', newId);
            }
        }
    },

    getForm() {
        return JS.selectFirst('#step-1-form') || JS.selectFirst('#step-3-form');
    },

    getAction(form) {
        return form.getAttribute('action');
    },

    removeErrorFromHiddenInputs(section) {
        let groups = JS.selectAll('.property-content--animated-inputs', section);

        for(let i = 0; i < groups.length; i++) {
            if (!JS.hasClass(groups[i], 'property-content--animated-inputs---active')) {
                ValidationUI.removeErrorNodesFromSection(groups[i]);
            }
        }
    },

    checkUpdateButton(button) {
        if(button) {
            JS.addClass(button, 'animated-button---clicked');
        }
    },

    checkAllUpdateButton(button) {
        const updateButtons = JS.selectAll('.property-content--update-button button');

        for(let i = 0; i < updateButtons.length; i++) {
            this.checkUpdateButton(updateButtons[i]);
        }
    },

    getLastAddButton() {
        const addButtons = JS.selectAll('.property-content--add-button');

        return addButtons[addButtons.length - 1];
    },

    getPropertiesWrapper() {
        return JS.selectFirst('[data-property]').parentNode;
    }
};

export let propertyRemove = {
    init(property) {
        let self = this;

        let init = function(property) {
            let deleteButton = JS.selectFirst('.property-content--remove-button', property);

            deleteButton.addEventListener('click', () => self.handleRemove(property, deleteButton));
            deleteButton.addEventListener('touch', () => self.handleRemove(property, deleteButton));
        };

        if(property) {
            init(property);
        } else {
            let properties = JS.selectAll('[data-property]');

            for(let i = 0; i < properties.length; i++) {
                init(properties[i]);
            }
        }
    },

    updatePropertyAttributes(inputs, index) {
        for(let i = 0; i < inputs.length; i++) {
            propertyHelpers.updateInputsAttributes(inputs[i], index);
        }
    },

    removeNode(node) {
        node.parentNode.removeChild(node);
    },

    updateAllProperties(offset = 0) {
        let properties = JS.selectAll('[data-property]');
        for(let i = 0; i < properties.length; i++) {
            let currentIndex =properties[i].getAttribute('data-property');

            if(currentIndex > offset) {
                /* Gets new inputs index*/
                let index = currentIndex - 1;

                /* Update property's data attribute */
                properties[i].setAttribute('data-property', index);

                /* Updates input's attributes*/
                let inputs = {
                    inputs: JS.selectAll('input', properties[i]),
                    selects: JS.selectAll('select', properties[i]),
                };
                this.updatePropertyAttributes(inputs.inputs, index);
                this.updatePropertyAttributes(inputs.selects, index);

                let updateButton = JS.selectFirst('.property-content--update-button button', properties[i]);
                if(updateButton) {
                    updateButton.value = index;
                }

                let addButton = JS.selectFirst('.property-content--add-button button', properties[i]);
                if(addButton) {
                    addButton.value = index;
                }

                /* Updates properties subheading */
                let subheading = JS.selectFirst('.property-content--heading h4', properties[i]);
                let lastIndex = subheading.innerHTML.lastIndexOf(" ");
                subheading.innerHTML = subheading.innerHTML.substring(0, lastIndex) + ' ' + (+index + 1);
            }
        }
    },

    removeHtmlFromDom(context) {
        let currentIndex = context.getAttribute('data-property');

        if(context.getAttribute('data-first-property') !== null) {
            /* Clones heading*/
            let heading = JS.selectFirst('.primary-panel--heading', context).cloneNode(true);

            /* Removes this box */
            this.removeNode(context);

            /* Prepend heading to new first box */
            let firstPropertyBox = JS.selectFirst('[data-property]');
            firstPropertyBox.setAttribute('data-first-property', '');
            firstPropertyBox.insertBefore(heading, firstPropertyBox.firstChild);

            /* Updates all property boxes */
            this.updateAllProperties();
        } else {
            /* Removes this box */
            this.removeNode(context);

            /* updates those property boxes which has higher property index than deleted one */
            this.updateAllProperties(currentIndex);
        }

        let properties = JS.selectAll('[data-property]');

        if(properties.length === 1) {
            JS.addClass(JS.selectFirst('.property-content--remove-button' ,properties[0]), 'property-content--remove-button---hide');
        }

        if(+properties.length === +currentIndex) {
            JS.removeClass(JS.selectFirst('.property-content--add-button' , properties[properties.length - 1]), 'property-content--add-button---hide');
        }
    },

    handleRemove(context, deleteButton) {
        const form = propertyHelpers.getForm();
        const url = propertyHelpers.getAction(form);
        const deleteUrl = deleteButton.getAttribute('data-delete-url');
        const lastAddButton = propertyHelpers.getLastAddButton();
        const propertiesWrapper = propertyHelpers.getPropertiesWrapper();

        propertyRemove.removeHtmlFromDom(context);
        if(deleteUrl) {
            Calculate.sendDeleteRequest(deleteUrl).then(() => {
                Validation.validateSection(propertiesWrapper).then(isValid => {
                    if(isValid === true) {
                        Calculate.sendRequest(form, url, lastAddButton);
                    } else {
                        propertyHelpers.removeErrorFromHiddenInputs(propertiesWrapper);
                        Validation.focusInput(isValid[0], 500, -50, false);
                    }
                });
            }).catch((reason) => {
                console.log('flash message: error on delete request: ', reason); //todo: flash message
            });
        } else {
            Validation.validateSection(propertiesWrapper).then(isValid => {
                if(isValid === true) {
                    Calculate.sendRequest(form, url, lastAddButton);
                } else {
                    propertyHelpers.removeErrorFromHiddenInputs(propertiesWrapper);
                    Validation.focusInput(isValid[0], 500, -50, false);
                }
            });
        }
    }
};

export let propertyTypeChange = {
    handlePreSelected(input, context) {
        if(input.value) {
            this.displayAnimatedElements(context);
        }
    },

    init(property) {
        let self = this;

        let init = function(property) {
            let input = JS.selectFirst('select', property);
            let inputToRePopulate = JS.selectFirst('[data-property-location-holder] select', property);

            self.handlePreSelected(input, property);
            input.addEventListener('change', () => self.handleChange(input, inputToRePopulate, property));
        };

        if(property) {
            init(property);
        } else {
            let properties = JS.selectAll('[data-property]');

            for(let i = 0; i < properties.length; i++) {
                init(properties[i]);
            }
        }
    },

    getOptions(groups, placeholder, selected = '') {
        let elementsHolder = document.createElement('div');

        /* Append placeholder */
        let optionElement = document.createElement('option');
        optionElement.innerHTML = placeholder;
        optionElement.setAttribute('value', '');
        optionElement.setAttribute('selected', 'true');
        optionElement.setAttribute('disabled', 'true');
        elementsHolder.appendChild(optionElement);

        /*/!* Append all other options with option groups *!/
        for(let i = 0; i < groups.length; i++) {
            let group = groups[i];
            let locationOptions = window.propertyLocations[group];
            let locationTitle = window.propertyLocationsTitles[group];

            let optgroupElement = document.createElement('optgroup');
            optgroupElement.setAttribute('label', locationTitle);

            for(let j = 0; j < locationOptions.length; j++) {
                let option = locationOptions[j];

                let optionElement = document.createElement('option');
                optionElement.innerHTML = option.title;
                optionElement.setAttribute('value', option.value);

                if(option.value === selected) {
                    optionElement.setAttribute('selected', 'true');
                }

                optgroupElement.appendChild(optionElement);
            }

            elementsHolder.appendChild(optgroupElement);
        }*/

        /* Append all other options without option groups */
        for(let i = 0; i < groups.length; i++) {
            let group = groups[i];
            let locationOptions = window.propertyLocations[group];

            for(let j = 0; j < locationOptions.length; j++) {
                let option = locationOptions[j];

                let optionElement = document.createElement('option');
                optionElement.innerHTML = (group === 'city' ? '&nbsp; &nbsp; ' : '') + option.title;
                optionElement.setAttribute('value', option.value);

                if(option.value === selected) {
                    optionElement.setAttribute('selected', 'true');
                }

                elementsHolder.appendChild(optionElement);
            }
        }

        return elementsHolder;
    },

    displayAnimatedElements(context) {
        let inputs = JS.selectAll('.property-content--animated-inputs', context);
        let buttons = JS.selectAll('.property-content--update-button', context);

        for(let i = 0; i < inputs.length; i++) {
            JS.addClass(inputs[i], 'property-content--animated-inputs---active');
        }

        for(let i = 0; i < buttons.length; i++) {
            JS.addClass(buttons[i], 'property-content--update-button---active');
        }
    },

    handleChange(input, inputToRePopulate, context) {
        if(input.value) {
            let groups = window.propertyTypeLocationsMap[input.value];
            let placeholder = inputToRePopulate.getAttribute('placeholder') || '-';

            let options = this.getOptions(groups, placeholder);

            while(inputToRePopulate.hasChildNodes()) {
                inputToRePopulate.removeChild(inputToRePopulate.firstChild);
            }

            while(options.hasChildNodes()) {
                inputToRePopulate.appendChild(options.removeChild(options.firstChild));
            }

            /* it will make two inputs and button appear */
            this.displayAnimatedElements(context);
        }
    }
};

export let propertyAdd = {
    init(property = null) {
        let self = this;

        let init = function(property) {
            let addButton = JS.selectFirst('.property-content--add-button button', property);

            addButton.addEventListener('click', (e) => self.handleClick(e, property, addButton));
            addButton.addEventListener('touch', (e) => self.handleClick(e, property, addButton));
        };

        if(property) {
            init(property);
        } else {
            let properties = JS.selectAll('[data-property]');

            for(let i = 0; i < properties.length; i++) {
                init(properties[i]);
            }
        }
    },

    clearInputs(inputs, index) {
        for(let i = 0; i < inputs.length; i++) {
            inputs[i].value = "";

            propertyHelpers.updateInputsAttributes(inputs[i], index);
        }
    },

    displayRemoveButton(context) {
        let removeButton = JS.selectFirst('.property-content--remove-button', context);
        if(removeButton) {
            JS.removeClass(removeButton, 'property-content--remove-button---hide');
        }
    },

    getPropertyHtml(resetButtonsAnimation = true) {
        let allPropertyBoxes = JS.selectAll('[data-property]');
        let propertyClone = allPropertyBoxes[allPropertyBoxes.length - 1].cloneNode(true);
        let index = +propertyClone.getAttribute('data-property') + 1;

        /* Updates attributes */
        propertyClone.removeAttribute('data-first-property');
        propertyClone.setAttribute('data-property', index);

        /* Removes big heading of first panel for newly added panels */
        let heading = JS.selectFirst('.primary-panel--heading', propertyClone);
        if(heading) {
            propertyClone.removeChild(heading);
        }

        /* Change subheading's number */
        let subheading = JS.selectFirst('.property-content--heading .h4', propertyClone);
        let lastIndex = subheading.innerHTML.lastIndexOf(" ");
        subheading.innerHTML = subheading.innerHTML.substring(0, lastIndex) + ' ' + (+index + 1);

        /* reset data-delete-url */
        let removeUrl = JS.selectFirst('[data-delete-url]', propertyClone);
        if(removeUrl) {
            removeUrl.setAttribute('data-delete-url', '');
        }

        /* Clears all inputs and change input indexes */
        let inputs = {
            inputs : JS.selectAll('input', propertyClone),
            selects : JS.selectAll('select', propertyClone),
        };
        this.clearInputs(inputs.inputs, index);
        this.clearInputs(inputs.selects, index);
        ValidationUI.removeSplitInputErrorNodesFromSection(propertyClone);

        /* restore animated inputs animation and displays remove button */
        let updateButton = JS.selectFirst('.property-content--update-button', propertyClone);
        let animatedInputs = JS.selectAll('.property-content--animated-inputs', propertyClone);
        if(animatedInputs.length < 4) {
            if (animatedInputs && resetButtonsAnimation) {
                for (let i = 0; i < animatedInputs.length; i++) {
                    JS.removeClass(animatedInputs[i], 'property-content--animated-inputs---active');
                }
            }
            if (updateButton && resetButtonsAnimation) {
                JS.removeClass(updateButton, 'property-content--update-button---active');
                JS.selectFirst('button', updateButton).setAttribute('value', index);
            }
        }
        let addButton = JS.selectFirst('.property-content--add-button', propertyClone);
        if(addButton) {
            JS.removeClass(addButton, 'property-content--add-button---hide');
            JS.selectFirst('button', addButton).setAttribute('value', index);
        }
        this.displayRemoveButton(propertyClone);

        JS.removeClass(JS.selectFirst('button', updateButton), 'animated-button---clicked');

        return propertyClone;
    },

    hideAddButton(context) {
        let button = JS.selectFirst('.property-content--add-button', context);
        if(button) {
            JS.addClass(button, 'property-content--add-button---hide')
        }
    },

    addHtmlToDOM(context) {
        let propertyHtml = this.getPropertyHtml();

        /* Display remove button */
        this.displayRemoveButton(context);

        /* Hide this button */
        this.hideAddButton(context);

        /* init events on new property box's buttons and inputs */
        this.init(propertyHtml);
        propertyTypeChange.init(propertyHtml);
        propertyRemove.init(propertyHtml);
        Validation.initInline(propertyHtml);
        Calculate.init(propertyHtml);

        /* add element to DOM */
        context.parentNode.appendChild(propertyHtml);
    },

    handleClick(e, context, addButton) {
        e.preventDefault();

        const self = this;
        const form = propertyHelpers.getForm();
        const url = propertyHelpers.getAction(form);
        const propertiesWrapper = propertyHelpers.getPropertiesWrapper();

        let clickedUpdateButton = JS.selectFirst('.animated-button.animated-button---clicked', context);
        if(!clickedUpdateButton) {
            Validation.validateSection(propertiesWrapper).then(isValid => {
               if(isValid === true) {
                   Calculate.sendRequest(form, url, addButton);
                   self.addHtmlToDOM(context);
                   propertyHelpers.checkAllUpdateButton();
               } else {
                   propertyHelpers.removeErrorFromHiddenInputs(propertiesWrapper);
                   Validation.focusInput(isValid[0], 500, -50, false);
               }
            });
        } else {
            self.addHtmlToDOM(context);
        }
    }
};

export let Calculate = {
    init(property) {
        let self = this;

        let init = function(property) {
            let updateButton = JS.selectFirst('.property-content--update-button button', property);

            if(updateButton) {
                updateButton.addEventListener('click', (e) => self.handleClick(e, updateButton));
                updateButton.addEventListener('touch', (e) => self.handleClick(e, updateButton));
            }

            let handleChange = function(inputs) {
                for(let i = 0; i < inputs.length; i++) {
                    inputs[i].addEventListener('change', (e) => self.resetCalculateButton(updateButton));
                    inputs[i].addEventListener('keydown', (e) => self.resetCalculateButton(updateButton));
                }
            };

            handleChange(JS.selectAll('input', property));
            handleChange(JS.selectAll('select', property));
        };

        if(property) {
            init(property);
        } else {
            let properties = JS.selectAll('[data-property]');

            for(let i = 0; i < properties.length; i++) {
                init(properties[i]);
            }
        }
    },

    sendDeleteRequest(url) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        let response = JSON.parse(xhr.response);

                        if(response.success === 'ok') {
                            resolve();
                        } else {
                            reject('response succes: false');
                        }
                    } else {
                        reject(xhr.status);
                    }
                }
            }
        });
    },

    sendRequest(form, url, button) {
        let self = this;
        let data = new FormData(form);
        let buttonValue = button ? button.getAttribute('value') : null;
        let index = buttonValue || 0;
        data.append('calculate', index);

        let xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(data);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.response);
                    if(response.success === 'ok') {
                        self.useRetrievedData(response);
                    } else {
                        self.useRetrievedData({maxAmount: '--'});
                        window.flash.new('error', 'flash-info', window.flashTranslations.errorOfferNotEnough);
                    }
                } else {
                    self.useRetrievedData({maxAmount: '--'});
                    window.flash.new('error', 'flash-info', window.flashTranslations.errorOfferNotEnough);
                    console.log('Error status: ', xhr.status);
                }
            }
        }
    },

    checkTargetAmount(offer) {
        const input = JS.selectFirst('#loan_amount');
        let value;

        if(input) {
            value = input.value;
        } else {
            const valueHolder = JS.selectFirst('.info-block--value[data-format="currency"]');
            value = valueHolder.innerHTML.replace(' ', '').replace(window.currencyLetters, '');
        }

        if(offer < value && !isNaN(offer)) {
            window.flash.new('info', 'flash-info', window.flashTranslations.infoOfferNotEnough);
        }
    },

    useRetrievedData(data) {
        let el = JS.selectFirst('.alternative-panel--amount-value');

        if(data.maxAmount !== null) {
            this.checkTargetAmount(data.maxAmount);
        }

        if(el && data.maxAmount !== null) {
            let clone = el.cloneNode(true);
            if(isNaN(data.maxAmount)) {
                clone.innerHTML = data.maxAmount;
            } else {
                clone.innerHTML = formatCurrency.formatMoney(data.maxAmount, 0, '', ' ') + ' ' + window.currency;
            }

            /* Removes and recreates element to re-run CSS animation */
            clone.setAttribute('calculated', '');
            el.parentNode.insertBefore(clone, el);
            el.parentNode.removeChild(el);

            JS.addClass(clone, 'alternative-panel--amount-value---run-animation');
        }
    },

    resetCalculateButton(button) {
        JS.removeClass(button, 'animated-button---clicked');
    },

    handleClick(e, updateButton) {
        e.preventDefault();

        const form = propertyHelpers.getForm();
        const url = propertyHelpers.getAction(form);
        const propertiesWrapper = propertyHelpers.getPropertiesWrapper();
        const lastAddButton = propertyHelpers.getLastAddButton();

        let clickedUpdateButton = JS.selectFirst('.animated-button.animated-button---clicked', updateButton.parentNode);
        if (!clickedUpdateButton) {
            Validation.validateSection(propertiesWrapper).then(isValid => {
                if (isValid === true) {
                    Calculate.sendRequest(form, url, lastAddButton);
                    propertyHelpers.checkAllUpdateButton();
                } else {
                    propertyHelpers.removeErrorFromHiddenInputs(propertiesWrapper);
                    Validation.focusInput(isValid[0], 500, -50, false);
                }
            });
        }
    }
};

export let stickyPanel = {
    addEvents() {
        window.addEventListener('scroll', this.onScroll.bind(this));
        window.addEventListener('resize', this.onResize.bind(this));
    },

    init(element, stickyClass) {
        if(element) {
            this.element = element;
            this.originalWidth = element.offsetWidth;
            this.stickyClass = stickyClass
            this.inUse = true;
            this.onResize();
            this.addEvents();
            this.position = element.offsetTop - 20;
            this.onScroll();
        }
    },

    aboveScroll() {
        return this.position < window.scrollY;
    },

    onScroll() {
        if(this.inUse) {
            if (this.aboveScroll()) {
                this.setFixed();
            } else {
                this.setStatic();
            }
        } else {
            this.setStatic();
        }
    },

    onResize() {
        let w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        this.originalWidth = this.element.offsetWidth;

        if(w >= 1024) {
            this.inUse = true;
            this.onScroll();
        } else {
            this.inUse = false;
            this.setStatic();
        }
    },

    setFixed() {
        JS.addClass(this.element, this.stickyClass);
        this.element.style.width = this.originalWidth + 'px';
    },

    setStatic() {
        this.originalWidth = this.element.offsetWidth;
        JS.removeClass(this.element, this.stickyClass);
        this.element.removeAttribute('style');
    }

};

JS.addEvent(window, 'load', () => {
    propertyTypeChange.init();
    propertyAdd.init();
    propertyRemove.init();
    stickyPanel.init(JS.selectFirst('#step-1-form .alternative-panel'), 'alternative-panel---loan-for-RE-sticky');

    Calculate.init();
});