import {JS} from "./functions";
import {Validation, ValidationUI} from "./validation";

export const config = {
    personInputNames: [
        'person_code',
        'first_name',
        'last_name',
        'politician',
        'residence',
        'mmin',
        'citizenship',
    ],
    companyInputNames: [
        'company_code',
        'company_name',
        'company_country',
    ]
};

export const helpers = {
    replaceFirstArrayIndex(string, newIndex) {
        let start = string.indexOf('[');
        let end = string.indexOf(']');

        return start >= 0 && end >= 0 ? string.substring(0, start + 1) + newIndex + string.substring(end) : false;
    },

    getShareholderIndex(element) {
        return element.getAttribute('data-shareholder');
    },

    setShareholderIndex(element, index) {
        element.setAttribute('data-shareholder', index);
    },

    getBeneficiaryIndex(element) {
        return element.getAttribute('data-beneficiary');
    },

    setBeneficiaryIndex(element, index) {
        element.setAttribute('data-beneficiary', index);
    },

    setHeadingNo(element, index) {
        let heading = JS.selectFirst('.primary-panel--heading .h3', element);
        const headingValue = heading.innerHTML;

        heading.innerHTML = headingValue.substring(0, headingValue.length - 1) + index;
    },

    setNotFirst(element) {
        element.removeAttribute('data-first-property');
    },

    clearInputs(section) {
        const clear = function(inputs) {
            for(let i = 0; i < inputs.length; i++) {
                inputs[i].value = '';
                inputs[i].setAttribute('value', "");


                if(inputs[i].hasAttribute('data-toggle')) {
                    const options = JS.selectAll('option', inputs[i]);

                    for(let i = 0; i < options.length; i++) {
                        if(options[i].hasAttribute('selected')) {
                            options[i].removeAttribute('selected');
                            options[i].setAttribute('selected', 'true');
                        }
                    }
                }
            }
        };

        clear(JS.selectAll('input', section));
        clear(JS.selectAll('select', section));
    },

    setInputIndexes(section, index) {
        const update = function(input, attribute, newIndex){
            let newValue;
            if(input.hasAttribute(attribute)) {
                if(newValue = helpers.replaceFirstArrayIndex(input.getAttribute(attribute), newIndex)) {
                    input.setAttribute(attribute, newValue);
                }
            }
        };
        const initUpdate = function(inputs) {
               for(let i = 0; i < inputs.length; i++) {
                   update(inputs[i], 'id', index);
                   update(inputs[i], 'for', index);
                   update(inputs[i], 'name', index);
            }
        };

        initUpdate(JS.selectAll('input', section));
        initUpdate(JS.selectAll('label', section));
        initUpdate(JS.selectAll('select', section));
    },

    needBeneficiariesValidation() {
        const shareholders = JS.selectAll('[data-shareholder]');
        let select;

        for(let i = 0; i < shareholders.length; i++) {
            select = JS.selectFirst('select', shareholders[i]);

            if(select.value === 'company') {
                return true;
            }
        }

        return false;
    },

    hideBeneficiaries() {
        const beneficiaries = JS.selectFirst('[data-beneficiaries]');
        beneficiaries.style.display = "none";
    },

    showBeneficiaries() {
        const beneficiaries = JS.selectFirst('[data-beneficiaries]');
        beneficiaries.style.display = "block";
    }
};

export const formControl = {
    init(shareholder) {
        const self = this;
        const init = function(context) {
            const select = JS.selectFirst('select', context);

            self.handleChange(select);
            JS.addEvent(select, 'change', () => self.handleChange(select));
        };

        if(shareholder) {
            init(shareholder);
        } else {
            const shareholders = JS.selectAll('[data-shareholder]');
            for(let i = 0; i < shareholders.length; i++) {
                init(shareholders[i]);
            }
        }
    },

    handleChange(input) {
        const shareholder = input.closest('[data-shareholder]');
        const index = shareholder.getAttribute('data-shareholder');
        const beneficiaries = JS.selectFirst('[data-beneficiaries]');

        if(input.value === 'company') {
            this.hideInputs('personInputNames', index);
            this.showInputs('companyInputNames', index);
            beneficiaries.style.display = 'block';
        } else {
            this.hideInputs('companyInputNames', index);
            this.showInputs('personInputNames', index);
            if(!helpers.needBeneficiariesValidation()) {
                beneficiaries.style.display = 'none';
            }
        }
    },

    hideInputs(varName, index) {
        let inputs = this.getInputsByVarName(varName, index);

        for(let i = 0; i < inputs.length; i++) {
            let formGroup = inputs[i].closest('.form-group');
            if (formGroup) {
                formGroup.parentNode.style.display = 'none';
            } else {
                inputs[i].parentNode.style.display = 'block';
            }
        }
    },

    showInputs(varName, index) {
        let inputs = this.getInputsByVarName(varName, index);

        for(let i = 0; i < inputs.length; i++) {
            let formGroup = inputs[i].closest('.form-group');
            if (formGroup) {
                formGroup.parentNode.style.display = 'block';
            } else {
                inputs[i].parentNode.style.display = 'block';
            }
        }
    },

    getInputsByVarName(varName, index) {
        const names = config[varName];
        let result = [];

        for(let i = 0; i < names.length; i++) {
            let input = JS.selectFirst('[name="items[' + index + '][' + names[i] + ']');

            if(input) {
                result.push(input);
            }
        }

        return result;
    }
};
 
export const mminControl = {
    initOnShareHolder(shareholder) {
        const self = this;
        const init = function(context) {
            const residenceInput = self.getResidenceInput(context);

            self.handleResidenceChange(residenceInput);
            JS.addEvent(residenceInput, 'change', () => self.handleResidenceChange(residenceInput));
        };

        if(shareholder) {
            init(shareholder);
        } else {
            const shareholders = JS.selectAll('[data-shareholder]');
            for(let i = 0; i < shareholders.length; i++) {
                init(shareholders[i]);
            }
        }
    },

    initOnBeneficiary(beneficiary) {
        const self = this;
        const init = function(context) {
            const residenceInput = self.getResidenceInput(context);

            self.handleResidenceChange(residenceInput);
            JS.addEvent(residenceInput, 'change', () => self.handleResidenceChange(residenceInput));
        };

        if(beneficiary) {
            init(beneficiary);
        } else {
            const beneficiaries = JS.selectAll('[data-beneficiary]');
            for(let i = 0; i < beneficiaries.length; i++) {
                init(beneficiaries[i]);
            }
        }
    },

    getResidenceInput(context) {
        const selects = JS.selectAll('select', context);

        for(let i = 0; i < selects.length; i++) {
            let name = selects[i].getAttribute('name');

            if(name.includes('residence')) {
                return selects[i];
            }
        }
    },

    handleResidenceChange(input) {
        let context;
        let index;
        let mminInput;

        if(context = input.closest('[data-beneficiary]')) {
            index = context.getAttribute('data-beneficiary');
            mminInput = JS.selectFirst('[name="items[' + index + '][final_beneficiary_mmin]"]', context) ;
        } else if(context = input.closest('[data-shareholder]')) {
            index = context.getAttribute('data-shareholder');
            mminInput = JS.selectFirst('[name="items[' + index + '][mmin]"]', context);
        }

        const makeRequired = function(input) {
            input.setAttribute('required', '');
        };
        const removeRequired = function(input) {
            input.removeAttribute('required');
        };

        removeRequired(mminInput);

        if(+input.value !== 126) {
            makeRequired(mminInput);
        } else {
            removeRequired(mminInput);
        }
    }
};

export const addShareHolder = {
    init(shareholder) {
        const self = this;
        const init = function(context) {
           const addButton = JS.selectFirst('.property-content--add-button button', context);

           JS.addEvent(addButton, 'click', () => self.handleClick());
           JS.addEvent(addButton, 'touch', () => self.handleClick());
        };

        if(shareholder) {
            init(shareholder);
        } else {
            const shareholders = JS.selectAll('[data-shareholder]');
            for(let i = 0; i < shareholders.length; i++) {
                init(shareholders[i]);
            }
        }
    },

    handleClick() {
        const shareHolderHtml = this.getShareHolderHtml();

        let shareholdersContainer = JS.selectFirst('[data-shareholders]');

        this.showCross(shareholdersContainer.firstElementChild);

        shareholdersContainer.appendChild(shareHolderHtml);

        const select = JS.selectFirst('select', shareHolderHtml);
        formControl.handleChange(select);
    },

    getShareHolderHtml() {
        let containers = JS.selectAll('[data-shareholder]');
        let newShareholder = containers[containers.length - 1].cloneNode(true);

        const newIndex = +helpers.getShareholderIndex(newShareholder) + 1;
        helpers.setShareholderIndex(newShareholder, newIndex);
        helpers.setHeadingNo(newShareholder, newIndex + 1);
        helpers.setNotFirst(newShareholder);

        helpers.clearInputs(newShareholder);
        helpers.setInputIndexes(newShareholder, newIndex);

        formControl.init(newShareholder);
        mminControl.initOnShareHolder(newShareholder);
        removeShareHolder.initShareholder(newShareholder);
        this.init(newShareholder);
        Validate.initInputs(newShareholder);

        ValidationUI.removeErrorNodesFromSection(newShareholder);

        this.hideAddButton(newShareholder);
        this.showCross(newShareholder);

        return newShareholder;
    },

    hideAddButton(section) {
        let shareholders = JS.selectAll('[data-shareholder]');

        if(shareholders.length >= 3) {
            let addButton = JS.selectFirst('.property-content--add-button', section);
            //JS.addClass(addButton, 'property-content--add-button---hide');
            addButton.style.display = 'none';
        }
    },

    showCross(shareholder) {
        shareholder = shareholder || JS.selectFirst('[data-shareholder][data-first-property]');
        const removeButton = JS.selectFirst('.primary-panel--heading-close-button', shareholder);

        JS.removeClass(removeButton, 'primary-panel--heading-close-button---hidden')
    }
};

export const addBeneficiary = {
    init(beneficiary) {
        const self = this;
        const init = function(context) {
            const addButton = JS.selectFirst('.property-content--add-button button', context);

            JS.addEvent(addButton, 'click', () => self.handleClick());
            JS.addEvent(addButton, 'touch', () => self.handleClick());
        };

        if(beneficiary) {
            init(beneficiary);
        } else {
            const beneficiary = JS.selectAll('[data-beneficiary]');
            for(let i = 0; i < beneficiary.length; i++) {
                init(beneficiary[i]);
            }
        }
    },

    handleClick() {
        const beneficiaryHtml = this.getBeneficiaryHtml();

        let beneficiariesContainer = JS.selectFirst('[data-beneficiaries]');
        beneficiariesContainer.appendChild(beneficiaryHtml);

        this.showCross();
    },

    getBeneficiaryHtml() {
        let beneficiaries = JS.selectAll('[data-beneficiary]');
        let newBeneficiary = beneficiaries[beneficiaries.length - 1].cloneNode(true);

        const newIndex = +helpers.getBeneficiaryIndex(newBeneficiary) + 1;
        helpers.setBeneficiaryIndex(newBeneficiary, newIndex);
        helpers.setHeadingNo(newBeneficiary, newIndex + 1);
        helpers.setNotFirst(newBeneficiary);

        helpers.clearInputs(newBeneficiary);
        helpers.setInputIndexes(newBeneficiary, newIndex);

        this.init(newBeneficiary);
        removeShareHolder.initBeneficiary(newBeneficiary);
        //beneficiariesControl.init(newBeneficiary);
        Validate.initInputs(newBeneficiary);

        newBeneficiary.setAttribute('class', 'primary-panel primary-panel---with-sticking-elements');

        this.hideAddButton(newBeneficiary);
        this.showCross(newBeneficiary);

        return newBeneficiary;
    },

    hideAddButton(section) {
        let beneficiaries = JS.selectAll('[data-beneficiary]');

        if(beneficiaries.length >= 3) {
            let addButton = JS.selectFirst('.property-content--add-button', section);
            addButton.style.display = "none";
        }
    },

    showCross(beneficiary) {
        beneficiary = beneficiary || JS.selectFirst('[data-beneficiary][data-first-property]');
        const removeButton = JS.selectFirst('.primary-panel--heading-close-button', beneficiary);

        JS.removeClass(removeButton, 'primary-panel--heading-close-button---hidden')
    }
};

export const removeShareHolder = {
    initShareholder(shareholder) {
        const self = this;
        const init = function(context) {
            const removeButton = JS.selectFirst('.property-content--remove-button .cross', context);

            JS.addEvent(removeButton, 'click', () => self.handleClick(removeButton));
            JS.addEvent(removeButton, 'touch', () => self.handleClick(removeButton));
        };

        if(shareholder) {
            init(shareholder);
        } else {
            const shareholders = JS.selectAll('[data-shareholder]');
            for(let i = 0; i < shareholders.length; i++) {
                init(shareholders[i]);
            }
        }
    },

    initBeneficiary(beneficiary) {
        const self = this;
        const init = function(context) {
            const removeButton = JS.selectFirst('.property-content--remove-button .cross', context);

            JS.addEvent(removeButton, 'click', () => self.handleClick(removeButton));
            JS.addEvent(removeButton, 'touch', () => self.handleClick(removeButton));
        };

        if(beneficiary) {
            init(beneficiary);
        } else {
            const beneficiary = JS.selectAll('[data-beneficiary]');
            for(let i = 0; i < beneficiary.length; i++) {
                init(beneficiary[i]);
            }
        }
    },

    handleClick(removeButton) {
        let section;
        let index;

        if(section = removeButton.closest('[data-shareholder]')) {
            index = section.getAttribute('data-shareholder');

            if(section.parentNode.childElementCount <= 2) {
                this.hideShareholderCrosses();
            }

            if(section.parentNode.childElementCount >= 2) {
                const beneficiary = JS.selectFirst('[data-beneficiary="' + index + '"]');
                section.parentNode.removeChild(section);

                this.updateAllShareholders(index);
                this.updateAllBeneficiaries(index);
            }

        } else if(section = removeButton.closest('[data-beneficiary]')) {
            index = section.getAttribute('data-beneficiary');

            if(section.parentNode.childElementCount <= 2) {
                this.hideBeneficiariesCrosses();
            }

            if(section.parentNode.childElementCount >= 2) {
                const beneficiary = JS.selectFirst('[data-beneficiary="' + index + '"]');
                section.parentNode.removeChild(section);

                this.updateAllShareholders(index);
                this.updateAllBeneficiaries(index);
            }
        }



        if(!helpers.needBeneficiariesValidation()) {
            helpers.hideBeneficiaries();
        } else {
            helpers.showBeneficiaries();
        }
    },

    updateAllShareholders(offset = 0) {
        let shareholders = JS.selectAll('[data-shareholder]');
        for(let i = 0; i < shareholders.length; i++) {
            let currentIndex = helpers.getShareholderIndex(shareholders[i]);

            if(currentIndex > offset) {
                /* Gets new inputs index*/
                let index = currentIndex - 1;
                helpers.setShareholderIndex(shareholders[i], index)
                helpers.setInputIndexes(shareholders[i], index);

                /* Updates shareholders subheading */
                let heading = JS.selectFirst('.primary-panel--heading h3', shareholders[i]);
                let lastIndex = heading.innerHTML.lastIndexOf(" ");
                heading.innerHTML = heading.innerHTML.substring(0, lastIndex) + ' ' + (+index + 1);
            }
        }
    },

    updateAllBeneficiaries(offset = 0) {
        let beneficiaries = JS.selectAll('[data-beneficiary]');

        for(let i = 0; i < beneficiaries.length; i++) {
            let currentIndex = helpers.getBeneficiaryIndex(beneficiaries[i]);

            if(currentIndex > offset) {
                /* Gets new inputs index*/
                let index = currentIndex - 1;
                helpers.setBeneficiaryIndex(beneficiaries[i], index)
                helpers.setInputIndexes(beneficiaries[i], index);

                /* Updates beneficiaries subheading */
                let heading = JS.selectFirst('.primary-panel--heading h3', beneficiaries[i]);
                let lastIndex = heading.innerHTML.lastIndexOf(" ");
                heading.innerHTML = heading.innerHTML.substring(0, lastIndex) + ' ' + (+index + 1);
            }
        }
    },

    hideShareholderCrosses() {
        let removeButton;
        const shareholder = JS.selectAll('[data-shareholder]');

        for(let i = 0; i < shareholder.length; i++) {
            if(removeButton = JS.selectFirst('.primary-panel--heading-close-button', shareholder[i])){
                JS.addClass(removeButton, 'primary-panel--heading-close-button---hidden');
            }
        }
    },

    hideBeneficiariesCrosses() {
        let removeButton;
        const beneficiary = JS.selectAll('[data-beneficiary]');

        for(let i = 0; i < beneficiary.length; i++) {
            if(removeButton = JS.selectFirst('.primary-panel--heading-close-button', beneficiary[i])){
                JS.addClass(removeButton, 'primary-panel--heading-close-button---hidden');
            }
        }
    }
};

export const Validate = {
    initInputs(context) {
        const self = this;
        const inputs = JS.selectAll('input', context);
        const selects = JS.selectAll('select', context);
        const addEvent = function(els) {
            for(let i = 0; i < els.length; i++) {
                JS.addEvent(els[i], 'change', () => self.handleChange(els[i]));
            }
        };

        addEvent(inputs);
        addEvent(selects);
    },

    init() {
        this.form = JS.selectFirst('#ppp-form');

        if(this.form) {
            this.form.setAttribute('novalidate', '');

            JS.addEvent(this.form, 'submit', (e) => this.handleSubmit(e));
        }

        const self = this;
        const initInputs = function(selector) {
            const section = JS.selectAll(selector);

            for(let i = 0; i < section.length; i++) {
                self.initInputs(section[i]);
            }
        };

        initInputs('[data-shareholder]');
        initInputs('[data-beneficiary]');
    },

    disableButtons() {
        const submitBtns = this.form.querySelectorAll('[type="submit"]');
        [].forEach.call(submitBtns, submitBtn => {
            submitBtn.disabled = true;
        });
    },

    enableButtons() {
        const submitBtns = this.form.querySelectorAll('[type="submit"]');
        [].forEach.call(submitBtns, submitBtn => {
            submitBtn.disabled = false;
        });
    },

    handleSubmit(e) {
        e.preventDefault();

        this.disableButtons();
        this.validateInputs(this.getInputsToValidate()).then(() => {
            this.form.submit();
        }).catch((errors) => {
            this.enableButtons();
        });
    },

    handleChange(input) {
        Validation.checkInput(input).then(() => {

        }).catch((reason) => {

        });
    },

    getInputsByShareholderEl(shareholder) {
        const filterByName = function(input, invalidNames) {
            const index = shareholder.getAttribute('data-shareholder');
            for(let i = 0; i < invalidNames.length; i++) {
                if(input.getAttribute('name') === 'items[' + index + '][' + invalidNames[i] + ']') {
                    return false;
                }
            }

            return true;
        };
        const select = JS.selectFirst('select', shareholder);
        let allInputs = JS.selectAll('[name]', shareholder);
        let inputs = [];

        if(select.value === 'company') {
            for(let i = 0; i < allInputs.length; i++) {
                if(filterByName(allInputs[i], config.personInputNames)) {
                    inputs.push(allInputs[i]);
                }
            }
        } else if(select.value === 'person') {
            for(let i = 0; i < allInputs.length; i++) {
                if (filterByName(allInputs[i], config.companyInputNames)) {
                    inputs.push(allInputs[i]);
                }
            }
        }

        return inputs;
    },

    getInputsToValidate() {
        const self = this;
        const shareholders = JS.selectAll('[data-shareholder]');

        let inputs = [];

        for(let i = 0; i < shareholders.length; i++) {
            inputs = inputs.concat(this.getInputsByShareholderEl(shareholders[i]));
        }

        if(helpers.needBeneficiariesValidation()) {
            const beneficiaries = JS.selectFirst('[data-beneficiaries]');
            const beneficiaryInputs = JS.selectAll('[name]', beneficiaries);

            for(let i = 0; i < beneficiaryInputs.length; i++) {
                inputs.push(beneficiaryInputs[i]);
            }
        }

        return inputs;
    },

    validateInputs(inputs) {
        return new Promise((resolve, reject) => {
            if(inputs) {
                for (let i = 0; i < inputs.length; i++) {
                    Validation.checkInput(inputs[i]).then(() => {
                        resolve();
                    }).catch((reason) => {
                        reject(reason);
                    });
                }
            } else {
                reject();
            }
        });
    }
};

JS.addEvent(window, 'load', function() {
    addShareHolder.init();
    addBeneficiary.init();
    removeShareHolder.initShareholder();
    removeShareHolder.initBeneficiary();
    formControl.init();
    mminControl.initOnShareHolder();
    mminControl.initOnBeneficiary();
    Validate.init();
});
