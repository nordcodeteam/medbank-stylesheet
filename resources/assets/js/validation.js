import {BunnyFile} from "./file/file";
import {BunnyImage} from "./file/image";
import {Ajax} from "./bunny.ajax";
import {BunnyElement} from "./BunnyElement";
import {JS} from "./functions";

Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

export const ValidationConfig = {

    // div/node class name selector which contains one label, one input, one help text etc.
    classInputGroup: 'data-input-module',
    // class to be applied on input group node if it has invalid input
    classInputGroupError: '---with-error',
    // class to be applied on input group node if it input passed validation (is valid)
    classInputGroupSuccess: '---with-success',

    // label to pick textContent from to insert field name into error message
    classLabel: '--label',

    // error message tag name
    tagNameError: 'div',
    // error message class
    classError: '--error',
    classErrorText: '--error-text',
    classErrorIcon: '--error-icon',

    //split input error message class
    splitInputModule: 'split-input-error',

    // query selector to search inputs within input groups to validate
    selectorInput: '[name]'

};

/**
 * Bunny Form Validation default Translations (EN)
 *
 * object key = validator method name
 * may use additional parameters in rejected (invalid) Promise
 * each invalid input will receive {label} parameter anyway
 * ajax error message should be received from server via JSON response in "message" key
 */
export const ValidationLang = {

    required: window.validationTranslations.required || "'{label}' is required",
    email: window.validationTranslations.email || "'{label}' should be a valid e-mail address",
    url: window.validationTranslations.url || "{label} should be a valid website URL",
    tel: window.validationTranslations.tel || "'{label}' is not a valid telephone number",
    maxLength: window.validationTranslations.maxLength || "'{label}' length must be < '{maxLength}'",
    minLength: window.validationTranslations.minLength || "'{label}' length must be > '{minLength}'",
    maxFileSize: window.validationTranslations.maxFileSize || "Max file size must be < {maxFileSize}MB, uploaded {fileSize}MB",
    image: window.validationTranslations.image || "'{label}' should be an image (JPG or PNG)",
    minImageDimensions: window.validationTranslations.minImageDimensions || "'{label}' must be > {minWidth}x{minHeight}, uploaded {width}x{height}",
    maxImageDimensions: window.validationTranslations.maxImageDimensions || "'{label}' must be < {maxWidth}x{maxHeight}, uploaded {width}x{height}",
    requiredFromList: window.validationTranslations.requiredFromList || "Select '{label}' from list",
    confirmation: window.validationTranslations.confirmation || "'{label}' is not equal to '{originalLabel}'",
    minOptions: window.validationTranslations.minOptions || "Please select at least {minOptionsCount} options",
    minValue: window.validationTranslations.minValue || "not enough",
    maxValue: window.validationTranslations.maxValue || "too much",
    companyCode: window.validationTranslations.companyCode || "wrong company code",
    personsName: window.validationTranslations.personsName || "bad name",
    documentNumber: window.validationTranslations.documentNumber || 'bad document number',
    documentDates: window.validationTranslations.documentDates || '{message}',
    identificationCode: window.validationTranslations.identificationCode || 'bad code',
    vatCode: window.validationTranslations.vatCode || 'bad vat code',
    password: window.validationTranslations.password || 'bad password',
    uniqueNumberNotUnique: window.validationTranslations.uniqueNumberNotUnique || 'not unique',
    iban: window.validationTranslations.iban || 'wrong iban number',
};

/**
 * Bunny Validation helper - get file to validate
 * @param {HTMLInputElement} input
 * @returns {File|Blob|boolean} - If no file uploaded - returns false
 * @private
 */
const _bn_getFile = (input) => {
    // if there is custom file upload logic, for example, images are resized client-side
    // generated Blobs should be assigned to fileInput._file
    // and can be sent via ajax with FormData

    // if file was deleted, custom field can be set to an empty string

    // Bunny Validation detects if there is custom Blob assigned to file input
    // and uses this file for validation instead of original read-only input.files[]
    if (input._file !== undefined && input._file !== '') {
        if (input._file instanceof Blob === false) {
            console.error(`Custom file for input ${input.name} is not an instance of Blob`);
            return false;
        }
        return input._file;
    }
    return input.files[0] || false;
};

/**
 * Bunny Form Validation Validators
 *
 * Each validator is a separate method
 * Each validator return Promise
 * Each Promise has valid and invalid callbacks
 * Invalid callback may contain argument - string of error message or object of additional params for lang error message
 */
export const ValidationValidators = {

    required(input){
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('required')) {
                // input is required, check value
                if (
                    input.getAttribute('type') !== 'file' && input.value === ''
                    || ((input.type === 'radio' || input.type === 'checkbox') && !input.checked)
                    || input.getAttribute('type') === 'file' && _bn_getFile(input) === false) {
                    // input is empty or file is not uploaded
                    invalid();
                } else {
                    valid();
                }
            } else {
                valid();
            }
        });
    },

    email(input) {
        return new Promise((valid, invalid) => {
            if (input.value.length > 0 && input.getAttribute('type') === 'email') {
                // input is email, parse string to match email regexp
                const Regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
                if (Regex.test(input.value)) {
                    valid();
                } else {
                    invalid();
                }
            } else {
                valid();
            }
        });
    },

    url(input){
        return new Promise((valid, invalid) => {
            if (input.value.length > 0 && input.getAttribute('type') === 'url') {
                // input is URL, parse string to match website URL regexp
                const Regex = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
                if (Regex.test(input.value)) {
                    valid();
                } else {
                    invalid();
                }
            } else {
                valid();
            }
        });
    },

    tel(input){
        return new Promise((valid, invalid) => {
            if (input.value.length > 0 && input.getAttribute('type') === 'tel') {
                // input is tel, parse string to match tel regexp
                const Regex = /^[0-9\-\+\(\)\#\ \*]{6,20}$/;
                if (Regex.test(input.value)) {
                    valid();
                } else {
                    invalid();
                }
            } else {
                valid();
            }
        });
    },

    maxLength(input) {
        return new Promise((valid, invalid) => {
            if (input.getAttribute('maxlength') !== null && input.value.length > input.getAttribute('maxlength')) {
                invalid({maxLength: input.getAttribute('maxlength')});
            } else {
                valid();
            }
        });
    },

    minLength(input) {
        return new Promise((valid, invalid) => {
            if (
                //rule
                input.value.length < input.getAttribute('minlength')

                 /* when it should be applied*/
                && input.getAttribute('minlength') !== null

                /* when it should not be triggered */
                && ! (
                        input.hasAttribute('nullable')
                        && input.value.length === 0
                     )
                ) {
                invalid({minLength: input.getAttribute('minlength')});
            } else {
                valid();
            }
        });
    },

    maxFileSize(input) {
        return new Promise((valid, invalid) => {
            if (
                input.getAttribute('type') === 'file'
                && input.hasAttribute('maxfilesize')
                && _bn_getFile(input) !== false
            ) {
                const maxFileSize = parseFloat(input.getAttribute('maxfilesize')); // in MB
                const fileSize = (_bn_getFile(input).size / 1000000).toFixed(2); // in MB
                if (fileSize <= maxFileSize) {
                    valid(input);
                } else {
                    invalid({maxFileSize, fileSize});
                }
            } else {
                valid(input);
            }
        });
    },

    // if file input has "accept" attribute and it contains "image",
    // then check if uploaded file is a JPG or PNG
    image(input) {
        return new Promise((valid, invalid) => {
            if (
                input.getAttribute('type') === 'file'
                && input.getAttribute('accept').indexOf('image') > -1
                && _bn_getFile(input) !== false
            ) {
                BunnyFile.getSignature(_bn_getFile(input)).then(signature => {
                    if (BunnyFile.isJpeg(signature) || BunnyFile.isPng(signature)) {
                        valid();
                    } else {
                        invalid({signature});
                    }
                }).catch(e => {
                    invalid(e);
                });
            } else {
                valid();
            }
        });
    },

    minImageDimensions(input) {
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('mindimensions') && _bn_getFile(input) !== false) {
                const [minWidth, minHeight] = input.getAttribute('mindimensions').split('x');
                BunnyImage.getImageByBlob(_bn_getFile(input)).then(img => {
                    const width = BunnyImage.getImageWidth(img);
                    const height = BunnyImage.getImageHeight(img);
                    if (width < minWidth || height < minHeight) {
                        invalid({width: width, height: height, minWidth, minHeight});
                    } else {
                        valid();
                    }
                }).catch(e => {
                    invalid(e);
                });
            } else {
                valid();
            }
        });
    },

    maxImageDimensions(input) {
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('maxdimensions') && _bn_getFile(input) !== false) {
                const [maxWidth, maxHeight] = input.getAttribute('maxdimensions').split('x');
                BunnyImage.getImageByBlob(_bn_getFile(input)).then(img => {
                    const width = BunnyImage.getImageWidth(img);
                    const height = BunnyImage.getImageHeight(img);
                    if (width > maxWidth || height > maxHeight) {
                        invalid({width: width, height: height, maxWidth, maxHeight});
                    } else {
                        valid();
                    }
                }).catch(e => {
                    invalid(e);
                });
            } else {
                valid();
            }
        });
    },

    requiredFromList(input) {
        return new Promise((valid, invalid) => {
            let id;
            if (input.hasAttribute('requiredfromlist')) {
                id = input.getAttribute('requiredfromlist')
            } else {
                id = input.name + '_id';
            }
            const srcInput = document.getElementById(id);
            if (srcInput) {
                if (srcInput.value.length > 0) {
                    valid();
                } else {
                    invalid();
                }
            } else {
                valid();
            }
        });
    },

    minOptions(input) {
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('minoptions')) {
                const minOptionsCount = parseInt(input.getAttribute('minoptions'));
                const inputGroup = ValidationUI.getInputGroup(input);
                const hiddenInputs = inputGroup.getElementsByTagName('input');
                let selectedOptionsCount = 0;
                [].forEach.call(hiddenInputs, hiddenInput => {
                    if (hiddenInput !== input && hiddenInput.value !== '') {
                        selectedOptionsCount++
                    }
                });
                if (selectedOptionsCount < minOptionsCount) {
                    invalid({minOptionsCount});
                } else {
                    valid();
                }
            } else {
                valid();
            }
        })
    },

    confirmation(input) {
        return new Promise((valid, invalid) => {
            if (input.name.indexOf('_confirmation') > -1) {
                const originalInputId = input.name.substr(0, input.name.length - 13);
                const originalInput = document.getElementById(originalInputId);
                if (originalInput.value == input.value) {
                    valid();
                } else {
                    invalid({originalLabel: ValidationUI.getLabel(ValidationUI.getInputGroup(originalInput)).textContent});
                }
            } else {
                valid();
            }
        });
    },

    // if input's value is not empty and input has attribute "data-ajax" which should contain ajax URL with {value}
    // which will be replaced by URI encoded input.value
    // then ajax request will be made to validate input
    //
    // ajax request should return JSON response
    // if JSON response has "message" key and message key is not empty string - input is invalid
    // server should return validation error message, it may contain {label}
    // Does not works with file inputs
    ajax(input) {
        return new Promise((valid, invalid) => {
            if (input.dataset.ajax !== undefined && input.value.length > 0) {
                const url = input.dataset.ajax.replace('{value}', encodeURIComponent(input.value))
                Ajax.get(url, data => {
                    data = JSON.parse(data);
                    if (data.message !== undefined && data.message !== '') {
                        invalid(data.message);
                    } else {
                        valid();
                    }
                }, () => {
                    invalid('Ajax error');
                });
            } else {
                valid();
            }
        });
    },

    minValue(input) {
        return new Promise((valid, invalid) => {
            if (input.getAttribute('minvalue') !== null && (input.value - input.getAttribute('minvalue')) < 0) {
                invalid({minValue: input.getAttribute('minvalue')});
            } else {
                valid();
            }
        });
    },

    maxValue(input) {
        return new Promise((valid, invalid) => {
            if (input.getAttribute('maxvalue') !== null && (input.value - input.getAttribute('maxvalue')) > 0) {
                invalid({maxValue: input.getAttribute('maxvalue')});
            } else {
                valid();
            }
        });
    },

    companyCode(input) {
        return new Promise((valid, invalid) => {
            if (
                input.value.length > 0 && input.value.length < 9 &&
                input.hasAttribute('companycode') &&
                !isNaN(input.value)
            ) {
                console.log(input, input.value.length > 0, input.value.length < 9, input.hasAttribute('companycode'), !isNaN(input.value));
                invalid({inputLenght: input.length});
            } else {
                valid();
            }
        });
    },

    personsName(input) {
        return new Promise((valid, invalid) => {

            if (
                input.hasAttribute('personsname') &&
                !/^[a-z\-ąčęėįššųūž ]+$/ig.test(input.value)
            ) {
                invalid();
            } else {
                valid();
            }
        });
    },

    documentNumber(input) {
        return new Promise((valid, invalid) => {

            if (input.hasAttribute('documentnumber')) {
                let typeInput = JS.selectFirst('#' + input.getAttribute('documentnumber'));

                if (typeInput.value === 'passport') {
                    if (!/^2[0-9]{7,}$/.test(input.value)) {
                        invalid();
                    }
                } else if (typeInput.value === 'card') {
                    if (!/^1[0-9]{7,}$/.test(input.value)) {
                        invalid();
                    }
                }
            }

            valid();
        });
    },

    documentDates(input) {
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('documentexpiredate') && input.hasAttribute('pair')) {
                let releaseDateInput = JS.selectFirst('#' + input.getAttribute('pair'));

                if (releaseDateInput) {
                    let currentDate = new Date();
                    let realeaseDate = new Date(releaseDateInput.value);
                    let expireDate = new Date(input.value);

                    if (ValidationUI.dateDiffInYears(realeaseDate, expireDate) !== 10) {
                        invalid({message: 'year difference is invalid'});
                    }

                    if (ValidationUI.dateDiffInDays(expireDate, currentDate) < 0) {
                        invalid({message: 'document is expired'});
                    }

                    if (ValidationUI.dateDiffInDays(expireDate, currentDate) < 14) {
                        invalid({message: 'document will expire to soon'});
                    }
                }
            }

            valid();
        });
    },

    identificationCode(input) {
        return new Promise((valid, invalid) => {

            if (
                input.hasAttribute('identificationcode')
            ) {
                let code = input.value;

                let firstDigit = [3, 4, 5, 6];
                let date = new Date(code.substring(1, 3), code.substring(3, 5), code.substring(5, 7));

                if (code.length === 11
                    && firstDigit.indexOf(+code[0]) !== -1
                    && date.toString().length > 12
                ) {
                    console.log('cia esu');
                    let checksum = code[0] * 1 + code[1] * 2 + code[2] * 3 + code[3] * 4 + code[4] * 5 + code[5] * 6 + code[6] * 7 + code[7] * 8 + code[8] * 9 + code[9] * 1;

                    checksum = checksum % 11;

                    if (checksum === 10) {
                        checksum = code[0] * 3 + code[1] * 4 + code[2] * 5 + code[3] * 6 + code[4] * 7 + code[5] * 8 + code[6] * 9 + code[7] * 1 + code[8] * 2 + code[9] * 3;

                        checksum = checksum % 11;

                        if (checksum === 10 && +code.substring(10, 11) === 0) {
                            valid();
                        } else if (checksum === +code.substring(10, 11)) {
                            valid();
                        }
                    } else if (checksum === +code.substring(10, 11)) {
                        valid();
                    }
                }

            } else {
                valid();
            }

            invalid();
        });
    },

    vatCode(input) {
        return new Promise((valid, invalid) => {
            if (input.hasAttribute('vatcode')) {
                switch (input.value.length) {
                    case 9:
                        valid();
                        break;
                    case 12:
                        valid();
                        break;
                    case 0:
                        valid();
                        break;
                    default:
                        invalid();
                        break;
                }
            }

            valid();
        });
    },

    password(input) {
        return new Promise((valid, invalid) => {
            if(input.hasAttribute('password') &&
                (
                    !/([A-Z])/g.test(input.value) ||
                    !/([A-z])/g.test(input.value) ||
                    !/([0-9])/g.test(input.value)
                )
            ) {
                invalid();
            } else {
                valid();
            }
        });
    },

    iban(input) {
      const table = [
            {'A' : 10},
            {'B' : 11},
            {'C' : 12},
            {'D' : 13},
            {'E' : 14},
            {'F' : 15},
            {'G' : 16},
            {'H' : 17},
            {'I' : 18},
            {'J' : 19},
            {'K' : 20},
            {'L' : 21},
            {'M' : 22},
            {'N' : 23},
            {'O' : 24},
            {'P' : 25},
            {'Q' : 26},
            {'R' : 27},
            {'S' : 28},
            {'T' : 29},
            {'U' : 30},
            {'V' : 31},
            {'W' : 32},
            {'X' : 33},
            {'Y' : 34},
            {'Z' : 35},
        ];
        const getValue = function (letter) {
            for(let i = 0; i < table.length; i++) {
                if(table[i][letter]) {
                    return table[i][letter]
                }
            }
        };

        const getNewNumber = function(_number, prefixNumber) {
            return _number.substring(2) + prefixNumber + _number.substring(0, 2);
        };

        const mod97 = function(string) {
            let checksum = string.slice(0, 2), fragment;
            for (let offset = 2; offset < string.length; offset += 7) {
                fragment = String(checksum) + string.substring(offset, offset + 7);
                checksum = parseInt(fragment, 10) % 97;
            }
            return checksum;
        };

        return new Promise((valid, invalid) => {
            if(input.hasAttribute('iban') && input.value.length >= 18) {
                const prefix = JS.selectFirst('.primary-input--prefix', input.parentNode).innerHTML;

                let prefixInNumbers = '';
                for(let i = 0; i < prefix.length; i++) {
                    prefixInNumbers += getValue(prefix[i]);
                }

                if(mod97(getNewNumber(input.value, prefixInNumbers))!== 1) {
                    invalid();
                }
            }

            valid();
        });
    }
};

export const GroupedValidationUI = {
    getInputs(section) {
        return JS.selectAll('input', section);
    },

    getInputsValues(inputs) {
        let values = [];

        for(let i = 0; i < inputs.length; i++) {
            values.push(inputs[i].value);
        }

        return values;
    },

    getValidator(group) {
        return group.getAttribute('data-grouped-validation');
    },

    arraysEqual(a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;

        for (var i = 0; i < a.length; ++i) {
            if (a[i] !== b[i]) return false;
        }
        return true;
    }
};

export const GroupedValidationValidators = {
    uniqueInputs(group) {
        const allGroups = JS.selectAll('[data-grouped-validation]');

        return new Promise((valid, invalid) => {
           if(GroupedValidationUI.getValidator(group) === 'unique-inputs') {
               const inputs = GroupedValidationUI.getInputs(group);
               const values = GroupedValidationUI.getInputsValues(inputs);

               let faultyGroups = [];
               for(let i = 0; i < allGroups.length; i++) {
                   let otherGroup = allGroups[i];
                   if(otherGroup !== group) {
                       const otherValues = GroupedValidationUI.getInputsValues(GroupedValidationUI.getInputs(otherGroup));

                       if(GroupedValidationUI.arraysEqual(values, otherValues)) {
                           faultyGroups.push(otherGroup);
                       }
                   }
               }

               if(faultyGroups.length > 0) {
                   invalid(faultyGroups);
               } else {
                   valid();
               }
           }
        });
    }
};

/**
 * @package BunnyJS
 * @component Validation
 *
 * Base Object to work with DOM, creates error messages
 * and searches for inputs within "input groups" and related elements
 * Each input should be wrapped around an "input group" element
 * Each "input group" should contain one input, may contain one label
 * Multiple inputs within same "Input group" should not be used for validation
 * <fieldset> is recommended to be used to wrap more then one input
 */
export const ValidationUI = {

    config: ValidationConfig,

    dateDiffInYears(dateold, datenew) {
        let ynew = datenew.getFullYear();
        let mnew = datenew.getMonth();
        let dnew = datenew.getDate();
        let yold = dateold.getFullYear();
        let mold = dateold.getMonth();
        let dold = dateold.getDate();
        let diff = ynew - yold;
        if (mold > mnew) diff--;
        else {
            if (mold === mnew) {
                if (dold > dnew) diff--;
            }
        }
        return diff;
    },

    dateDiffInDays(dateold, datenew) {
        let timeDiff = dateold.getTime() - datenew.getTime();

        return Math.ceil(timeDiff / (1000 * 3600 * 24));
    },

    /* ************************************************************************
     * ERROR MESSAGE
     */

    /**
     * DOM algorithm - where to insert error node/message
     *
     * @param {HTMLElement} inputGroup
     * @param {HTMLElement} errorNode
     */
    insertErrorNode(inputGroup, errorNode) {
        inputGroup.appendChild(errorNode);
    },

    insertSplitInputErrorNode(inputGroup, errorNode) {
        inputGroup.appendChild(errorNode);
    },


    /**
     * DOM algorithm - where to add/remove error class
     *
     * @param {HTMLElement} inputGroup
     */
    toggleErrorClass(inputGroup) {
        const module = inputGroup.getAttribute(this.config.classInputGroup);
        inputGroup.classList.toggle(module + this.config.classInputGroupError);
    },

    toggleSplitInputErrorClass(inputGroup) {
        const module = this.config.classInputGroup;
        inputGroup.classList.toggle(module + this.config.classInputGroupError);
    },


    /**
     * Create DOM element for error message
     *
     * @returns {HTMLElement}
     */
    createErrorNode(inputGroup, message) {
        const module = inputGroup.getAttribute(this.config.classInputGroup);

        let el = document.createElement(this.config.tagNameError);
        el.classList.add(module + this.config.classError);

        let errorTextEl = el.appendChild(document.createElement('div'));
        errorTextEl.classList.add(module + this.config.classErrorText);
        errorTextEl.textContent = message;

        let errorIconEl = el.appendChild(document.createElement('div'));
        errorIconEl.classList.add(module + this.config.classErrorIcon);

        el.appendChild(errorTextEl);
        el.appendChild(errorIconEl);

        return el;
    },

    createSplitInputErrorNode(message) {
        const module = this.config.splitInputModule;

        let el = document.createElement(this.config.tagNameError);
        el.classList.add(module + this.config.classError);

        let errorTextEl = el.appendChild(document.createElement('div'));
        errorTextEl.classList.add(module + this.config.classErrorText);
        errorTextEl.textContent = message;

        let errorIconEl = el.appendChild(document.createElement('div'));
        errorIconEl.classList.add(module + this.config.classErrorIcon);

        el.appendChild(errorTextEl);
        el.appendChild(errorIconEl);

        return el;
    },


    /**
     * Find error message node within input group or false if not found
     *
     * @param {HTMLElement} inputGroup
     *
     * @returns {HTMLElement|boolean}
     */
    getErrorNode(inputGroup) {
        const module = inputGroup.getAttribute(this.config.classInputGroup);
        return inputGroup.getElementsByClassName(module + this.config.classError)[0] || false;
    },

    getSplitInputErrorNode(inputGroup) {
        const module = this.config.splitInputModule;
        return inputGroup.parentNode.getElementsByClassName(module + this.config.classError)[0] || false;
    },

    getErrorTextNode(inputGroup) {
        const module = inputGroup.getAttribute(this.config.classInputGroup);
        return inputGroup.getElementsByClassName(module + this.config.classErrorText)[0] || false;
    },

    getSplitInputErrorTextNode(inputGroup) {
        const module = this.config.splitInputModule;
        return inputGroup.parentNode.getElementsByClassName(module + this.config.classErrorText)[0] || false;
    },

    /**
     * Removes error node and class from input group if exists
     *
     * @param {HTMLElement} inputGroup
     */
    removeErrorNode(inputGroup) {
        const el = this.getErrorNode(inputGroup);
        if (el) {
            el.parentNode.removeChild(el);
            this.toggleErrorClass(inputGroup);
        }
    },

    removeSplitInputErrorNode(groupedValidationNode) {
        const el = this.getSplitInputErrorNode(groupedValidationNode);
        const module = this.config.splitInputModule;

        if (el) {
            el.parentNode.removeChild(el);
            groupedValidationNode.classList.remove(module + this.config.classInputGroupError);
        }
    },


    /**
     * Removes all error node and class from input group if exists within section
     *
     * @param {HTMLElement} section
     */
    removeErrorNodesFromSection(section) {
        [].forEach.call(this.getInputGroupsInSection(section), inputGroup => {
            this.removeErrorNode(inputGroup);
        });
    },

    removeSplitInputErrorNodesFromSection(section) {
        [].forEach.call(JS.selectAll('[data-grouped-validation]', section), groupedValidationInputs => {
            this.removeSplitInputErrorNode(groupedValidationInputs);
        });
    },

    setSplitInputErrorMessage(inputGroup, message) {
        let errorNode = this.getSplitInputErrorTextNode(inputGroup);
        if (errorNode === false) {
            // container for error message doesn't exists, create new
            errorNode = this.createSplitInputErrorNode(message);
            this.toggleSplitInputErrorClass(inputGroup);
            this.insertSplitInputErrorNode(inputGroup, errorNode)
        } else {
            errorNode.textContent = message;
        }
    },

    /**
     * Creates and includes into DOM error node or updates error message
     *
     * @param {HTMLElement} inputGroup
     * @param {String} message
     */
    setErrorMessage(inputGroup, message) {
        let errorNode = this.getErrorTextNode(inputGroup);
        if (errorNode === false) {
            // container for error message doesn't exists, create new
            errorNode = this.createErrorNode(inputGroup, message);
            this.toggleErrorClass(inputGroup);
            this.insertErrorNode(inputGroup, errorNode)
        } else {
            errorNode.textContent = message;
        }
    },


    /**
     * Marks input as valid
     *
     * @param {HTMLElement} inputGroup
     */
    setInputValid(inputGroup) {
        const module = inputGroup.getAttribute(this.config.classInputGroup);
        inputGroup.classList.add(module + this.config.classInputGroupSuccess);
    },


    /* ************************************************************************
     * SEARCH DOM
     */

    /**
     * DOM Algorithm - which inputs should be selected for validation
     *
     * @param {HTMLElement} inputGroup
     *
     * @returns {HTMLElement|boolean}
     */
    getInput(inputGroup) {
        return inputGroup.querySelector(this.config.selectorInput) || false;
    },


    /**
     * Find closest parent inputGroup element by Input element
     *
     * @param {HTMLElement} input
     *
     * @returns {HTMLElement}
     */
    getInputGroup(input) {
        let el = input;
        while ((el = el.parentNode) && !el.getAttribute(this.config.classInputGroup));
        return el;
    },


    /**
     * Find inputs in section
     *
     * @meta if second argument true - return object with meta information to use during promise resolving
     *
     * @param {HTMLElement} node
     * @param {boolean} resolving = false
     *
     * @returns {Array|Object}
     */
    getInputsInSection(node, resolving = false) {
        const inputGroups = this.getInputGroupsInSection(node);
        let inputs;
        if (resolving) {
            inputs = {
                inputs: {},
                invalidInputs: {},
                length: 0,
                unresolvedLength: 0,
                invalidLength: 0
            };
        } else {
            inputs = [];
        }
        for (let k = 0; k < inputGroups.length; k++) {
            const input = this.getInput(inputGroups[k]);
            if (input === false) {
                console.error(inputGroups[k]);
                throw new Error('Bunny Validation: Input group has no input');
            }
            if (resolving) {
                inputs.inputs[k] = {
                    input: input,
                    isValid: null
                };
                inputs.length++;
                inputs.unresolvedLength++;
            } else {
                inputs.push(input);
            }
        }
        return inputs;
    },


    /**
     * Find label associated with input within input group
     *
     * @param {HTMLElement} inputGroup
     *
     * @returns {HTMLElement|boolean}
     */
    getLabel(inputGroup) {
        return inputGroup.getElementsByTagName('label')[0] || false;
    },


    /**
     * Find all input groups within section
     *
     * @param {HTMLElement} node
     *
     * @returns {HTMLCollection}
     */
    getInputGroupsInSection(node) {
        return node.querySelectorAll('[' + this.config.classInputGroup + ']');
    },


    /* First form step only */
    removeErrorFromHiddenInputs(section) {
        let groups = JS.selectAll('.property-content--animated-inputs', section);

        for(let i = 0; i < groups.length; i++) {
            if (!JS.hasClass(groups[i], 'property-content--animated-inputs---active')) {
                ValidationUI.removeErrorNodesFromSection(groups[i]);
            }
        }
    }
};

export const Validation = {

    validators: ValidationValidators,
    groupedValidators: GroupedValidationValidators,
    lang: ValidationLang,
    ui: ValidationUI,
    reCaptcha: true,
    minLoanValue: true,

    init(form, inline = false) {
        let self = this;

        // disable browser built-in validation
        form.setAttribute('novalidate', '');

        form.addEventListener('submit', e => {
            e.preventDefault();
            this.validateMinLoanValue();

            const submitBtns = form.querySelectorAll('[type="submit"]');
            [].forEach.call(submitBtns, submitBtn => {
                submitBtn.disabled = true;
            });
            this.validateSection(form).then(result => {
                [].forEach.call(submitBtns, submitBtn => {
                    submitBtn.disabled = false;
                });
                self.validateGrouped().then(() => {
                    if (result === true && this.reCaptcha && this.minLoanValue) {
                        form.submit();
                    } else {
                        this.focusInput(result[0]);
                        ValidationUI.removeErrorFromHiddenInputs(form);
                    }
                }).catch((faultyGroups) => {
                    const filteredGroups = self.concatAndRemoveDublicate(faultyGroups);

                    for(let i = filteredGroups.length - 1; i >= 0; i--) {
                        if(i < filteredGroups.length - 1) {
                            self.addErrorToGroup(filteredGroups[i]);
                        }
                    }
                });
            });
        });

        if (inline) {
            this.initInline(form);
        }
    },

    initInline(node) {
        const inputs = this.ui.getInputsInSection(node);

        let self = this;
        let i = 0;
        let interval = [];
        let checkExpireDateInput = function (input) {
            let expireDateInput = JS.selectFirst('#' + input.getAttribute('pair'));

            if (expireDateInput && expireDateInput.value !== '') {
                self.checkInput(expireDateInput).catch(e => {
                });
            }
        };

        inputs.forEach(input => {
            i++;

            input.addEventListener('change', () => {
                if(input.type !== 'checkbox') {
                    clearTimeout(interval[i]);
                    this.checkInput(input).catch(e => {
                    });
                }
            });
            input.addEventListener('keyup', (e) => {
                let errorNode = ValidationUI.getErrorTextNode(ValidationUI.getInputGroup(input));

                if (errorNode) {
                    this.checkInput(input).catch(e => {
                    });
                }
            });
            input.addEventListener('blur', (e) => {
                let errorNode = ValidationUI.getErrorTextNode(ValidationUI.getInputGroup(input));

                if (errorNode) {
                    this.checkInput(input).catch(e => {
                    });
                }

                if (input.hasAttribute('documentreleasedate')) {
                    checkExpireDateInput(input);
                }
            });

            input.addEventListener('input', (e) => {
                if (input.getAttribute('maxlength') !== null && input.value.length > input.getAttribute('maxlength')) {
                    input.value = input.value.slice(0, input.getAttribute('maxlength'))
                }
            });
        })
    },

    concatAndRemoveDublicate(array) {
        let newArray = [];

        for(let i = 0; i < array.length; i++) {
            newArray = newArray.concat(array[i]).unique();
        }

        return newArray;
    },

    addErrorToCaptcha() {
        let captchaEl = JS.selectFirst('#g-recaptcha-error');

        JS.removeClass(captchaEl, 'primary-input--error---hide');
    },

    addErrorToGroup(group) {
        ValidationUI.setSplitInputErrorMessage(group.parentNode, ValidationLang.uniqueNumberNotUnique);
    },

    removeErrorFromCaptcha() {
        let captchaEl = JS.selectFirst('#g-recaptcha-error');

        JS.addClass(captchaEl, 'primary-input--error---hide');
    },

    validateGrouped() {
        return new Promise((resolve, reject) => {
            const groups = JS.selectAll('[data-grouped-validation]');

            if(groups.length < 2)
                resolve();

            let faultyGroups = [];
            let finished = 1;

            for(let i = 0; i < groups.length; i++) {
                const validators = Object.keys(this.groupedValidators);

                for(let j = 0; j < validators.length; j++) {
                    const currentValidatorName = validators[j];
                    const currentValidator = this.groupedValidators[currentValidatorName];

                    currentValidator(groups[i]).then(() => {
                        finished++;
                        if(finished === groups.length) {
                            if(faultyGroups.length > 0) {
                                reject(faultyGroups);
                            } else {
                                ValidationUI.removeSplitInputErrorNode(groups[i]);
                                resolve();
                            }
                        }
                    }).catch((reason) => {
                        finished++;
                        faultyGroups.push(reason);
                        if(finished === groups.length) {
                            reject(faultyGroups);
                        }
                    });
                }
            }
        });
    },

    validateMinLoanValue() {
        let self = this;
        let maxAmount = JS.selectFirst('.alternative-panel--amount-value[calculated]');
        let offerToSmall = function() {
            self.minLoanValue = false;

            window.flash.new('error', 'flash-info', window.flashTranslations.errorOfferNotEnough);
        };

        if(maxAmount) {
            maxAmount = maxAmount.innerHTML.replace('€', '').replace(/\s+/g, '').replace(/[A-z]+/g, '');
            if (!isNaN(maxAmount)) {
                if (window.minLoanAmount) {
                    if (window.minLoanAmount > +maxAmount) {
                        offerToSmall();
                    } else {
                        this.minLoanValue = true;
                    }
                }
            } else {
                offerToSmall();
            }
        }
    },

    validateSection(node) {
        let self = this;

        if (node.querySelector('.g-recaptcha')) {
            if (grecaptcha.getResponse() === '') {
                this.reCaptcha = false;
                self.addErrorToCaptcha();
            } else {
                this.reCaptcha = true;
                self.removeErrorFromCaptcha();
            }
        }

        if (node.__bunny_validation_state === undefined) {
            node.__bunny_validation_state = true;
        } else {
            throw new Error('Bunny Validation: validation already in progress.');
        }
        return new Promise(resolve => {
            const resolvingInputs = this.ui.getInputsInSection(node, true);
            if (resolvingInputs.length === 0) {
                // nothing to validate, end
                this._endSectionValidation(node, resolvingInputs, resolve);
            } else {
                // run async validation for each input
                // when last async validation will be completed, call validSection or invalidSection
                let promises = [];
                for (let i = 0; i < resolvingInputs.length; i++) {
                    const input = resolvingInputs.inputs[i].input;

                    this.checkInput(input).then(() => {
                        this._addValidInput(resolvingInputs, input);
                        if (resolvingInputs.unresolvedLength === 0) {
                            this._endSectionValidation(node, resolvingInputs, resolve);
                        }
                    }).catch(errorMessage => {
                        this._addInvalidInput(resolvingInputs, input);
                        if (resolvingInputs.unresolvedLength === 0) {
                            this._endSectionValidation(node, resolvingInputs, resolve);
                        }
                    });
                }

                // if there are not resolved promises after 3s, terminate validation, mark pending inputs as invalid
                setTimeout(() => {
                    if (resolvingInputs.unresolvedLength > 0) {
                        let unresolvedInputs = this._getUnresolvedInputs(resolvingInputs);
                        for (let i = 0; i < unresolvedInputs.length; i++) {
                            const input = unresolvedInputs[i];
                            const inputGroup = this.ui.getInputGroup(input);
                            this._addInvalidInput(resolvingInputs, input);
                            this.ui.setErrorMessage(inputGroup, 'Validation terminated after 3s');
                            if (resolvingInputs.unresolvedLength === 0) {
                                this._endSectionValidation(node, resolvingInputs, resolve);
                            }
                        }
                    }
                }, 3000);
            }
        });
    },

    focusInput(input, delay = 500, offset = -50, scroll = true) {
        if (input) {
            if (scroll) {
                BunnyElement.scrollTo(input, delay, offset);
            }
            input.focus();

            if (
                input.offsetParent !== null
                && input.setSelectionRange !== undefined
                && ['text', 'search', 'url', 'tel', 'password'].indexOf(input.type) !== -1
                && typeof input.setSelectionRange === 'function'
            ) {
                input.setSelectionRange(input.value.length, input.value.length);
            }
        }
    },

    checkInput(input) {
        return new Promise((valid, invalid) => {
            this._checkInput(input, 0, valid, invalid);
        });
    },

    _addValidInput(resolvingInputs, input) {
        resolvingInputs.unresolvedLength--;
        for (let k in resolvingInputs.inputs) {
            if (input === resolvingInputs.inputs[k].input) {
                resolvingInputs.inputs[k].isValid = true;
                break;
            }
        }
    },

    _addInvalidInput(resolvingInputs, input) {
        resolvingInputs.unresolvedLength--;
        resolvingInputs.invalidLength++;
        for (let k in resolvingInputs.inputs) {
            if (input === resolvingInputs.inputs[k].input) {
                resolvingInputs.inputs[k].isValid = false;
                resolvingInputs.invalidInputs[k] = input;
                break;
            }
        }
    },

    _getUnresolvedInputs(resolvingInputs) {
        let unresolvedInputs = [];
        for (let k in resolvingInputs.inputs) {
            if (!resolvingInputs.inputs[k].isValid) {
                unresolvedInputs.push(resolvingInputs.inputs[k].input);
            }
        }
        return unresolvedInputs;
    },

    _endSectionValidation(node, resolvingInputs, resolve) {
        delete node.__bunny_validation_state;

        if (resolvingInputs.invalidLength === 0) {
            // form or section is valid
            return resolve(true);
        } else {
            let invalidInputs = [];
            for (let k in resolvingInputs.invalidInputs) {
                invalidInputs.push(resolvingInputs.invalidInputs[k]);
            }
            // form or section has invalid inputs
            return resolve(invalidInputs);
        }
    },

    _checkInput(input, index, valid, invalid) {
        const validators = Object.keys(this.validators);
        const currentValidatorName = validators[index];
        const currentValidator = this.validators[currentValidatorName];


        currentValidator(input).then(() => {
            index++;
            if (validators[index] !== undefined) {
                this._checkInput(input, index, valid, invalid)
            } else {
                const inputGroup = this.ui.getInputGroup(input);
                // if has error message, remove it
                this.ui.removeErrorNode(inputGroup);

                if (input.form !== undefined && input.form.hasAttribute('showvalid')) {
                    // mark input as valid
                    this.ui.setInputValid(inputGroup);
                }

                valid();
            }
        }).catch(data => {
            // get input group and label
            const inputGroup = this.ui.getInputGroup(input);
            const label = this.ui.getLabel(inputGroup);

            // get error message
            const errorMessage = this._getErrorMessage(currentValidatorName, input, label, data);

            // set error message
            this.ui.setErrorMessage(inputGroup, errorMessage);
            invalid(errorMessage);
        });
    },

    _getErrorMessage(validatorName, input, label, data) {
        let message = '';
        if (typeof data === 'string') {
            // if validator returned string (from ajax for example), use it
            message = data;
        } else {
            if (this.lang[validatorName] === undefined) {
                throw new Error('Bunny Validation: Lang message not found for validator: ' + validatorName);
            }
            message = this.lang[validatorName];
        }

        // replace params in error message
        if (label !== false) {
            message = message.replace('{label}', label.textContent);
        } else if (input.placeholder && input.placeholder !== '') {
            message = message.replace('{label}', input.placeholder);
        } else {
            message = message.replace('{label}', '');
        }

        for (let paramName in data) {
            message = message.replace('{' + paramName + '}', data[paramName]);
        }
        return message;
    },
};

document.addEventListener('DOMContentLoaded', () => {
    [].forEach.call(document.forms, form => {
        if (form.getAttribute('validator') === 'bunny') {
            const inline = form.hasAttribute('validator-inline');
            Validation.init(form, inline);
        }
    });
});
