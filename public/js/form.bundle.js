/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	__webpack_require__(212);
	__webpack_require__(219);
	module.exports = __webpack_require__(214);


/***/ }),

/***/ 212:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.selectFocus = exports.FormValidation = undefined;

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _functions = __webpack_require__(213);

	var _validation = __webpack_require__(214);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var FormValidation = exports.FormValidation = function () {
	    function FormValidation() {
	        _classCallCheck(this, FormValidation);
	    }

	    _createClass(FormValidation, [{
	        key: "autoInit",
	        value: function autoInit() {
	            var formsToValidate = _functions.JS.selectAll('[js-validate]');

	            for (var i = 0; i < formsToValidate.length; i++) {
	                _validation.Validation.init(formsToValidate[i], true);
	            }
	        }
	    }]);

	    return FormValidation;
	}();

	var selectFocus = exports.selectFocus = {
	    init: function init() {
	        var _this = this;

	        var inputs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

	        this.inputs = inputs || _functions.JS.selectAll('.primary-input--field[data-toggle]');

	        if (_typeof(this.inputs) === 'object') {
	            for (var i = 0; i < this.inputs.length; i++) {
	                this.addEvents(this.inputs[i]);
	            }
	        } else {
	            this.addEvents(inputs);
	        }

	        _functions.JS.addEvent(document, 'click', function (e) {
	            return _this.handleClick(e);
	        });
	    },
	    addEvents: function addEvents(input) {
	        // JS.addEvent(input, 'focus', () => this.handleFocus(input));
	        // JS.addEvent(input, 'blur', () => this.handleBlur(input));
	    },
	    closeAllSelects: function closeAllSelects() {
	        var inputs = _functions.JS.selectAll('[data-toggle]');

	        for (var i = 0; i < inputs.length; i++) {
	            inputs[i].setAttribute('data-toggle', 'blur');
	        }
	    },
	    handleClick: function handleClick(e) {
	        var toggleState = e.target.getAttribute('data-toggle');

	        this.closeAllSelects();

	        if (_functions.JS.hasClass(e.target, 'primary-input--field')) {
	            if (toggleState === 'blur') {
	                e.target.focus();
	                e.target.setAttribute('data-toggle', 'focus');
	            } else if (toggleState === 'focus') {
	                e.target.blur();
	                e.target.setAttribute('data-toggle', 'blur');
	            }
	        } else {
	            this.blurInput();
	        }
	    },
	    blurInput: function blurInput() {
	        var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

	        var handleIt = function handleIt(input) {
	            input.blur();
	            input.setAttribute('data-toggle', 'blur');
	        };

	        if (input === null) {
	            for (var i = 0; i < this.inputs.length; i++) {
	                handleIt(this.inputs[i]);
	            }
	        } else {
	            handleIt(input);
	        }
	    },
	    focusInput: function focusInput() {
	        var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

	        var handleIt = function handleIt(input) {
	            input.focus();
	            input.setAttribute('data-toggle', 'focus');
	        };

	        if (input === null) {
	            for (var i = 0; i < this.inputs.length; i++) {
	                handleIt(this.inputs[i]);
	            }
	        } else {
	            handleIt(input);
	        }
	    }
	};

	var formValidation = void 0;
	_functions.JS.addEvent(window, 'load', function () {
	    formValidation = new FormValidation().autoInit();
	    selectFocus.init();
	});

/***/ }),

/***/ 213:
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var JS = exports.JS = {
	    addEvent: function addEvent(el, type, handler) {
	        if (!el) return;
	        if (el.attachEvent) el.attachEvent('on' + type, handler);else el.addEventListener(type, handler);
	    },
	    removeEvent: function removeEvent(el, type, handler) {
	        if (!el) return;
	        if (el.detachEvent) el.detachEvent('on' + type, handler);else el.removeEventListener(type, handler);
	    },
	    selectAll: function selectAll(selector, context) {
	        return (context || document).querySelectorAll(selector);
	    },
	    selectFirst: function selectFirst(selector, context) {
	        return (context || document).querySelector(selector);
	    },
	    hasClass: function hasClass(el, className) {
	        return el ? el.classList.contains(className) : false;
	    },
	    addClass: function addClass(el, className) {
	        var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	        if (el && !el.classList.contains(className)) el.classList.add(className);
	        if (callback) callback();
	    },
	    removeClass: function removeClass(el, className) {
	        var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	        if (el && el.classList.contains(className)) el.classList.remove(className);
	        if (callback) callback();
	    },
	    isNumber: function isNumber(obj) {
	        return !isNaN(parseFloat(obj));
	    },
	    closestByClass: function closestByClass(el, className) {
	        while (!el.classList.contains(className)) {
	            el = el.parentNode;
	            if (!el) {
	                return null;
	            }
	        }
	        return el;
	    },
	    postAjax: function postAjax(url, data, success) {
	        var params = typeof data == 'string' ? data : Object.keys(data).map(function (k) {
	            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]);
	        }).join('&');

	        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	        xhr.open('POST', url);
	        xhr.onreadystatechange = function () {
	            if (xhr.readyState > 3 && xhr.status == 200) {
	                success(xhr.responseText);
	            }
	        };
	        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	        xhr.send(params);
	        return xhr;
	    },
	    scrollTo: function scrollTo(elementY, duration) {
	        var startingY = window.pageYOffset;
	        var diff = elementY - startingY;
	        var start;

	        // Bootstrap our animation - it will get called right before next frame shall be rendered.
	        window.requestAnimationFrame(function step(timestamp) {
	            if (!start) start = timestamp;
	            // Elapsed miliseconds since start of scrolling.
	            var time = timestamp - start;
	            // Get percent of completion in range [0, 1].
	            var percent = Math.min(time / duration, 1);

	            window.scrollTo(0, startingY + diff * percent);

	            // Proceed with animation as long as we wanted it to.
	            if (time < duration) {
	                window.requestAnimationFrame(step);
	            }
	        });
	    },
	    getPosition: function getPosition(el) {
	        var elm = el;
	        var y = elm.offsetTop;
	        var node = elm;
	        while (node.offsetParent && node.offsetParent != document.body) {
	            node = node.offsetParent;
	            y += node.offsetTop;
	        }return y;
	    },
	    loadScript: function loadScript(url, callback) {

	        var script = document.createElement("script");
	        script.type = "text/javascript";

	        if (script.readyState) {
	            //IE
	            script.onreadystatechange = function () {
	                if (script.readyState == "loaded" || script.readyState == "complete") {
	                    script.onreadystatechange = null;
	                    callback();
	                }
	            };
	        } else {
	            //Others
	            script.onload = function () {
	                callback();
	            };
	        }

	        script.src = url;
	        document.getElementsByTagName("head")[0].appendChild(script);
	    },
	    isSet: function isSet(obj) {
	        return typeof obj !== "undefined";
	    },
	    triggerEvent: function triggerEvent(el, type) {
	        if ('createEvent' in document) {
	            // modern browsers, IE9+
	            var e = document.createEvent('HTMLEvents');
	            e.initEvent(type, false, true);
	            el.dispatchEvent(e);
	        } else {
	            // IE 8
	            var e = document.createEventObject();
	            e.eventType = type;
	            el.fireEvent('on' + e.eventType, e);
	        }
	    },
	    onTransitionEnd: function onTransitionEnd() {
	        var t;
	        var el = document.createElement('fakeelement');
	        var transitions = {
	            'transition': 'transitionend',
	            'OTransition': 'oTransitionEnd',
	            'MozTransition': 'transitionend',
	            'WebkitTransition': 'webkitTransitionEnd'
	        };

	        for (t in transitions) {
	            if (el.style[t] !== undefined) {
	                return transitions[t];
	            }
	        }
	    },
	    style: function style(el) {
	        return el.currentStyle || window.getComputedStyle(el);
	    },
	    extend: function extend(obj, src) {
	        for (var key in src) {
	            if (src.hasOwnProperty(key)) obj[key] = src[key];
	        }
	        return obj;
	    },
	    getScrollbarWidth: function getScrollbarWidth() {
	        var inner = document.createElement('p');
	        inner.style.width = "100%";
	        inner.style.height = "200px";

	        var outer = document.createElement('div');
	        outer.style.position = "absolute";
	        outer.style.top = "0px";
	        outer.style.left = "0px";
	        outer.style.visibility = "hidden";
	        outer.style.width = "200px";
	        outer.style.height = "150px";
	        outer.style.overflow = "hidden";
	        outer.appendChild(inner);

	        document.body.appendChild(outer);
	        var w1 = inner.offsetWidth;
	        outer.style.overflow = 'scroll';
	        var w2 = inner.offsetWidth;

	        if (w1 == w2) {
	            w2 = outer.clientWidth;
	        }

	        document.body.removeChild(outer);

	        return w1 - w2;
	    },
	    isMobile: function isMobile() {
	        var isMobile = false;
	        (function (a, b) {
	            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) isMobile = b;
	        })(navigator.userAgent || navigator.vendor || window.opera, true);

	        return isMobile;
	    },
	    is_touch_device: function is_touch_device() {
	        try {
	            document.createEvent("TouchEvent");
	            return true;
	        } catch (e) {
	            return false;
	        }
	    },
	    windows: function windows() {
	        var w = window,
	            d = document,
	            e = d.documentElement,
	            g = d.getElementsByTagName('body')[0],
	            x = w.innerWidth || e.clientWidth || g.clientWidth,
	            y = w.innerHeight || e.clientHeight || g.clientHeight;

	        return { width: x, height: y };
	    },
	    getXhr: function getXhr(method, url) {
	        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');

	        xhr.open(method, url, true);
	        xhr.withCredentials = true;
	        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');

	        return xhr;
	    },
	    getPreloader: function getPreloader() {
	        return JS.selectFirst('.prefab[data-prefab="preloader"]').firstElementChild.cloneNode(true);
	    }
	};

	// this.Element && function (ElementPrototype) {
	//     ElementPrototype.matches = ElementPrototype.matches ||
	//         ElementPrototype.matchesSelector ||
	//         ElementPrototype.webkitMatchesSelector ||
	//         ElementPrototype.msMatchesSelector ||
	//         function (selector) {
	//             var node = this, nodes = (node.parentNode || node.document).querySelectorAll(selector), i = -1;
	//             while (nodes[++i] && nodes[i] != node);
	//             return !!nodes[i];
	//         }
	// }(Element.prototype);
	//
	// this.Element && function(ElementPrototype) {
	//     ElementPrototype.closest = ElementPrototype.closest ||
	//         function(selector) {
	//             var el = this;
	//             while (el.matches && !el.matches(selector)) el = el.parentNode;
	//             return el.matches ? el : null;
	//         }
	// }(Element.prototype);

	if (!String.prototype.trim) {
	    String.prototype.trim = function () {
	        return this.replace(/^\s+|\s+$/g, '');
	    };
	}

	if (!String.prototype.repeat) {
	    String.prototype.repeat = function (count) {
	        'use strict';

	        if (this == null) {
	            throw new TypeError('can\'t convert ' + this + ' to object');
	        }
	        var str = '' + this;
	        count = +count;
	        if (count != count) {
	            count = 0;
	        }
	        if (count < 0) {
	            throw new RangeError('repeat count must be non-negative');
	        }
	        if (count == Infinity) {
	            throw new RangeError('repeat count must be less than infinity');
	        }
	        count = Math.floor(count);
	        if (str.length == 0 || count == 0) {
	            return '';
	        }
	        // Ensuring count is a 31-bit integer allows us to heavily optimize the
	        // main part. But anyway, most current (August 2014) browsers can't handle
	        // strings 1 << 28 chars or longer, so:
	        if (str.length * count >= 1 << 28) {
	            throw new RangeError('repeat count must not overflow maximum string size');
	        }
	        var rpt = '';
	        for (;;) {
	            if ((count & 1) == 1) {
	                rpt += str;
	            }
	            count >>>= 1;
	            if (count == 0) {
	                break;
	            }
	            str += str;
	        }
	        // Could we try:
	        // return Array(count + 1).join(this);
	        return rpt;
	    };
	}

	if (typeof Object.assign != 'function') {
	    (function () {
	        Object.assign = function (target) {
	            'use strict';
	            // We must check against these specific cases.

	            if (target === undefined || target === null) {
	                throw new TypeError('Cannot convert undefined or null to object');
	            }

	            var output = Object(target);
	            for (var index = 1; index < arguments.length; index++) {
	                var source = arguments[index];
	                if (source !== undefined && source !== null) {
	                    for (var nextKey in source) {
	                        if (source.hasOwnProperty(nextKey)) {
	                            output[nextKey] = source[nextKey];
	                        }
	                    }
	                }
	            }
	            return output;
	        };
	    })();
	}

	Array.prototype.contains = function (obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	};

/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.Validation = exports.ValidationUI = exports.GroupedValidationValidators = exports.GroupedValidationUI = exports.ValidationValidators = exports.ValidationLang = exports.ValidationConfig = undefined;

	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

	var _file = __webpack_require__(215);

	var _image = __webpack_require__(216);

	var _bunny = __webpack_require__(217);

	var _BunnyElement = __webpack_require__(218);

	var _functions = __webpack_require__(213);

	Array.prototype.unique = function () {
	    var a = this.concat();
	    for (var i = 0; i < a.length; ++i) {
	        for (var j = i + 1; j < a.length; ++j) {
	            if (a[i] === a[j]) a.splice(j--, 1);
	        }
	    }

	    return a;
	};

	var ValidationConfig = exports.ValidationConfig = {

	    // div/node class name selector which contains one label, one input, one help text etc.
	    classInputGroup: 'data-input-module',
	    // class to be applied on input group node if it has invalid input
	    classInputGroupError: '---with-error',
	    // class to be applied on input group node if it input passed validation (is valid)
	    classInputGroupSuccess: '---with-success',

	    // label to pick textContent from to insert field name into error message
	    classLabel: '--label',

	    // error message tag name
	    tagNameError: 'div',
	    // error message class
	    classError: '--error',
	    classErrorText: '--error-text',
	    classErrorIcon: '--error-icon',

	    //split input error message class
	    splitInputModule: 'split-input-error',

	    // query selector to search inputs within input groups to validate
	    selectorInput: '[name]'

	};

	/**
	 * Bunny Form Validation default Translations (EN)
	 *
	 * object key = validator method name
	 * may use additional parameters in rejected (invalid) Promise
	 * each invalid input will receive {label} parameter anyway
	 * ajax error message should be received from server via JSON response in "message" key
	 */
	var ValidationLang = exports.ValidationLang = {

	    required: window.validationTranslations.required || "'{label}' is required",
	    email: window.validationTranslations.email || "'{label}' should be a valid e-mail address",
	    url: window.validationTranslations.url || "{label} should be a valid website URL",
	    tel: window.validationTranslations.tel || "'{label}' is not a valid telephone number",
	    maxLength: window.validationTranslations.maxLength || "'{label}' length must be < '{maxLength}'",
	    minLength: window.validationTranslations.minLength || "'{label}' length must be > '{minLength}'",
	    maxFileSize: window.validationTranslations.maxFileSize || "Max file size must be < {maxFileSize}MB, uploaded {fileSize}MB",
	    image: window.validationTranslations.image || "'{label}' should be an image (JPG or PNG)",
	    minImageDimensions: window.validationTranslations.minImageDimensions || "'{label}' must be > {minWidth}x{minHeight}, uploaded {width}x{height}",
	    maxImageDimensions: window.validationTranslations.maxImageDimensions || "'{label}' must be < {maxWidth}x{maxHeight}, uploaded {width}x{height}",
	    requiredFromList: window.validationTranslations.requiredFromList || "Select '{label}' from list",
	    confirmation: window.validationTranslations.confirmation || "'{label}' is not equal to '{originalLabel}'",
	    minOptions: window.validationTranslations.minOptions || "Please select at least {minOptionsCount} options",
	    minValue: window.validationTranslations.minValue || "not enough",
	    maxValue: window.validationTranslations.maxValue || "too much",
	    companyCode: window.validationTranslations.companyCode || "wrong company code",
	    personsName: window.validationTranslations.personsName || "bad name",
	    documentNumber: window.validationTranslations.documentNumber || 'bad document number',
	    documentDates: window.validationTranslations.documentDates || '{message}',
	    identificationCode: window.validationTranslations.identificationCode || 'bad code',
	    vatCode: window.validationTranslations.vatCode || 'bad vat code',
	    password: window.validationTranslations.password || 'bad password',
	    uniqueNumberNotUnique: window.validationTranslations.uniqueNumberNotUnique || 'not unique',
	    iban: window.validationTranslations.iban || 'wrong iban number'
	};

	/**
	 * Bunny Validation helper - get file to validate
	 * @param {HTMLInputElement} input
	 * @returns {File|Blob|boolean} - If no file uploaded - returns false
	 * @private
	 */
	var _bn_getFile = function _bn_getFile(input) {
	    // if there is custom file upload logic, for example, images are resized client-side
	    // generated Blobs should be assigned to fileInput._file
	    // and can be sent via ajax with FormData

	    // if file was deleted, custom field can be set to an empty string

	    // Bunny Validation detects if there is custom Blob assigned to file input
	    // and uses this file for validation instead of original read-only input.files[]
	    if (input._file !== undefined && input._file !== '') {
	        if (input._file instanceof Blob === false) {
	            console.error("Custom file for input " + input.name + " is not an instance of Blob");
	            return false;
	        }
	        return input._file;
	    }
	    return input.files[0] || false;
	};

	/**
	 * Bunny Form Validation Validators
	 *
	 * Each validator is a separate method
	 * Each validator return Promise
	 * Each Promise has valid and invalid callbacks
	 * Invalid callback may contain argument - string of error message or object of additional params for lang error message
	 */
	var ValidationValidators = exports.ValidationValidators = {
	    required: function required(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('required')) {
	                // input is required, check value
	                if (input.getAttribute('type') !== 'file' && input.value === '' || (input.type === 'radio' || input.type === 'checkbox') && !input.checked || input.getAttribute('type') === 'file' && _bn_getFile(input) === false) {
	                    // input is empty or file is not uploaded
	                    invalid();
	                } else {
	                    valid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    email: function email(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.value.length > 0 && input.getAttribute('type') === 'email') {
	                // input is email, parse string to match email regexp
	                var Regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
	                if (Regex.test(input.value)) {
	                    valid();
	                } else {
	                    invalid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    url: function url(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.value.length > 0 && input.getAttribute('type') === 'url') {
	                // input is URL, parse string to match website URL regexp
	                var Regex = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi;
	                if (Regex.test(input.value)) {
	                    valid();
	                } else {
	                    invalid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    tel: function tel(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.value.length > 0 && input.getAttribute('type') === 'tel') {
	                // input is tel, parse string to match tel regexp
	                var Regex = /^[0-9\-\+\(\)\#\ \*]{6,20}$/;
	                if (Regex.test(input.value)) {
	                    valid();
	                } else {
	                    invalid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    maxLength: function maxLength(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.getAttribute('maxlength') !== null && input.value.length > input.getAttribute('maxlength')) {
	                invalid({ maxLength: input.getAttribute('maxlength') });
	            } else {
	                valid();
	            }
	        });
	    },
	    minLength: function minLength(input) {
	        return new Promise(function (valid, invalid) {
	            if (
	            //rule
	            input.value.length < input.getAttribute('minlength')

	            /* when it should be applied*/
	            && input.getAttribute('minlength') !== null

	            /* when it should not be triggered */
	            && !(input.hasAttribute('nullable') && input.value.length === 0)) {
	                invalid({ minLength: input.getAttribute('minlength') });
	            } else {
	                valid();
	            }
	        });
	    },
	    maxFileSize: function maxFileSize(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.getAttribute('type') === 'file' && input.hasAttribute('maxfilesize') && _bn_getFile(input) !== false) {
	                var maxFileSize = parseFloat(input.getAttribute('maxfilesize')); // in MB
	                var fileSize = (_bn_getFile(input).size / 1000000).toFixed(2); // in MB
	                if (fileSize <= maxFileSize) {
	                    valid(input);
	                } else {
	                    invalid({ maxFileSize: maxFileSize, fileSize: fileSize });
	                }
	            } else {
	                valid(input);
	            }
	        });
	    },


	    // if file input has "accept" attribute and it contains "image",
	    // then check if uploaded file is a JPG or PNG
	    image: function image(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.getAttribute('type') === 'file' && input.getAttribute('accept').indexOf('image') > -1 && _bn_getFile(input) !== false) {
	                _file.BunnyFile.getSignature(_bn_getFile(input)).then(function (signature) {
	                    if (_file.BunnyFile.isJpeg(signature) || _file.BunnyFile.isPng(signature)) {
	                        valid();
	                    } else {
	                        invalid({ signature: signature });
	                    }
	                }).catch(function (e) {
	                    invalid(e);
	                });
	            } else {
	                valid();
	            }
	        });
	    },
	    minImageDimensions: function minImageDimensions(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('mindimensions') && _bn_getFile(input) !== false) {
	                var _input$getAttribute$s = input.getAttribute('mindimensions').split('x'),
	                    _input$getAttribute$s2 = _slicedToArray(_input$getAttribute$s, 2),
	                    minWidth = _input$getAttribute$s2[0],
	                    minHeight = _input$getAttribute$s2[1];

	                _image.BunnyImage.getImageByBlob(_bn_getFile(input)).then(function (img) {
	                    var width = _image.BunnyImage.getImageWidth(img);
	                    var height = _image.BunnyImage.getImageHeight(img);
	                    if (width < minWidth || height < minHeight) {
	                        invalid({ width: width, height: height, minWidth: minWidth, minHeight: minHeight });
	                    } else {
	                        valid();
	                    }
	                }).catch(function (e) {
	                    invalid(e);
	                });
	            } else {
	                valid();
	            }
	        });
	    },
	    maxImageDimensions: function maxImageDimensions(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('maxdimensions') && _bn_getFile(input) !== false) {
	                var _input$getAttribute$s3 = input.getAttribute('maxdimensions').split('x'),
	                    _input$getAttribute$s4 = _slicedToArray(_input$getAttribute$s3, 2),
	                    maxWidth = _input$getAttribute$s4[0],
	                    maxHeight = _input$getAttribute$s4[1];

	                _image.BunnyImage.getImageByBlob(_bn_getFile(input)).then(function (img) {
	                    var width = _image.BunnyImage.getImageWidth(img);
	                    var height = _image.BunnyImage.getImageHeight(img);
	                    if (width > maxWidth || height > maxHeight) {
	                        invalid({ width: width, height: height, maxWidth: maxWidth, maxHeight: maxHeight });
	                    } else {
	                        valid();
	                    }
	                }).catch(function (e) {
	                    invalid(e);
	                });
	            } else {
	                valid();
	            }
	        });
	    },
	    requiredFromList: function requiredFromList(input) {
	        return new Promise(function (valid, invalid) {
	            var id = void 0;
	            if (input.hasAttribute('requiredfromlist')) {
	                id = input.getAttribute('requiredfromlist');
	            } else {
	                id = input.name + '_id';
	            }
	            var srcInput = document.getElementById(id);
	            if (srcInput) {
	                if (srcInput.value.length > 0) {
	                    valid();
	                } else {
	                    invalid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    minOptions: function minOptions(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('minoptions')) {
	                var minOptionsCount = parseInt(input.getAttribute('minoptions'));
	                var inputGroup = ValidationUI.getInputGroup(input);
	                var hiddenInputs = inputGroup.getElementsByTagName('input');
	                var selectedOptionsCount = 0;
	                [].forEach.call(hiddenInputs, function (hiddenInput) {
	                    if (hiddenInput !== input && hiddenInput.value !== '') {
	                        selectedOptionsCount++;
	                    }
	                });
	                if (selectedOptionsCount < minOptionsCount) {
	                    invalid({ minOptionsCount: minOptionsCount });
	                } else {
	                    valid();
	                }
	            } else {
	                valid();
	            }
	        });
	    },
	    confirmation: function confirmation(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.name.indexOf('_confirmation') > -1) {
	                var originalInputId = input.name.substr(0, input.name.length - 13);
	                var originalInput = document.getElementById(originalInputId);
	                if (originalInput.value == input.value) {
	                    valid();
	                } else {
	                    invalid({ originalLabel: ValidationUI.getLabel(ValidationUI.getInputGroup(originalInput)).textContent });
	                }
	            } else {
	                valid();
	            }
	        });
	    },


	    // if input's value is not empty and input has attribute "data-ajax" which should contain ajax URL with {value}
	    // which will be replaced by URI encoded input.value
	    // then ajax request will be made to validate input
	    //
	    // ajax request should return JSON response
	    // if JSON response has "message" key and message key is not empty string - input is invalid
	    // server should return validation error message, it may contain {label}
	    // Does not works with file inputs
	    ajax: function ajax(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.dataset.ajax !== undefined && input.value.length > 0) {
	                var url = input.dataset.ajax.replace('{value}', encodeURIComponent(input.value));
	                _bunny.Ajax.get(url, function (data) {
	                    data = JSON.parse(data);
	                    if (data.message !== undefined && data.message !== '') {
	                        invalid(data.message);
	                    } else {
	                        valid();
	                    }
	                }, function () {
	                    invalid('Ajax error');
	                });
	            } else {
	                valid();
	            }
	        });
	    },
	    minValue: function minValue(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.getAttribute('minvalue') !== null && input.value - input.getAttribute('minvalue') < 0) {
	                invalid({ minValue: input.getAttribute('minvalue') });
	            } else {
	                valid();
	            }
	        });
	    },
	    maxValue: function maxValue(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.getAttribute('maxvalue') !== null && input.value - input.getAttribute('maxvalue') > 0) {
	                invalid({ maxValue: input.getAttribute('maxvalue') });
	            } else {
	                valid();
	            }
	        });
	    },
	    companyCode: function companyCode(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.value.length > 0 && input.value.length < 9 && input.hasAttribute('companycode') && !isNaN(input.value)) {
	                console.log(input, input.value.length > 0, input.value.length < 9, input.hasAttribute('companycode'), !isNaN(input.value));
	                invalid({ inputLenght: input.length });
	            } else {
	                valid();
	            }
	        });
	    },
	    personsName: function personsName(input) {
	        return new Promise(function (valid, invalid) {

	            if (input.hasAttribute('personsname') && !/^[a-z\-ąčęėįššųūž ]+$/ig.test(input.value)) {
	                invalid();
	            } else {
	                valid();
	            }
	        });
	    },
	    documentNumber: function documentNumber(input) {
	        return new Promise(function (valid, invalid) {

	            if (input.hasAttribute('documentnumber')) {
	                var typeInput = _functions.JS.selectFirst('#' + input.getAttribute('documentnumber'));

	                if (typeInput.value === 'passport') {
	                    if (!/^2[0-9]{7,}$/.test(input.value)) {
	                        invalid();
	                    }
	                } else if (typeInput.value === 'card') {
	                    if (!/^1[0-9]{7,}$/.test(input.value)) {
	                        invalid();
	                    }
	                }
	            }

	            valid();
	        });
	    },
	    documentDates: function documentDates(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('documentexpiredate') && input.hasAttribute('pair')) {
	                var releaseDateInput = _functions.JS.selectFirst('#' + input.getAttribute('pair'));

	                if (releaseDateInput) {
	                    var currentDate = new Date();
	                    var realeaseDate = new Date(releaseDateInput.value);
	                    var expireDate = new Date(input.value);

	                    if (ValidationUI.dateDiffInYears(realeaseDate, expireDate) !== 10) {
	                        invalid({ message: 'year difference is invalid' });
	                    }

	                    if (ValidationUI.dateDiffInDays(expireDate, currentDate) < 0) {
	                        invalid({ message: 'document is expired' });
	                    }

	                    if (ValidationUI.dateDiffInDays(expireDate, currentDate) < 14) {
	                        invalid({ message: 'document will expire to soon' });
	                    }
	                }
	            }

	            valid();
	        });
	    },
	    identificationCode: function identificationCode(input) {
	        return new Promise(function (valid, invalid) {

	            if (input.hasAttribute('identificationcode')) {
	                var code = input.value;

	                var firstDigit = [3, 4, 5, 6];
	                var date = new Date(code.substring(1, 3), code.substring(3, 5), code.substring(5, 7));

	                if (code.length === 11 && firstDigit.indexOf(+code[0]) !== -1 && date.toString().length > 12) {
	                    console.log('cia esu');
	                    var checksum = code[0] * 1 + code[1] * 2 + code[2] * 3 + code[3] * 4 + code[4] * 5 + code[5] * 6 + code[6] * 7 + code[7] * 8 + code[8] * 9 + code[9] * 1;

	                    checksum = checksum % 11;

	                    if (checksum === 10) {
	                        checksum = code[0] * 3 + code[1] * 4 + code[2] * 5 + code[3] * 6 + code[4] * 7 + code[5] * 8 + code[6] * 9 + code[7] * 1 + code[8] * 2 + code[9] * 3;

	                        checksum = checksum % 11;

	                        if (checksum === 10 && +code.substring(10, 11) === 0) {
	                            valid();
	                        } else if (checksum === +code.substring(10, 11)) {
	                            valid();
	                        }
	                    } else if (checksum === +code.substring(10, 11)) {
	                        valid();
	                    }
	                }
	            } else {
	                valid();
	            }

	            invalid();
	        });
	    },
	    vatCode: function vatCode(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('vatcode')) {
	                switch (input.value.length) {
	                    case 9:
	                        valid();
	                        break;
	                    case 12:
	                        valid();
	                        break;
	                    case 0:
	                        valid();
	                        break;
	                    default:
	                        invalid();
	                        break;
	                }
	            }

	            valid();
	        });
	    },
	    password: function password(input) {
	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('password') && (!/([A-Z])/g.test(input.value) || !/([A-z])/g.test(input.value) || !/([0-9])/g.test(input.value))) {
	                invalid();
	            } else {
	                valid();
	            }
	        });
	    },
	    iban: function iban(input) {
	        var table = [{ 'A': 10 }, { 'B': 11 }, { 'C': 12 }, { 'D': 13 }, { 'E': 14 }, { 'F': 15 }, { 'G': 16 }, { 'H': 17 }, { 'I': 18 }, { 'J': 19 }, { 'K': 20 }, { 'L': 21 }, { 'M': 22 }, { 'N': 23 }, { 'O': 24 }, { 'P': 25 }, { 'Q': 26 }, { 'R': 27 }, { 'S': 28 }, { 'T': 29 }, { 'U': 30 }, { 'V': 31 }, { 'W': 32 }, { 'X': 33 }, { 'Y': 34 }, { 'Z': 35 }];
	        var getValue = function getValue(letter) {
	            for (var i = 0; i < table.length; i++) {
	                if (table[i][letter]) {
	                    return table[i][letter];
	                }
	            }
	        };

	        var getNewNumber = function getNewNumber(_number, prefixNumber) {
	            return _number.substring(2) + prefixNumber + _number.substring(0, 2);
	        };

	        var mod97 = function mod97(string) {
	            var checksum = string.slice(0, 2),
	                fragment = void 0;
	            for (var offset = 2; offset < string.length; offset += 7) {
	                fragment = String(checksum) + string.substring(offset, offset + 7);
	                checksum = parseInt(fragment, 10) % 97;
	            }
	            return checksum;
	        };

	        return new Promise(function (valid, invalid) {
	            if (input.hasAttribute('iban') && input.value.length >= 18) {
	                var prefix = _functions.JS.selectFirst('.primary-input--prefix', input.parentNode).innerHTML;

	                var prefixInNumbers = '';
	                for (var i = 0; i < prefix.length; i++) {
	                    prefixInNumbers += getValue(prefix[i]);
	                }

	                if (mod97(getNewNumber(input.value, prefixInNumbers)) !== 1) {
	                    invalid();
	                }
	            }

	            valid();
	        });
	    }
	};

	var GroupedValidationUI = exports.GroupedValidationUI = {
	    getInputs: function getInputs(section) {
	        return _functions.JS.selectAll('input', section);
	    },
	    getInputsValues: function getInputsValues(inputs) {
	        var values = [];

	        for (var i = 0; i < inputs.length; i++) {
	            values.push(inputs[i].value);
	        }

	        return values;
	    },
	    getValidator: function getValidator(group) {
	        return group.getAttribute('data-grouped-validation');
	    },
	    arraysEqual: function arraysEqual(a, b) {
	        if (a === b) return true;
	        if (a == null || b == null) return false;
	        if (a.length != b.length) return false;

	        for (var i = 0; i < a.length; ++i) {
	            if (a[i] !== b[i]) return false;
	        }
	        return true;
	    }
	};

	var GroupedValidationValidators = exports.GroupedValidationValidators = {
	    uniqueInputs: function uniqueInputs(group) {
	        var allGroups = _functions.JS.selectAll('[data-grouped-validation]');

	        return new Promise(function (valid, invalid) {
	            if (GroupedValidationUI.getValidator(group) === 'unique-inputs') {
	                var inputs = GroupedValidationUI.getInputs(group);
	                var values = GroupedValidationUI.getInputsValues(inputs);

	                var faultyGroups = [];
	                for (var i = 0; i < allGroups.length; i++) {
	                    var otherGroup = allGroups[i];
	                    if (otherGroup !== group) {
	                        var otherValues = GroupedValidationUI.getInputsValues(GroupedValidationUI.getInputs(otherGroup));

	                        if (GroupedValidationUI.arraysEqual(values, otherValues)) {
	                            faultyGroups.push(otherGroup);
	                        }
	                    }
	                }

	                if (faultyGroups.length > 0) {
	                    invalid(faultyGroups);
	                } else {
	                    valid();
	                }
	            }
	        });
	    }
	};

	/**
	 * @package BunnyJS
	 * @component Validation
	 *
	 * Base Object to work with DOM, creates error messages
	 * and searches for inputs within "input groups" and related elements
	 * Each input should be wrapped around an "input group" element
	 * Each "input group" should contain one input, may contain one label
	 * Multiple inputs within same "Input group" should not be used for validation
	 * <fieldset> is recommended to be used to wrap more then one input
	 */
	var ValidationUI = exports.ValidationUI = {

	    config: ValidationConfig,

	    dateDiffInYears: function dateDiffInYears(dateold, datenew) {
	        var ynew = datenew.getFullYear();
	        var mnew = datenew.getMonth();
	        var dnew = datenew.getDate();
	        var yold = dateold.getFullYear();
	        var mold = dateold.getMonth();
	        var dold = dateold.getDate();
	        var diff = ynew - yold;
	        if (mold > mnew) diff--;else {
	            if (mold === mnew) {
	                if (dold > dnew) diff--;
	            }
	        }
	        return diff;
	    },
	    dateDiffInDays: function dateDiffInDays(dateold, datenew) {
	        var timeDiff = dateold.getTime() - datenew.getTime();

	        return Math.ceil(timeDiff / (1000 * 3600 * 24));
	    },


	    /* ************************************************************************
	     * ERROR MESSAGE
	     */

	    /**
	     * DOM algorithm - where to insert error node/message
	     *
	     * @param {HTMLElement} inputGroup
	     * @param {HTMLElement} errorNode
	     */
	    insertErrorNode: function insertErrorNode(inputGroup, errorNode) {
	        inputGroup.appendChild(errorNode);
	    },
	    insertSplitInputErrorNode: function insertSplitInputErrorNode(inputGroup, errorNode) {
	        inputGroup.appendChild(errorNode);
	    },


	    /**
	     * DOM algorithm - where to add/remove error class
	     *
	     * @param {HTMLElement} inputGroup
	     */
	    toggleErrorClass: function toggleErrorClass(inputGroup) {
	        var module = inputGroup.getAttribute(this.config.classInputGroup);
	        inputGroup.classList.toggle(module + this.config.classInputGroupError);
	    },
	    toggleSplitInputErrorClass: function toggleSplitInputErrorClass(inputGroup) {
	        var module = this.config.classInputGroup;
	        inputGroup.classList.toggle(module + this.config.classInputGroupError);
	    },


	    /**
	     * Create DOM element for error message
	     *
	     * @returns {HTMLElement}
	     */
	    createErrorNode: function createErrorNode(inputGroup, message) {
	        var module = inputGroup.getAttribute(this.config.classInputGroup);

	        var el = document.createElement(this.config.tagNameError);
	        el.classList.add(module + this.config.classError);

	        var errorTextEl = el.appendChild(document.createElement('div'));
	        errorTextEl.classList.add(module + this.config.classErrorText);
	        errorTextEl.textContent = message;

	        var errorIconEl = el.appendChild(document.createElement('div'));
	        errorIconEl.classList.add(module + this.config.classErrorIcon);

	        el.appendChild(errorTextEl);
	        el.appendChild(errorIconEl);

	        return el;
	    },
	    createSplitInputErrorNode: function createSplitInputErrorNode(message) {
	        var module = this.config.splitInputModule;

	        var el = document.createElement(this.config.tagNameError);
	        el.classList.add(module + this.config.classError);

	        var errorTextEl = el.appendChild(document.createElement('div'));
	        errorTextEl.classList.add(module + this.config.classErrorText);
	        errorTextEl.textContent = message;

	        var errorIconEl = el.appendChild(document.createElement('div'));
	        errorIconEl.classList.add(module + this.config.classErrorIcon);

	        el.appendChild(errorTextEl);
	        el.appendChild(errorIconEl);

	        return el;
	    },


	    /**
	     * Find error message node within input group or false if not found
	     *
	     * @param {HTMLElement} inputGroup
	     *
	     * @returns {HTMLElement|boolean}
	     */
	    getErrorNode: function getErrorNode(inputGroup) {
	        var module = inputGroup.getAttribute(this.config.classInputGroup);
	        return inputGroup.getElementsByClassName(module + this.config.classError)[0] || false;
	    },
	    getSplitInputErrorNode: function getSplitInputErrorNode(inputGroup) {
	        var module = this.config.splitInputModule;
	        return inputGroup.parentNode.getElementsByClassName(module + this.config.classError)[0] || false;
	    },
	    getErrorTextNode: function getErrorTextNode(inputGroup) {
	        var module = inputGroup.getAttribute(this.config.classInputGroup);
	        return inputGroup.getElementsByClassName(module + this.config.classErrorText)[0] || false;
	    },
	    getSplitInputErrorTextNode: function getSplitInputErrorTextNode(inputGroup) {
	        var module = this.config.splitInputModule;
	        return inputGroup.parentNode.getElementsByClassName(module + this.config.classErrorText)[0] || false;
	    },


	    /**
	     * Removes error node and class from input group if exists
	     *
	     * @param {HTMLElement} inputGroup
	     */
	    removeErrorNode: function removeErrorNode(inputGroup) {
	        var el = this.getErrorNode(inputGroup);
	        if (el) {
	            el.parentNode.removeChild(el);
	            this.toggleErrorClass(inputGroup);
	        }
	    },
	    removeSplitInputErrorNode: function removeSplitInputErrorNode(groupedValidationNode) {
	        var el = this.getSplitInputErrorNode(groupedValidationNode);
	        var module = this.config.splitInputModule;

	        if (el) {
	            el.parentNode.removeChild(el);
	            groupedValidationNode.classList.remove(module + this.config.classInputGroupError);
	        }
	    },


	    /**
	     * Removes all error node and class from input group if exists within section
	     *
	     * @param {HTMLElement} section
	     */
	    removeErrorNodesFromSection: function removeErrorNodesFromSection(section) {
	        var _this = this;

	        [].forEach.call(this.getInputGroupsInSection(section), function (inputGroup) {
	            _this.removeErrorNode(inputGroup);
	        });
	    },
	    removeSplitInputErrorNodesFromSection: function removeSplitInputErrorNodesFromSection(section) {
	        var _this2 = this;

	        [].forEach.call(_functions.JS.selectAll('[data-grouped-validation]', section), function (groupedValidationInputs) {
	            _this2.removeSplitInputErrorNode(groupedValidationInputs);
	        });
	    },
	    setSplitInputErrorMessage: function setSplitInputErrorMessage(inputGroup, message) {
	        var errorNode = this.getSplitInputErrorTextNode(inputGroup);
	        if (errorNode === false) {
	            // container for error message doesn't exists, create new
	            errorNode = this.createSplitInputErrorNode(message);
	            this.toggleSplitInputErrorClass(inputGroup);
	            this.insertSplitInputErrorNode(inputGroup, errorNode);
	        } else {
	            errorNode.textContent = message;
	        }
	    },


	    /**
	     * Creates and includes into DOM error node or updates error message
	     *
	     * @param {HTMLElement} inputGroup
	     * @param {String} message
	     */
	    setErrorMessage: function setErrorMessage(inputGroup, message) {
	        var errorNode = this.getErrorTextNode(inputGroup);
	        if (errorNode === false) {
	            // container for error message doesn't exists, create new
	            errorNode = this.createErrorNode(inputGroup, message);
	            this.toggleErrorClass(inputGroup);
	            this.insertErrorNode(inputGroup, errorNode);
	        } else {
	            errorNode.textContent = message;
	        }
	    },


	    /**
	     * Marks input as valid
	     *
	     * @param {HTMLElement} inputGroup
	     */
	    setInputValid: function setInputValid(inputGroup) {
	        var module = inputGroup.getAttribute(this.config.classInputGroup);
	        inputGroup.classList.add(module + this.config.classInputGroupSuccess);
	    },


	    /* ************************************************************************
	     * SEARCH DOM
	     */

	    /**
	     * DOM Algorithm - which inputs should be selected for validation
	     *
	     * @param {HTMLElement} inputGroup
	     *
	     * @returns {HTMLElement|boolean}
	     */
	    getInput: function getInput(inputGroup) {
	        return inputGroup.querySelector(this.config.selectorInput) || false;
	    },


	    /**
	     * Find closest parent inputGroup element by Input element
	     *
	     * @param {HTMLElement} input
	     *
	     * @returns {HTMLElement}
	     */
	    getInputGroup: function getInputGroup(input) {
	        var el = input;
	        while ((el = el.parentNode) && !el.getAttribute(this.config.classInputGroup)) {}
	        return el;
	    },


	    /**
	     * Find inputs in section
	     *
	     * @meta if second argument true - return object with meta information to use during promise resolving
	     *
	     * @param {HTMLElement} node
	     * @param {boolean} resolving = false
	     *
	     * @returns {Array|Object}
	     */
	    getInputsInSection: function getInputsInSection(node) {
	        var resolving = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	        var inputGroups = this.getInputGroupsInSection(node);
	        var inputs = void 0;
	        if (resolving) {
	            inputs = {
	                inputs: {},
	                invalidInputs: {},
	                length: 0,
	                unresolvedLength: 0,
	                invalidLength: 0
	            };
	        } else {
	            inputs = [];
	        }
	        for (var k = 0; k < inputGroups.length; k++) {
	            var input = this.getInput(inputGroups[k]);
	            if (input === false) {
	                console.error(inputGroups[k]);
	                throw new Error('Bunny Validation: Input group has no input');
	            }
	            if (resolving) {
	                inputs.inputs[k] = {
	                    input: input,
	                    isValid: null
	                };
	                inputs.length++;
	                inputs.unresolvedLength++;
	            } else {
	                inputs.push(input);
	            }
	        }
	        return inputs;
	    },


	    /**
	     * Find label associated with input within input group
	     *
	     * @param {HTMLElement} inputGroup
	     *
	     * @returns {HTMLElement|boolean}
	     */
	    getLabel: function getLabel(inputGroup) {
	        return inputGroup.getElementsByTagName('label')[0] || false;
	    },


	    /**
	     * Find all input groups within section
	     *
	     * @param {HTMLElement} node
	     *
	     * @returns {HTMLCollection}
	     */
	    getInputGroupsInSection: function getInputGroupsInSection(node) {
	        return node.querySelectorAll('[' + this.config.classInputGroup + ']');
	    },


	    /* First form step only */
	    removeErrorFromHiddenInputs: function removeErrorFromHiddenInputs(section) {
	        var groups = _functions.JS.selectAll('.property-content--animated-inputs', section);

	        for (var i = 0; i < groups.length; i++) {
	            if (!_functions.JS.hasClass(groups[i], 'property-content--animated-inputs---active')) {
	                ValidationUI.removeErrorNodesFromSection(groups[i]);
	            }
	        }
	    }
	};

	var Validation = exports.Validation = {

	    validators: ValidationValidators,
	    groupedValidators: GroupedValidationValidators,
	    lang: ValidationLang,
	    ui: ValidationUI,
	    reCaptcha: true,
	    minLoanValue: true,

	    init: function init(form) {
	        var _this3 = this;

	        var inline = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	        var self = this;

	        // disable browser built-in validation
	        form.setAttribute('novalidate', '');

	        form.addEventListener('submit', function (e) {
	            e.preventDefault();
	            _this3.validateMinLoanValue();

	            var submitBtns = form.querySelectorAll('[type="submit"]');
	            [].forEach.call(submitBtns, function (submitBtn) {
	                submitBtn.disabled = true;
	            });
	            _this3.validateSection(form).then(function (result) {
	                [].forEach.call(submitBtns, function (submitBtn) {
	                    submitBtn.disabled = false;
	                });
	                self.validateGrouped().then(function () {
	                    if (result === true && _this3.reCaptcha && _this3.minLoanValue) {
	                        form.submit();
	                    } else {
	                        _this3.focusInput(result[0]);
	                        ValidationUI.removeErrorFromHiddenInputs(form);
	                    }
	                }).catch(function (faultyGroups) {
	                    var filteredGroups = self.concatAndRemoveDublicate(faultyGroups);

	                    for (var i = filteredGroups.length - 1; i >= 0; i--) {
	                        if (i < filteredGroups.length - 1) {
	                            self.addErrorToGroup(filteredGroups[i]);
	                        }
	                    }
	                });
	            });
	        });

	        if (inline) {
	            this.initInline(form);
	        }
	    },
	    initInline: function initInline(node) {
	        var _this4 = this;

	        var inputs = this.ui.getInputsInSection(node);

	        var self = this;
	        var i = 0;
	        var interval = [];
	        var checkExpireDateInput = function checkExpireDateInput(input) {
	            var expireDateInput = _functions.JS.selectFirst('#' + input.getAttribute('pair'));

	            if (expireDateInput && expireDateInput.value !== '') {
	                self.checkInput(expireDateInput).catch(function (e) {});
	            }
	        };

	        inputs.forEach(function (input) {
	            i++;

	            input.addEventListener('change', function () {
	                if (input.type !== 'checkbox') {
	                    clearTimeout(interval[i]);
	                    _this4.checkInput(input).catch(function (e) {});
	                }
	            });
	            input.addEventListener('keyup', function (e) {
	                var errorNode = ValidationUI.getErrorTextNode(ValidationUI.getInputGroup(input));

	                if (errorNode) {
	                    _this4.checkInput(input).catch(function (e) {});
	                }
	            });
	            input.addEventListener('blur', function (e) {
	                var errorNode = ValidationUI.getErrorTextNode(ValidationUI.getInputGroup(input));

	                if (errorNode) {
	                    _this4.checkInput(input).catch(function (e) {});
	                }

	                if (input.hasAttribute('documentreleasedate')) {
	                    checkExpireDateInput(input);
	                }
	            });

	            input.addEventListener('input', function (e) {
	                if (input.getAttribute('maxlength') !== null && input.value.length > input.getAttribute('maxlength')) {
	                    input.value = input.value.slice(0, input.getAttribute('maxlength'));
	                }
	            });
	        });
	    },
	    concatAndRemoveDublicate: function concatAndRemoveDublicate(array) {
	        var newArray = [];

	        for (var i = 0; i < array.length; i++) {
	            newArray = newArray.concat(array[i]).unique();
	        }

	        return newArray;
	    },
	    addErrorToCaptcha: function addErrorToCaptcha() {
	        var captchaEl = _functions.JS.selectFirst('#g-recaptcha-error');

	        _functions.JS.removeClass(captchaEl, 'primary-input--error---hide');
	    },
	    addErrorToGroup: function addErrorToGroup(group) {
	        ValidationUI.setSplitInputErrorMessage(group.parentNode, ValidationLang.uniqueNumberNotUnique);
	    },
	    removeErrorFromCaptcha: function removeErrorFromCaptcha() {
	        var captchaEl = _functions.JS.selectFirst('#g-recaptcha-error');

	        _functions.JS.addClass(captchaEl, 'primary-input--error---hide');
	    },
	    validateGrouped: function validateGrouped() {
	        var _this5 = this;

	        return new Promise(function (resolve, reject) {
	            var groups = _functions.JS.selectAll('[data-grouped-validation]');

	            if (groups.length < 2) resolve();

	            var faultyGroups = [];
	            var finished = 1;

	            var _loop = function _loop(i) {
	                var validators = Object.keys(_this5.groupedValidators);

	                for (var j = 0; j < validators.length; j++) {
	                    var currentValidatorName = validators[j];
	                    var currentValidator = _this5.groupedValidators[currentValidatorName];

	                    currentValidator(groups[i]).then(function () {
	                        finished++;
	                        if (finished === groups.length) {
	                            if (faultyGroups.length > 0) {
	                                reject(faultyGroups);
	                            } else {
	                                ValidationUI.removeSplitInputErrorNode(groups[i]);
	                                resolve();
	                            }
	                        }
	                    }).catch(function (reason) {
	                        finished++;
	                        faultyGroups.push(reason);
	                        if (finished === groups.length) {
	                            reject(faultyGroups);
	                        }
	                    });
	                }
	            };

	            for (var i = 0; i < groups.length; i++) {
	                _loop(i);
	            }
	        });
	    },
	    validateMinLoanValue: function validateMinLoanValue() {
	        var self = this;
	        var maxAmount = _functions.JS.selectFirst('.alternative-panel--amount-value[calculated]');
	        var offerToSmall = function offerToSmall() {
	            self.minLoanValue = false;

	            window.flash.new('error', 'flash-info', window.flashTranslations.errorOfferNotEnough);
	        };

	        if (maxAmount) {
	            maxAmount = maxAmount.innerHTML.replace('€', '').replace(/\s+/g, '').replace(/[A-z]+/g, '');
	            if (!isNaN(maxAmount)) {
	                if (window.minLoanAmount) {
	                    if (window.minLoanAmount > +maxAmount) {
	                        offerToSmall();
	                    } else {
	                        this.minLoanValue = true;
	                    }
	                }
	            } else {
	                offerToSmall();
	            }
	        }
	    },
	    validateSection: function validateSection(node) {
	        var _this6 = this;

	        var self = this;

	        if (node.querySelector('.g-recaptcha')) {
	            if (grecaptcha.getResponse() === '') {
	                this.reCaptcha = false;
	                self.addErrorToCaptcha();
	            } else {
	                this.reCaptcha = true;
	                self.removeErrorFromCaptcha();
	            }
	        }

	        if (node.__bunny_validation_state === undefined) {
	            node.__bunny_validation_state = true;
	        } else {
	            throw new Error('Bunny Validation: validation already in progress.');
	        }
	        return new Promise(function (resolve) {
	            var resolvingInputs = _this6.ui.getInputsInSection(node, true);
	            if (resolvingInputs.length === 0) {
	                // nothing to validate, end
	                _this6._endSectionValidation(node, resolvingInputs, resolve);
	            } else {
	                // run async validation for each input
	                // when last async validation will be completed, call validSection or invalidSection
	                var promises = [];

	                var _loop2 = function _loop2(i) {
	                    var input = resolvingInputs.inputs[i].input;

	                    _this6.checkInput(input).then(function () {
	                        _this6._addValidInput(resolvingInputs, input);
	                        if (resolvingInputs.unresolvedLength === 0) {
	                            _this6._endSectionValidation(node, resolvingInputs, resolve);
	                        }
	                    }).catch(function (errorMessage) {
	                        _this6._addInvalidInput(resolvingInputs, input);
	                        if (resolvingInputs.unresolvedLength === 0) {
	                            _this6._endSectionValidation(node, resolvingInputs, resolve);
	                        }
	                    });
	                };

	                for (var i = 0; i < resolvingInputs.length; i++) {
	                    _loop2(i);
	                }

	                // if there are not resolved promises after 3s, terminate validation, mark pending inputs as invalid
	                setTimeout(function () {
	                    if (resolvingInputs.unresolvedLength > 0) {
	                        var unresolvedInputs = _this6._getUnresolvedInputs(resolvingInputs);
	                        for (var i = 0; i < unresolvedInputs.length; i++) {
	                            var _input = unresolvedInputs[i];
	                            var inputGroup = _this6.ui.getInputGroup(_input);
	                            _this6._addInvalidInput(resolvingInputs, _input);
	                            _this6.ui.setErrorMessage(inputGroup, 'Validation terminated after 3s');
	                            if (resolvingInputs.unresolvedLength === 0) {
	                                _this6._endSectionValidation(node, resolvingInputs, resolve);
	                            }
	                        }
	                    }
	                }, 3000);
	            }
	        });
	    },
	    focusInput: function focusInput(input) {
	        var delay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
	        var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : -50;
	        var scroll = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

	        if (input) {
	            if (scroll) {
	                _BunnyElement.BunnyElement.scrollTo(input, delay, offset);
	            }
	            input.focus();

	            if (input.offsetParent !== null && input.setSelectionRange !== undefined && ['text', 'search', 'url', 'tel', 'password'].indexOf(input.type) !== -1 && typeof input.setSelectionRange === 'function') {
	                input.setSelectionRange(input.value.length, input.value.length);
	            }
	        }
	    },
	    checkInput: function checkInput(input) {
	        var _this7 = this;

	        return new Promise(function (valid, invalid) {
	            _this7._checkInput(input, 0, valid, invalid);
	        });
	    },
	    _addValidInput: function _addValidInput(resolvingInputs, input) {
	        resolvingInputs.unresolvedLength--;
	        for (var k in resolvingInputs.inputs) {
	            if (input === resolvingInputs.inputs[k].input) {
	                resolvingInputs.inputs[k].isValid = true;
	                break;
	            }
	        }
	    },
	    _addInvalidInput: function _addInvalidInput(resolvingInputs, input) {
	        resolvingInputs.unresolvedLength--;
	        resolvingInputs.invalidLength++;
	        for (var k in resolvingInputs.inputs) {
	            if (input === resolvingInputs.inputs[k].input) {
	                resolvingInputs.inputs[k].isValid = false;
	                resolvingInputs.invalidInputs[k] = input;
	                break;
	            }
	        }
	    },
	    _getUnresolvedInputs: function _getUnresolvedInputs(resolvingInputs) {
	        var unresolvedInputs = [];
	        for (var k in resolvingInputs.inputs) {
	            if (!resolvingInputs.inputs[k].isValid) {
	                unresolvedInputs.push(resolvingInputs.inputs[k].input);
	            }
	        }
	        return unresolvedInputs;
	    },
	    _endSectionValidation: function _endSectionValidation(node, resolvingInputs, resolve) {
	        delete node.__bunny_validation_state;

	        if (resolvingInputs.invalidLength === 0) {
	            // form or section is valid
	            return resolve(true);
	        } else {
	            var invalidInputs = [];
	            for (var k in resolvingInputs.invalidInputs) {
	                invalidInputs.push(resolvingInputs.invalidInputs[k]);
	            }
	            // form or section has invalid inputs
	            return resolve(invalidInputs);
	        }
	    },
	    _checkInput: function _checkInput(input, index, valid, invalid) {
	        var _this8 = this;

	        var validators = Object.keys(this.validators);
	        var currentValidatorName = validators[index];
	        var currentValidator = this.validators[currentValidatorName];

	        currentValidator(input).then(function () {
	            index++;
	            if (validators[index] !== undefined) {
	                _this8._checkInput(input, index, valid, invalid);
	            } else {
	                var inputGroup = _this8.ui.getInputGroup(input);
	                // if has error message, remove it
	                _this8.ui.removeErrorNode(inputGroup);

	                if (input.form !== undefined && input.form.hasAttribute('showvalid')) {
	                    // mark input as valid
	                    _this8.ui.setInputValid(inputGroup);
	                }

	                valid();
	            }
	        }).catch(function (data) {
	            // get input group and label
	            var inputGroup = _this8.ui.getInputGroup(input);
	            var label = _this8.ui.getLabel(inputGroup);

	            // get error message
	            var errorMessage = _this8._getErrorMessage(currentValidatorName, input, label, data);

	            // set error message
	            _this8.ui.setErrorMessage(inputGroup, errorMessage);
	            invalid(errorMessage);
	        });
	    },
	    _getErrorMessage: function _getErrorMessage(validatorName, input, label, data) {
	        var message = '';
	        if (typeof data === 'string') {
	            // if validator returned string (from ajax for example), use it
	            message = data;
	        } else {
	            if (this.lang[validatorName] === undefined) {
	                throw new Error('Bunny Validation: Lang message not found for validator: ' + validatorName);
	            }
	            message = this.lang[validatorName];
	        }

	        // replace params in error message
	        if (label !== false) {
	            message = message.replace('{label}', label.textContent);
	        } else if (input.placeholder && input.placeholder !== '') {
	            message = message.replace('{label}', input.placeholder);
	        } else {
	            message = message.replace('{label}', '');
	        }

	        for (var paramName in data) {
	            message = message.replace('{' + paramName + '}', data[paramName]);
	        }
	        return message;
	    }
	};

	document.addEventListener('DOMContentLoaded', function () {
	    [].forEach.call(document.forms, function (form) {
	        if (form.getAttribute('validator') === 'bunny') {
	            var inline = form.hasAttribute('validator-inline');
	            Validation.init(form, inline);
	        }
	    });
	});

/***/ }),

/***/ 215:
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var BunnyFile = exports.BunnyFile = {

	    /**
	     * Download file from URL via AJAX and make Blob object or return base64 string if 2nd argument is false
	     * Only files from CORS-enabled domains can be downloaded or AJAX will get security error
	     *
	     * @param {String} URL
	     * @param {Boolean} convert_to_blob = true
	     * @returns {Promise}: success(Blob object | base64 string), fail(response XHR object)
	     */
	    download: function download(URL) {
	        var convert_to_blob = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	        var request = new XMLHttpRequest();
	        var p = new Promise(function (success, fail) {
	            request.onload = function () {
	                if (request.status === 200) {
	                    var blob = request.response;
	                    success(blob);
	                } else {
	                    fail(request);
	                }
	            };
	        });

	        request.open('GET', URL, true);
	        if (convert_to_blob) {
	            request.responseType = 'blob';
	        }
	        request.send();

	        return p;
	    },


	    /**
	     * Get File/Blob header (signature) to parse for MIME-type or any magic numbers
	     * @param {File|Blob} blob
	     * @returns {Promise} callback(str:signature)
	     */
	    getSignature: function getSignature(blob) {
	        return new Promise(function (callback) {
	            var reader = new FileReader();
	            reader.onloadend = function () {
	                var arr = new Uint8Array(reader.result).subarray(0, 4);
	                var signature = '';
	                for (var i = 0; i < arr.length; i++) {
	                    signature += arr[i].toString(16);
	                }
	                callback(signature);
	            };
	            reader.readAsArrayBuffer(blob);
	        });
	    },


	    /**
	     * Check if string is a valid signature for image/jpeg
	     * @param {String} signature
	     * @returns {boolean}
	     */
	    isJpeg: function isJpeg(signature) {
	        var signatures = ['ffd8ffe0', 'ffd8ffe1', 'ffd8ffe2'];
	        return signatures.indexOf(signature) > -1;
	    },


	    /**
	     * Check if string is a valid signature for image/png
	     * @param {String} signature
	     * @returns {boolean}
	     */
	    isPng: function isPng(signature) {
	        return signature === '89504e47';
	    },


	    /**
	     * Convert base64 string to Blob object
	     * @param {String} base64
	     * @returns {Blob}
	     */
	    base64ToBlob: function base64ToBlob(base64) {
	        // convert base64 to raw binary data held in a string
	        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
	        var byteString = atob(base64.split(',')[1]);

	        // separate out the mime component
	        var mimeString = base64.split(',')[0].split(':')[1].split(';')[0];

	        // write the bytes of the string to an ArrayBuffer
	        var ab = new ArrayBuffer(byteString.length);
	        var ia = new Uint8Array(ab);
	        for (var i = 0; i < byteString.length; i++) {
	            ia[i] = byteString.charCodeAt(i);
	        }

	        // write the ArrayBuffer to a blob, and you're done
	        return new Blob([ab], { type: mimeString });
	    },


	    /**
	     * Convert Blob object to base64string
	     * @param {Blob} blob
	     * @returns {Promise} success(base64 string), fail(error)
	     */
	    blobToBase64: function blobToBase64(blob) {
	        var reader = new FileReader();
	        var p = new Promise(function (success, fail) {
	            reader.onloadend = function () {
	                var base64 = reader.result;
	                success(base64);
	            };
	            reader.onerror = function (e) {
	                fail(e);
	            };
	        });

	        reader.readAsDataURL(blob);

	        return p;
	    },


	    /**
	     * Get local browser object URL which can be used in img.src for example
	     * @param {Blob} blob
	     * @returns {String}
	     */
	    getBlobLocalURL: function getBlobLocalURL(blob) {
	        if (!(blob instanceof Blob || blob instanceof File)) {
	            throw new TypeError('Argument in BunnyFile.getBlobLocalURL() is not a Blob or File object');
	        }
	        return URL.createObjectURL(blob);
	    }
	};

/***/ }),

/***/ 216:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.BunnyImage = undefined;

	var _file = __webpack_require__(215);

	/**
	 * @component BunnyImage
	 * Wrapper for Image object representing <img> tag, uses Canvas and BunnyFile
	 *
	 */
	var BunnyImage = exports.BunnyImage = {

	  IMG_CONVERT_TYPE: 'image/jpeg',
	  IMG_QUALITY: 0.7,

	  // SECTION: get Image object via different sources

	  /**
	   * Downloads image by any URL or converts from Blob, should work also for non-CORS domains
	   *
	   * @param {String|Blob} urlOrBlob
	   * @returns {Promise} success(Image object), fail(error)
	   */
	  getImage: function getImage(urlOrBlob) {
	    if (typeof urlOrBlob === 'string') {
	      return this.getImageByURL(urlOrBlob);
	    } else {
	      return this.getImageByBlob(urlOrBlob);
	    }
	  },


	  /**
	   * Downloads image by any URL, should work also for non-CORS domains
	   *
	   * @param {String} URL
	   * @returns {Promise} success(Image object), fail(error)
	   */
	  getImageByURL: function getImageByURL(URL) {
	    return this._toImagePromise(URL, true);
	  },
	  _toImagePromise: function _toImagePromise(src) {
	    var crossOrigin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

	    var img = new Image();
	    var p = new Promise(function (ok, fail) {
	      img.onload = function () {
	        ok(img);
	      };
	      img.onerror = function (e) {
	        fail(e);
	      };
	    });
	    if (crossOrigin) {
	      img.crossOrigin = 'Anonymous';
	    }
	    img.src = src;
	    return p;
	  },
	  getImageByBlob: function getImageByBlob(blob) {
	    var url = _file.BunnyFile.getBlobLocalURL(blob);
	    return this._toImagePromise(url);
	  },
	  getImageByBase64: function getImageByBase64(base64) {
	    var url = base64;
	    return this._toImagePromise(url);
	  },
	  getImageByCanvas: function getImageByCanvas(canvas) {
	    var url = canvas.toDataURL(this.IMG_CONVERT_TYPE, this.IMG_QUALITY);
	    return this._toImagePromise(url);
	  },


	  // SECTION:: create different sources from Image object

	  imageToCanvas: function imageToCanvas(img) {
	    var width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	    var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	    if (!img.complete) {
	      throw new Error('Can not create canvas from Image. Image is not loaded yet.');
	    }
	    var canvas = document.createElement("canvas");
	    if (width === null && height === null) {
	      canvas.width = img.naturalWidth;
	      canvas.height = img.naturalHeight;
	      canvas.getContext("2d").drawImage(img, 0, 0);
	    } else {
	      canvas.width = width;
	      canvas.height = height;
	      canvas.getContext("2d").drawImage(img, 0, 0, width, height);
	    }
	    return canvas;
	  },


	  /**
	   *
	   * @param {Image|HTMLImageElement} img
	   * @param {Number?} width
	   * @param {Number?} height
	   * @returns {string}
	   */
	  imageToBase64: function imageToBase64(img) {
	    var width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	    var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	    return this.imageToCanvas(img, width, height).toDataURL(this.IMG_CONVERT_TYPE, this.IMG_QUALITY);
	  },


	  /**
	   *
	   * @param {Image|HTMLImageElement} img
	   * @param {Number?} width
	   * @param {Number?} height
	   * @returns {Blob}
	   */
	  imageToBlob: function imageToBlob(img) {
	    var width = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	    var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	    return _file.BunnyFile.base64ToBlob(this.imageToBase64(img, width, height));
	  },


	  // SECTION: basic Image statistics and info functions

	  getImageURL: function getImageURL(img) {
	    return img.src;
	  },
	  getImageWidth: function getImageWidth(img) {
	    if (!img.complete) {
	      throw new Error('Can not get Image.width. Image is not loaded yet.');
	    }
	    return img.width;
	  },
	  getImageHeight: function getImageHeight(img) {
	    if (!img.complete) {
	      throw new Error('Can not get Image.height. Image is not loaded yet.');
	    }
	    return img.height;
	  },


	  // SECTION: basic Image data math functions

	  getImageNewAspectSizes: function getImageNewAspectSizes(img, max_width, max_height) {
	    var img_width = this.getImageWidth(img);
	    var img_height = this.getImageHeight(img);
	    if (img_width === 0 || img_height === 0) {
	      throw new Error('Image width or height is 0 in BunnyImage.getImageNewAspectSizes().');
	    }
	    var ratio = Math.min(max_width / img_width, max_height / img_height);

	    return {
	      width: Math.floor(img_width * ratio),
	      height: Math.floor(img_height * ratio)
	    };
	  },


	  // SECTION: basic Image manipulation
	  // returns canvas

	  /**
	   * Resize image
	   * @param {Image} img
	   * @param {Number} max_width
	   * @param {Number} max_height
	   * @returns {Promise} success(Image), fail(error)
	   */
	  resizeImage: function resizeImage(img, max_width, max_height) {
	    var sizes = this.getImageNewAspectSizes(img, max_width, max_height);
	    var width = sizes.width;
	    var height = sizes.height;
	    var canvas = this.imageToCanvas(img, width, height);
	    return canvas;
	    //return this.getImageByCanvas(canvas);
	  },
	  resizeCanvas: function resizeCanvas(canvas, width) {
	    var height = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

	    if (height === null) height = width;
	    var tmpCanvas = document.createElement('canvas');
	    var tmpCtx = tmpCanvas.getContext('2d');
	    tmpCanvas.width = width;
	    tmpCanvas.height = height;
	    tmpCtx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, width, height);
	    return tmpCanvas;
	  },
	  crop: function crop(img, x, y, width) {
	    var height = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

	    if (height === null) height = width;
	    var proportion = img.naturalWidth / img.clientWidth;
	    var canvas = document.createElement('canvas');
	    var sizeX = width * proportion;
	    var sizeY = height * proportion;
	    canvas.width = sizeX;
	    canvas.height = sizeY;
	    var ctx = canvas.getContext('2d');
	    ctx.drawImage(img, x * proportion, y * proportion, sizeX, sizeY, 0, 0, sizeX, sizeY);
	    return canvas;
	  },
	  cropByCursor: function cropByCursor(img, cursor) {
	    return this.crop(img, cursor.offsetLeft, cursor.offsetTop, cursor.clientWidth, cursor.clientHeight);
	  }
	};

/***/ }),

/***/ 217:
/***/ (function(module, exports) {

	'use strict';

	/**
	 * Base object Ajax
	 */

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var Ajax = exports.Ajax = {

	    /**
	     * Sends an async HTTP (AJAX) request or if last parameter is false - returns created instance
	     * with ability to modify native XMLHttpRequest (.request property) and manually send request when needed.
	     *
	     * @param {string} method - HTTP method (GET, POST, HEAD, ...)
	     * @param {string} url - URI for current domain or full URL for cross domain AJAX request
	     *        Please note that in cross domain requests only GET, POST and HEAD methods allowed as well as
	     *        only few headers available. For more info visit
	     *        https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
	     * @param {object} data - key: value pair of data to send. Data is automatically URL encoded
	     * @param {callback(responseText)} on_success - callback on response with status code 200
	     * @param {callback(responseText, responseStatusCode)} on_error = null - custom handler
	     *        for response with status code different from 200
	     * @param {object} headers = {} - key: value map of headers to send
	     * @param {boolean} do_send = true - instantly makes requests
	     *
	     * @returns {Object}
	     */
	    create: function create(method, url, data, on_success) {
	        var on_error = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
	        var headers = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};
	        var do_send = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : true;


	        var t = Object.create(this);
	        t.method = method;
	        t.url = url;
	        t.data = data;
	        t.request = new XMLHttpRequest();
	        t.onSuccess = on_success;
	        t.onError = on_error;
	        t.headers = headers;
	        t.request.onreadystatechange = function () {
	            if (t.request.readyState === XMLHttpRequest.DONE) {
	                if (t.request.status === 200) {
	                    t.onSuccess(t.request.responseText);
	                } else {
	                    if (t.onError !== null) {
	                        t.onError(t.request.responseText, t.request.status);
	                    } else {
	                        console.error('Bunny AJAX error: unhandled error with response status ' + t.request.status + ' and body: ' + t.request.responseText);
	                    }
	                }
	            }
	        };

	        if (do_send) {
	            t.send();
	        }

	        return t;
	    },


	    /**
	     * Should be called on instance created with factory Ajax.create() method
	     * Opens request, applies headers, builds data URL encoded string and sends request
	     */
	    send: function send() {

	        this.request.open(this.method, this.url);

	        for (var header in this.headers) {
	            this.request.setRequestHeader(header, this.headers[header]);
	        }

	        var str_data = '';

	        if (this.data instanceof FormData) {
	            this.request.send(this.data);
	        } else {
	            for (var name in this.data) {
	                str_data = str_data + name + '=' + encodeURIComponent(this.data[name]) + '&';
	            }
	            this.request.send(str_data);
	        }
	    },


	    /**
	     * Sends a form via ajax POST with header Content-Type: application/x-www-form-urlencoded
	     * Data is automatically taken form all form input values
	     *
	     * @param {object} form_el - Form document element
	     * @param {callback(responseText)} on_success - callback for status code 200
	     * @param {callback(responseText, responseStatusCode)} on_error = null - custom handler for non 200 status codes
	     * @param {object} headers = {'Content-Type': 'application/x-www-form-urlencoded'} - key: value map of headers
	     */
	    sendForm: function sendForm(form_el, on_success) {
	        var on_error = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	        var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { 'Content-Type': 'application/x-www-form-urlencoded' };

	        var data = {};
	        form_el.querySelectorAll('[name]').forEach(function (input) {
	            data[input.getAttribute('name')] = input.value;
	        });
	        this.create('POST', form_el.getAttribute('action'), data, on_success, on_error, headers, true);
	    },


	    /**
	     * Sends a form via ajax POST with header Content-Type: multipart/form-data which is required for file uploading
	     * Data is automatically taken form all form input values
	     *
	     * @param {object} form_el - Form document element
	     * @param {callback(responseText)} on_success - callback for status code 200
	     * @param {callback(responseText, responseStatusCode)} on_error = null - custom handler for non 200 status codes
	     * @param {object} headers = {'Content-Type': 'multipart/form-data'} - key: value map of headers
	     */
	    sendFormWithFiles: function sendFormWithFiles(form_el, on_success) {
	        var on_error = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	        var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { 'Content-Type': 'multipart/form-data' };

	        this.sendForm(form_el, on_success, on_error, headers);
	    },


	    /**
	     * Sends a simple GET request. By default adds header X-Requested-With: XMLHttpRequest
	     * which allows back-end applications to detect if request is ajax.
	     * However for making a cross domain requests this header might not be acceptable
	     * and in this case pass an empty object {} as a last argument to send no headers
	     *
	     * @param {string} url - URI or full URL for cross domain requests
	     * @param {callback(responseText)} on_success - callback for status code 200
	     * @param {callback(responseText, responseStatusCode)} on_error = null - custom handler for non 200 status codes
	     * @param headers = {'X-Requested-With': 'XMLHttpRequest'} key: value map of headers
	     */
	    get: function get(url, on_success) {
	        var on_error = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	        var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : { 'X-Requested-With': 'XMLHttpRequest' };

	        this.create('GET', url, {}, on_success, on_error, headers, true);
	    },

	    post: function post(url, data, on_success) {
	        var on_error = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
	        var headers = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : { 'X-Requested-With': 'XMLHttpRequest' };

	        this.create('POST', url, data, on_success, on_error, headers, true);
	    }

	};

/***/ }),

/***/ 218:
/***/ (function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	var BunnyElement = exports.BunnyElement = {
	  getCurrentDocumentPosition: function getCurrentDocumentPosition() {
	    var top = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

	    //return Math.abs(document.body.getBoundingClientRect().y);
	    return top ? window.scrollY : window.scrollY + window.innerHeight;
	  },
	  getPosition: function getPosition(el) {
	    var top = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

	    var curTop = 0;
	    var originalEl = el;
	    if (el.offsetParent) {
	      do {
	        curTop += el.offsetTop;
	      } while (el = el.offsetParent);
	    }
	    if (!top) {
	      curTop += originalEl.offsetHeight;
	    }
	    return curTop;
	  },
	  isInViewport: function isInViewport(el) {
	    var offset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
	    var top = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

	    var docPos = this.getCurrentDocumentPosition(top);
	    var elPos = this.getPosition(el, top);
	    return elPos + offset <= docPos;
	  },
	  scrollToIfNeeded: function scrollToIfNeeded(target) {
	    var viewportOffset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
	    var viewportTop = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
	    var duration = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 500;
	    var scrollOffset = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;

	    if (!this.isInViewport(target, viewportOffset, viewportTop)) {
	      this.scrollTo(target, duration, scrollOffset);
	    }
	  },


	  /**
	   * Smooth scrolling to DOM element or to relative window position
	   * If target is string it should be CSS selector
	   * If target is object it should be DOM element
	   * If target is number - it is used to relatively scroll X pixels form current position
	   *
	   * Based on https://www.sitepoint.com/smooth-scrolling-vanilla-javascript/
	   *
	   * @param {HTMLElement, string, number} target
	   * @param {Number|function} duration
	   * @param {Number} offset
	   * @param {HTMLElement} rootElement
	   */
	  scrollTo: function scrollTo(target) {
	    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
	    var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
	    var rootElement = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : window;

	    return new Promise(function (onAnimationEnd) {

	      var element = void 0;
	      if (typeof target === 'string') {
	        element = document.querySelector(target);
	      } else if ((typeof target === 'undefined' ? 'undefined' : _typeof(target)) === 'object') {
	        element = target;
	      } else {
	        // number
	        element = null;
	      }

	      if (element !== null && element.offsetParent === null) {
	        // element is not visible, scroll to top of parent element
	        element = element.parentNode;
	      }

	      var start = rootElement === window ? window.pageYOffset : rootElement.scrollTop;
	      var distance = 0;
	      if (element !== null) {
	        distance = element.getBoundingClientRect().top;
	      } else {
	        // number
	        distance = target;
	      }

	      distance = distance + offset;

	      if (typeof duration === 'function') {
	        duration = duration(distance);
	      }

	      var timeStart = 0;
	      var timeElapsed = 0;

	      requestAnimationFrame(function (time) {
	        timeStart = time;
	        loop(time);
	      });

	      function setScrollYPosition(el, y) {
	        if (el === window) {
	          window.scrollTo(0, y);
	        } else {
	          el.scrollTop = y;
	        }
	      }

	      function loop(time) {
	        timeElapsed = time - timeStart;
	        setScrollYPosition(rootElement, easeInOutQuad(timeElapsed, start, distance, duration));
	        if (timeElapsed < duration) {
	          requestAnimationFrame(loop);
	        } else {
	          end();
	        }
	      }

	      function end() {
	        setScrollYPosition(rootElement, start + distance);
	        onAnimationEnd();
	      }

	      // Robert Penner's easeInOutQuad - http://robertpenner.com/easing/
	      function easeInOutQuad(t, b, c, d) {
	        t /= d / 2;
	        if (t < 1) return c / 2 * t * t + b;
	        t--;
	        return -c / 2 * (t * (t - 2) - 1) + b;
	      }
	    });
	  },
	  hide: function hide(element) {
	    return new Promise(function (resolve) {
	      element.style.opacity = 0;
	      element.style.overflow = 'hidden';
	      var steps = 40;
	      var step_delay_ms = 10;
	      var height = element.offsetHeight;
	      var height_per_step = Math.round(height / steps);
	      element._originalHeight = height;

	      var _loop = function _loop(k) {
	        if (k === steps) {
	          setTimeout(function () {
	            element.style.display = 'none';
	            element.style.height = '0px';
	            resolve();
	          }, step_delay_ms * k);
	        } else {
	          setTimeout(function () {
	            element.style.height = height_per_step * (steps - k) + 'px';
	          }, step_delay_ms * k);
	        }
	      };

	      for (var k = 1; k <= steps; k++) {
	        _loop(k);
	      }
	    });
	  },
	  show: function show(element) {
	    if (element._originalHeight === undefined) {
	      throw new Error('element._originalHeight is undefined. Save original height when hiding element or use BunnyElement.hide()');
	    }
	    return new Promise(function (resolve) {
	      element.style.display = '';
	      var steps = 40;
	      var step_delay_ms = 10;
	      var height = element._originalHeight;
	      var height_per_step = Math.round(height / steps);
	      delete element._originalHeight;

	      var _loop2 = function _loop2(k) {
	        if (k === steps) {
	          setTimeout(function () {
	            element.style.opacity = 1;
	            element.style.height = '';
	            element.style.overflow = '';
	            resolve();
	          }, step_delay_ms * k);
	        } else {
	          setTimeout(function () {
	            element.style.height = height_per_step * k + 'px';
	          }, step_delay_ms * k);
	        }
	      };

	      for (var k = 1; k <= steps; k++) {
	        _loop2(k);
	      }
	    });
	  },
	  remove: function remove(element) {
	    element.parentNode.removeChild(element);
	  }
	};

/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';

	var _functions = __webpack_require__(213);

	_functions.JS.addEvent(window, 'load', function () {
	    var textareas = _functions.JS.selectAll('textarea');

	    for (var i = 0; i < textareas.length; i++) {
	        var element = textareas[i];

	        element.style.height = "5px";
	        element.style.height = element.scrollHeight + "px";
	    }
	});

/***/ })

/******/ });