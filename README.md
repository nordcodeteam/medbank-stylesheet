### Medbank stylesheet ###

* This repository contains CSS and JS bundles; js and less module files.

### Setup ###

* Include public/css/loan-for-RE.css in head section 
* Include public/js/form.bundle.js and public/js/form.bundle.js before the end of the body

For some optional functionality like popup
* Use resources/views/stylesheet_layout.blade.php layout file for global javascript values and pre-rendered prefabs

### Elements used with include and parameters ###

Use-ready elements are stored in resources/views/elements directory
* Include element and pass needed parameters
* Examples are used in resources/views/stylesheet.blade.php

### Demo ###

* Demo can be run from /demo/stylesheet.html file



### SVG render helper ###
```
if (! function_exists('renderSvg')) {
    function renderSvg($path, $class = '')
    {
        $svg = new \DOMDocument();
        $svg->load($path);
        $svg->documentElement->setAttribute("class", $class);

        return $svg->saveXML($svg->documentElement);
    }
}
```